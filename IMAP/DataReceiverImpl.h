/************************************************************************/
/* Name     : MEMAIL\DataReceiverImpl.h                                 */
/* Author   : Andrey Alekseev                                           */
/* Project  : MEMAIL                                                    */
/* Company  : Expert Solutions                                          */
/* Date     : 15 Aug 2016                                               */
/************************************************************************/

#pragma once
#include "Router\router_compatibility.h"
#include "DataReceiver.h"
#include "ImapMessageContext.h"


struct TImapParams
{
	std::wstring _email;
	std::wstring _certificate_uri;
	bool _sslEnable = false;
	std::wstring _uri;
	std::wstring _port;
	std::wstring _login;
	std::wstring _pass;
	bool _peek = false;
	std::wstring _afterReadMoveTo;
};

struct TSmtpParams
{

};

class CMailConnectorImpl;

class CDataReceiverImpl : public CDataReceiver < CDataReceiverImpl >
{

	friend class CDataReceiver < CDataReceiverImpl >;

public:
	CDataReceiverImpl(boost::asio::io_service& io);
	~CDataReceiverImpl();

	//void TestReplyEventHandler(CMessage& _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
	//{
	//	onReplyEventHandler(_msg, _from, _to);
	//}

private:
	int init_impl(CSystemLog* pLog);
	int start();

private:
	//router api
	void on_routerConnect();
	void on_routerDeliverError(CMessage&, const CLIENT_ADDRESS&, const CLIENT_ADDRESS&);
	void on_appStatusChange(DWORD dwClient, /*int*/core::ClientStatus	nStatus);
	void set_Subscribes();
	void stop();

	//void OnTimerExpired();


	// handlers
	//void onReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);

	//uint32_t get_ClientId()const
	//{
	//	return m_router.BoundAddress();
	//}

	//template <class TMessage>
	//void sendToAny(TMessage&& message)const
	//{
	//	m_router.SendToAny(std::forward<TMessage>(message));
	//}

	//template <class TMessage>
	//void sendToAll(TMessage&& message)const
	//{
	//	m_router.SendToAll(std::forward<TMessage>(message));
	//}

	//template <class TMessage, class TAddress>
	//void sendTo(TMessage&& message, TAddress&& address)const
	//{
	//	m_router.SendTo(std::forward<TMessage>(message), std::forward<TAddress>(address));
	//}


public:
	//handlers
	void receive_onAccepted(MessagePtr message)const;
	void receive_onCompleted(const std::wstring& _email, MessagePtr message)const;
	void receive_onFailed(const std::string& message)const;

	//void send_onAccepted(const CMessage& msg)const;
	//void send_onCompleted(const CMessage& msg)const;
	//void send_onFailed(const CMessage& msg)const;


private:
	CSystemLog * m_log;

	boost::asio::io_service& r_io;

	std::unique_ptr<CMailConnectorImpl> m_idapConnector;
};

/******************************* eof *************************************/