/************************************************************************/
/* Name     : MEMAIL\SMTP\DataReceiverImpl.h                            */
/* Author   : Andrey Alekseev                                           */
/* Project  : MEMAIL\SMTP                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 16 Aug 2016                                               */
/************************************************************************/

#pragma once

#include "Router\router_compatibility.h"
#include "DataReceiver.h"
//#include "ImapMessageContext.h"

class CMailConnectorImpl;

using MessageCollector = std::list<CMessage>;

using statusHandlerSmtp = std::function<void(const CMessage&)>;

struct TImapParams
{
};


struct TSmtpParams
{
	std::wstring _sSmtpServer;
	std::wstring _sSmtpPort;
	std::wstring _sLogin;
	std::wstring _sPassword;
	std::wstring _sCertificate;
	std::wstring _sSmtpClientAddress;
	bool _sslEnable;

	statusHandlerSmtp _accepted;
	statusHandlerSmtp _completed;
	statusHandlerSmtp _failed;
};

class CDataReceiverImpl : public CDataReceiver < CDataReceiverImpl >
{

	friend class CDataReceiver < CDataReceiverImpl >;

public:
	CDataReceiverImpl(boost::asio::io_service& io);
	~CDataReceiverImpl();

	void TestReplyEventHandler(CMessage& _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
	{
		onReplyEventHandler(_msg, _from, _to);
	}

private:
	int init_impl(CSystemLog* pLog);
	int start();

private:
	//router api
	void on_routerConnect();
	void on_routerDeliverError(CMessage&, const CLIENT_ADDRESS&, const CLIENT_ADDRESS&);
	void on_appStatusChange(DWORD dwClient, /*int*/core::ClientStatus	nStatus);
	void set_Subscribes();
	void stop();

	//void OnTimerExpired();


	// handlers
	void onReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to);

	//uint32_t get_ClientId()const
	//{
	//	return m_router.BoundAddress();
	//}

	//template <class TMessage>
	//void sendToAny(TMessage&& message)const
	//{
	//	m_router.SendToAny(std::forward<TMessage>(message));
	//}

	//template <class TMessage>
	//void sendToAll(TMessage&& message)const
	//{
	//	m_router.SendToAll(std::forward<TMessage>(message));
	//}

	//template <class TMessage, class TAddress>
	//void sendTo(TMessage&& message, TAddress&& address)const
	//{
	//	m_router.SendTo(std::forward<TMessage>(message), std::forward<TAddress>(address));
	//}


public:
	//handlers
	//void receive_onAccepted(MessagePtr message)const;
	//void receive_onCompleted(const std::wstring& _email, MessagePtr message)const;
	//void receive_onFailed(const std::string& message)const;

	void send_onAccepted(const CMessage& msg)const;
	void send_onCompleted(const CMessage& msg)const;
	void send_onFailed(const CMessage& msg)const;

	boost::asio::io_service& get_io_service()const { return r_io; }

private:
	CSystemLog * m_log;

	boost::asio::io_service& r_io;

	std::unique_ptr<CMailConnectorImpl> m_idapConnector;

	std::mutex m_mutex;

	MessageCollector m_incomingMsgList;
};

/******************************* eof *************************************/