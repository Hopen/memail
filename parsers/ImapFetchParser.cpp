/************************************************************************/
/* Name     : MCC\ImapFetchParser.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 11 Mar 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include "ImapFetchParser.h"


namespace fetchparser {

	using ParserCallBack2 = std::function <void(const int& numMsg, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)>;

	template <typename Iterator>
	bool parse2(Iterator first, Iterator last, ParserCallBack2 _callback, std::string& e_what)
	{
#ifdef PARSER_ENABLE
		bool r = false;

		try
		{
			r = boost::spirit::qi::phrase_parse(
				first,
				last,
				(
					'*'
					>> boost::spirit::qi::int_[_callback]
					>> "FETCH"
					),
				boost::spirit::ascii::space
				);
		}
		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
		{
			std::string error(e.first, e.last);
			e_what = error;

		}
		catch (const std::exception& e)
		{
			e_what = e.what();
		}
		catch (...)
		{
			e_what = "Unknown exception";
		}

		if (first != last) // fail if we did not get a full match
			return false;

		//sectionList list = output.GetSections();
		return r;
#else

		return false;
#endif
	}
}

bool fetchparser_parse2(std::string source, std::function<void(const int& numMsg, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string & errString)
{
	bool r = fetchparser::parse2(
		source.begin(),
		source.end(),
		_callback,
		errString
		);

	return r;
}


/******************************* eof *************************************/