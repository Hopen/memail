/************************************************************************/
/* Name     : MCC\ImapMessageContext.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 23 Mar 2016                                               */
/************************************************************************/
#pragma once

//#include <iostream>
//#include <memory>
//#include <vector>
#include <boost/program_options/detail/convert.hpp>
#include "Patterns\string_functions.h"
#include "Router\router_compatibility.h"
#include "codec.h"

class CBody;
class CSection;

using BodyPtr = std::shared_ptr <CBody>;
using SectionPtr = std::shared_ptr <CSection>;


//template<class value_type, class T>
//std::basic_string<value_type> toStr(T aa)
//{
//	std::basic_ostringstream<value_type> out;
//
//	out << aa;
//	return out.str();
//}
//std::string encodeBase64(std::string pData)
//{
//	using base64_text = boost::archive::iterators::base64_from_binary<boost::archive::iterators::transform_width<const char *, 6, 8> >;
//
//	std::stringstream os;
//	size_t sz = pData.size();
//	std::copy(base64_text(pData.c_str()), base64_text(pData.c_str() + sz), std::ostream_iterator<char>(os));
//	return os.str();
//}


class CBody
{
public:
	CBody() = default;
	CBody(int size, const std::wstring& _name,  const std::wstring& _encoding) : m_size(size), m_name(_name), m_encode(_encoding) {}
	virtual ~CBody() = default;

	CBody(const CBody&) = default;
	CBody& operator=(const CBody&) = default;

	CBody(CBody&&) = default;
	CBody& operator=(CBody&&) = default;

	virtual void setSource(const std::string&  _source)
	{
		m_source = stow(_source);
	}

	//virtual bool getIndexByContentType(const std::wstring& _content, const std::wstring& _type, int& index)const { return false; }
	virtual BodyPtr getSectionByIndex(int parent_index, const std::string& index) { return nullptr; }
	virtual void doCallbackByParam(const std::wstring& _content, const std::wstring& _type, const std::wstring& _subtype, boost::function<void(const int&)> callback)const {}
	virtual void getSectionsSource(boost::function<void(const std::wstring& _name, const std::wstring& _content, const std::wstring& _type, const std::wstring& source)> callback)const {}

	virtual void getSectionIndex(int parent_index, std::wstring s_parent_index, boost::function<void(const std::wstring&)> callback) const {}
	
	virtual std::wstring getSource()const noexcept { return m_source; }
	std::wstring getName()const noexcept { return m_name; }


	int getSize()const noexcept { return m_size; }


	//virtual void push_back(BodyPtr&& _ptr) {};
protected:
	std::wstring m_encode;
	std::wstring m_source;
	std::wstring m_name;
	std::wstring m_index;

	int m_size{ 0 };
};

class CText : public CBody
{
public:
	CText() = default;
	CText(int size, const std::wstring& _name, const std::wstring& _charset, const std::wstring& _encode)
		:CBody(size, _name,_encode), m_charset(_charset)
	{

	}

	//virtual bool getIndexByContentType(const std::wstring& _content, const std::wstring& _type, int& index) const { return false; }
	//virtual BodyPtr getSectionByIndex(int index) { return nullptr; }

	std::wstring getCharser()const noexcept { return m_charset; }

	void setSource(const std::string& _source) override
	{
		m_source = DecodeText(wtos(m_charset), wtos(m_encode), _source);
	}


private:
	std::wstring m_charset;
};

class CImage : public CBody
{
public:
	CImage() = default;

	CImage(int size, const std::wstring& _name, const std::wstring& _encode)
		: CBody(size, _name, _encode)
	{}

	//virtual bool getIndexByContentType(const std::wstring& _content, const std::wstring& _type, int& index)const { return false; }
	//virtual BodyPtr getSectionByIndex(int index) { return nullptr; }

	void setSource(const std::string& _source) override
	{
		m_source = decodeBase64_Safe(_source);
	}

};


class CApplication : public CBody
{
public:
	CApplication() = default;

	CApplication(int size, const std::wstring& _name, const std::wstring& _encode)
		: CBody(size, _name, _encode)
	{}

	//virtual bool getIndexByContentType(const std::wstring& _content, const std::wstring& _type, int& index)const { return false; }
	//virtual BodyPtr getSectionByIndex(int index) { return nullptr; }

	void setSource(const std::string& _source) override
	{
		m_source = decodeBase64_Safe(_source);
	}

};

class CMessageMfc : public CBody
{
public:
	CMessageMfc() = default;

	CMessageMfc(int size, const std::wstring& _name, const std::wstring& _encode)
		: CBody(size, _name, _encode)
	{}

	void setSource(const std::string& _source) override
	{
		m_source = decodeBase64_Safe(_source);
	}

};

using SectionList = std::vector< BodyPtr >;
//using HeaderPtr = std::unique_ptr <CHeader>;

class CSection : public CBody, public std::enable_shared_from_this <CSection>
{

public:
	CSection() = default;

	CSection(
		//const std::wstring& _content,
		//const std::wstring& _type,
		const std::wstring& _subtype)
		://m_content(_content),
		//m_type(_type),
		m_subtype(_subtype)
	{}

	template <class T>
	void push_back(T&& _ptr)
	{
		m_children.push_back(std::forward<T>(_ptr));
	}

	void setContent(const std::wstring& _content) { m_content = _content; }
	void setType(const std::wstring& _type) { m_type  = _type;}
	void setIndex(int index) { m_index = index; }

	int getChildrenSize() { return m_children.size(); }

	std::wstring getContent()const noexcept { return m_content; }
	std::wstring getType()const noexcept { return m_type; }

	//bool getIndexByContentType(const std::wstring& _content, const std::wstring& _type, int& index)const
	//{
	//	if (m_content == _content && m_type == _type)
	//	{
	//		index = m_index;
	//		return true;
	//	}

	//	for each (BodyPtr body in m_children)
	//	{
	//		if (body->getIndexByContentType(_content, _type, index))
	//			return true;
	//	}

	//	return false;
	//}

	void doCallbackByParam(const std::wstring& _content, const std::wstring& _type, const std::wstring& _subtype, boost::function<void(const int&)> callback) const override;

	void getSectionIndex(int parent_index, std::wstring s_parent_index, boost::function<void(const std::wstring&)> callback) const override;

	BodyPtr getSectionByIndex(int parent_index, const std::string& index) override;
	//{
	//	if (m_index == index)
	//		//return BodyPtr(const_cast<CSection*>(this));
	//	{
	//		return shared_from_this();
	//	}
	//		

	//	for each (BodyPtr body in m_children)
	//	{
	//		BodyPtr ptr = body->getSectionByIndex(index);
	//		if (ptr)
	//			return ptr;
	//	}

	//	return nullptr;
	//}


	BodyPtr getFirstChild()const
	{
		if (m_children.size())
			return *m_children.begin();

		return nullptr;
	}

	void getSectionsSource(boost::function<void(const std::wstring& _name, const std::wstring& _content, const std::wstring& _type, const std::wstring& source)> callback)const override;

	void increaseIndex() noexcept { ++m_index; }
	int getIndex()const noexcept { return m_index; }

private:
	std::wstring m_content;
	std::wstring m_type;
	std::wstring m_subtype;

	int m_index{0};

	SectionList m_children;
};

using MessageId = int/*__int64*/;

class CIMAPMessage
{
public:

	CIMAPMessage(
		MessageId msgId,
		const std::wstring& sSubject,
		const core::time_type& sReceiveDate,
		const core::time_type& sSendDate,
		const std::wstring& sFrom,
		const std::wstring& sTo,
		const std::wstring& sCC,
		SectionPtr&& _section)
		:m_id(msgId),
		m_sSubject(sSubject),
		m_sReceiveDate(sReceiveDate),
		m_sSendDate(sSendDate),
		m_sFrom(sFrom),
		m_sTo(sTo),
		m_sCC(sCC),
		m_section(_section)
	{}
	
	CIMAPMessage(
		MessageId msgId,
		const std::wstring& sSubject,
		const core::time_type& sReceiveDate,
		const core::time_type& sSendDate,
		const std::wstring& sFrom,
		const std::wstring& sTo,
		const std::wstring& sCC)
		:m_id(msgId),
		m_sSubject(sSubject),
		m_sReceiveDate(sReceiveDate),
		m_sSendDate(sSendDate),
		m_sFrom(sFrom),
		m_sTo(sTo),
		m_sCC(sCC)
	{}
	void setSections(SectionPtr&& _section) { m_section = _section; }

	void setSectionSource(const std::string& _index, const std::string& _source);
	//bool getSectionSourceAndCharsetByIndex(const unsigned int& _index, std::wstring& _source, std::wstring& _charset);
	//int getSectionIndexByContentType(const std::wstring& content, const std::wstring& type)const;

	//interface
public:
	int          getIMAPMessageId()const noexcept { return m_id; }
	std::wstring getSubject()const noexcept { return m_sSubject; }
	//std::wstring getReceiveDate   ()const noexcept { return m_sReceiveDate; }
	//std::wstring getSendDate      ()const noexcept { return m_sSendDate; }

	auto getReceiveDate()const noexcept { return m_sReceiveDate; }
	auto getSendDate()const noexcept { return m_sSendDate; }

	std::wstring getFrom()const noexcept { return m_sFrom; }
	std::wstring getTo()const noexcept { return m_sTo; }
	std::wstring getCC()const noexcept { return m_sCC; }
	int getWaitingSessionCount()const noexcept { return m_iWaitngSessionCount;  }
	void increaseWaitingSectionsCount() { ++m_iWaitngSessionCount; }
	void decreaseWaitingSectionsCount() { --m_iWaitngSessionCount; }

	//void getPlainTextAndCharset(std::wstring& _source, std::wstring& _charset)
	//{
	//	int index = getSectionIndexByContentType(L"text", L"plain");
	//	if (!index)
	//	{
	//		index = getSectionIndexByContentType(L"text", L"html");
	//	}

	//	if (index)
	//	{
	//		getSectionSourceAndCharsetByIndex(index, _source, _charset);
	//	}

	//}

	void getSectionsToDownload(boost::function<void(const std::wstring &)>);
	void getSectionsSource(boost::function<void(const std::wstring& _name, const std::wstring& _content, const std::wstring& _type, const std::wstring& source)>)const;


private:
	MessageId m_id;
	//HeaderPtr m_header;
	//SectionList m_sections;
	SectionPtr   m_section;

	std::wstring m_sSubject;
	//std::wstring m_sReceiveDate;
	//std::wstring m_sSendDate;
	core::time_type m_sReceiveDate;
	core::time_type m_sSendDate;
	std::wstring m_sFrom;
	std::wstring m_sTo;
	std::wstring m_sCC;
	int size;

	int m_iWaitngSessionCount{ 0 };
};

using MessagePtr = std::shared_ptr <CIMAPMessage>;
using MessageMap = std::map< int, MessagePtr >;
using MessageList = std::list< MessagePtr >;

using statusHandlerImap = std::function<void(MessagePtr)>;
using statusFailedHandlerImap = std::function<void(const std::string&)>;
using statusHandlerSmtp = std::function<void(const CMessage&)>;

/******************************* eof *************************************/