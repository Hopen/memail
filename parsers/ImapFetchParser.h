///************************************************************************/
///* Name     : MCC\ImapFetchParser.h                                     */
///* Author   : Andrey Alekseev                                           */
///* Company  : Expert Solutions                                          */
///* Date     : 11 Mar 2016                                               */
///************************************************************************/
//
//#pragma once
//
////#define PARSER_ENABLE
//
//#include <string>
//#include <map>
//#include <functional>
//
//#include <boost/format.hpp>
//#  pragma warning(disable:4348)
//#include <boost/config/warning_disable.hpp>
//#include <boost/spirit/include/qi.hpp>
//#include "Patterns/string_functions.h"
//#include "Patterns/time_functions.h"
//
//#ifdef PARSER_ENABLE
//#include <boost/spirit/include/phoenix_operator.hpp>
//#include <boost/spirit/include/phoenix_fusion.hpp>
//#include <boost/fusion/include/adapt_struct.hpp>
//#include <boost/variant/recursive_variant.hpp>
//#include <boost/spirit/include/phoenix_stl.hpp>
//#include <boost/spirit/include/qi_no_skip.hpp>
//#endif
//
//#include "ImapMessageContext.h"
//
//
//namespace fetchparser
//{
//	struct TDate
//	{
//		int day;
//		std::string month;
//		int year;
//
//		std::wstring makeDate()const
//		{
//			std::wstring s_date = (boost::wformat(L"%i %s %i") % day % month.c_str() % year).str();
//			return s_date;
//		}
//
//	};
//
//	struct TTime
//	{
//		int hours;
//		int minutes;
//		int seconds;
//
//		std::wstring addZero(int time)const
//		{
//			std::wstring s_time = (time > 9)? (boost::wformat(L"%i") % time).str() : (boost::wformat(L"0%i") % time).str();
//			return s_time;
//		}
//
//
//		std::wstring makeTime()const
//		{
//			std::wstring time = (boost::wformat(L"%s:%s:%s") % addZero(hours).c_str() % addZero(minutes).c_str() % addZero(seconds).c_str()).str();
//
//			return time;
//		}
//	};
//
//	struct TUtc
//	{
//		char sing;
//		int timezone;
//	};
//
//	struct TDateTime
//	{
//
//
//		TDate date;
//		TTime time;
//		//std::string utc;
//		TUtc utc;
//
//		//std::wstring makeDateTime()const
//		//{
//		//	return date.makeDate() + L" " + time.makeTime();
//		//}
//
//		auto getUtc()const
//		{
//			if (utc.sing == '+')
//				return static_cast<int>(static_cast<double>(utc.timezone) / 100);
//			else
//				return (-1) * static_cast<int>(static_cast<double>(utc.timezone) / 100);
//		}
//
//		auto makeDateTime(const int _utc = 0)const
//		{
//			std::map<std::string, int> months;
//			months["Jan"] = 0;
//			months["Feb"] = 1;
//			months["Mar"] = 2;
//			months["Apr"] = 3;
//			months["May"] = 4;
//			months["Jun"] = 5;
//			months["Jul"] = 6;
//			months["Aug"] = 7;
//			months["Sep"] = 8;
//			months["Oct"] = 9;
//			months["Nov"] = 10;
//			months["Dec"] = 11;
//			//core::time_type
//
//			std::tm tm = {};
//
//			tm.tm_hour = time.hours;
//			tm.tm_min = time.minutes;
//			tm.tm_sec = time.seconds;
//			tm.tm_year = date.year - 1900;
//			tm.tm_mday = date.day;
//			tm.tm_mon = months.at(date.month);
//
//			//tm.tm_hour += utc.sing == '+'? (+);
//
//			//if (utc.sing == '+')
//			//	tm.tm_hour += static_cast<int>(static_cast<double>(utc.timezone) / 100);
//			//else
//			//	tm.tm_hour -= static_cast<int>(static_cast<double>(utc.timezone) / 100);
//
//			tm.tm_hour += _utc;
//
//			return core::time_type::clock::from_time_t(_mkgmtime(&tm));
//		}
//	};
//
//	struct TAddress
//	{
//		std::string name;
//		std::string box_name;
//		std::string domain;
//
//		std::wstring makeAddress()const
//		{
//			return stow(box_name) + L"@" + stow(domain);
//		}
//	};
//
//
//	struct TAddressList
//	{
//		std::vector<TAddress> addressList;
//
//		std::wstring makeAddress()const
//		{
//			std::wstring addresses;
//
//			for each (auto address in addressList)
//			{
//				addresses += address.makeAddress() + L";";
//			}
//
//			return addresses;
//		}
//	};
//
//	struct TEnvelope
//	{
//		TDateTime send_date;
//		std::string subject;
//		TAddress from;
//		TAddressList to;
//		TAddressList cc;
//
//		std::string getSubject()const { return subject; }
//	};
//
//	struct TRFC822
//	{
//		int size;
//	};
//
//	struct TBodyParam
//	{
//		std::string name;
//		std::string value;
//	};
//
//	struct TBodyParamList
//	{
//		std::vector <TBodyParam> params;
//
//		template <class T>
//		std::wstring getParamByName(T &&name)const
//		{
//			std::wstring value;
//			auto cit = std::find_if(params.begin(), params.end(), [name = std::forward<T>(name) ](const TBodyParam& param)
//			{
//				if (param.name == name)
//				{
//					return true;
//				}
//
//				return false;
//			});
//
//			if (cit != params.end())
//			{
//				value = stow((*cit).value);
//			}
//
//			return value;
//		}
//
//		template <class T>
//		std::wstring getParamByName_Decode(T &&name)const
//		{
//			std::wstring value = getParamByName(std::forward<T>(name));
//			if (value.find(L"=?") == 0)
//				value = DecodeSubject(wtos(value));
//
//			return value;
//		}
//
//	};
//
//	struct TSection
//	{
//		std::string contentType;
//		std::string subType;
//		TBodyParamList param;
//		std::string encoding;
//		int size;
//
//	public:
//		std::wstring getEncoding()const { return stow(encoding); }
//		std::wstring getContentType()const { return stow(contentType); }
//		std::wstring getSubType()const { return stow(subType); }
//		int getSize()const noexcept { return size; }
//	};
//
//	//struct TSection
//	//{
//	//	TSectionDescription section;
//	//	std::string subType;
//	//public:
//	//	std::wstring getSubType()const { return stow(subType); };
//	//};
//
//	struct TBody;
//
//	typedef
//		boost::variant<
//		boost::recursive_wrapper<TBody>
//		, TSection
//		>
//		body_node;
//
//	using sectionList = std::vector<TSection>;
//
//	struct TBody
//	{
//
//	private:
//
//
//
//	public:
//		//sectionList GetSections()const
//		//{
//		//	sectionList list;
//
//		//	_lineanConverter converter(&list);
//		//	converter(*this);
//
//		//	return list;
//		//}
//
//		SectionPtr GetSections()const;
//		//{
//		//	SectionPtr section = std::make_shared<CSection>();
//
//		//	_lineanConverter converter(section, 0);
//		//	converter(*this);
//
//		//	_nodeLineanConverter::reset();
//
//		//	return section;
//		//}
//
//	public:
//		std::vector <body_node> sections;
//	std::string subType;
//	};
//
//	//void TBody::_lineanConverter::operator()(TBody const & body) const
//	//{
//	//	BOOST_FOREACH(body_node const& node, body.sections)
//	//	{
//	//		boost::apply_visitor(_nodeLineanConverter(m_pList), node);
//	//	}
//
//	//}
//
//	struct THeader
//	{
//		std::string flag;
//		TDateTime receive_date; //INTERNALDATE
//		TRFC822 size; //RFC822.SIZE
//		TEnvelope envelope; //ENVELOPE
//		TBody body; //BODY
//	};
//
//	struct TDocument
//	{
//		int num;
//		THeader header;
//		
//	};
//
//
//	struct TSource
//	{
//		std::string ss;
//	};
//
//
//	struct TContainer;
//
//	typedef
//		boost::variant<
//		boost::recursive_wrapper<TContainer>
//		, TSource
//		>
//		container_node;
//
//	struct TContainer
//	{
//	public:
//		std::vector <container_node> sections;
//	};
//
//};
//
//#ifdef PARSER_ENABLE
//
//BOOST_FUSION_ADAPT_STRUCT(
//	fetchparser::TDate,
//	(int, day)
//	(std::string, month)
//	(int, year)
//	)
//
//
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TTime,
//		(int, hours)
//		(int, minutes)
//		(int, seconds)
//		)
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TUtc,
//		(char, sing)
//		(int, timezone)
//		)
//
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TDateTime,
//		(fetchparser::TDate, date)
//		(fetchparser::TTime, time)
//		//(std::string, utc)
//		(fetchparser::TUtc, utc)
//		)
//
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TAddress,
//		(std::string, name)
//		(std::string, box_name)
//		(std::string, domain)
//		)
//
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TAddressList,
//		(std::vector<fetchparser::TAddress>, addressList)
//		)
//
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TEnvelope,
//		(fetchparser::TDateTime, send_date)
//		(std::string, subject)
//		(fetchparser::TAddress, from)
//		(fetchparser::TAddressList, to)
//		(fetchparser::TAddressList, cc)
//		)
//	
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TRFC822,
//		(int, size)
//		)
//
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TBodyParam,
//		(std::string, name)
//		(std::string, value)
//		)
//
//BOOST_FUSION_ADAPT_STRUCT(
//	fetchparser::TBodyParamList,
//	(std::vector<fetchparser::TBodyParam>, params)
//	)
//
//
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TSection,
//		(std::string, contentType)
//		(std::string, subType)
//		(fetchparser::TBodyParamList, param)
//		(std::string, encoding)
//		(int, size)
//		)
//
//
//
////	BOOST_FUSION_ADAPT_STRUCT(
////		fetchparser::TSection,
////		(fetchparser::TSectionDescription, section)
////		(std::string, subType)
////		)
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TBody,
//		(std::vector <fetchparser::body_node>, sections)
//(std::string, subType)
//		)
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::THeader,
//		(std::string, flag)
//		(fetchparser::TDateTime, receive_date)
//		(fetchparser::TRFC822, size)
//		(fetchparser::TEnvelope, envelope)
//		(fetchparser::TBody, body)
//		)
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TDocument,
//		(int, num)
//		(fetchparser::THeader, header)
//		)
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TContainer,
//		(std::vector <fetchparser::container_node>, sections)
//		)
//
//	BOOST_FUSION_ADAPT_STRUCT(
//		fetchparser::TSource,
//		(std::string, ss)
//		)
//
//
//#endif
//
//namespace fetchparser {
//
//	using ParserCallBack = std::function <void(const TDocument& answer, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)>;
//
//	template <typename Iterator>
//	bool parse(Iterator first, Iterator last, ParserCallBack _callback, std::string& e_what)
//	{
//#ifdef PARSER_ENABLE
//
//		// rules
//		boost::spirit::qi::rule<Iterator, std::string(), boost::spirit::ascii::space_type> flags, month, week_day, /*utc,*/ nil, name, utf8, content, type, content_type, code, /*body_param_type,*/ subtype, name2;
//		boost::spirit::qi::rule<Iterator, TRFC822(), boost::spirit::ascii::space_type> rfc822;
//		boost::spirit::qi::rule<Iterator, TDate(), boost::spirit::ascii::space_type> date;
//		boost::spirit::qi::rule<Iterator, TTime(), boost::spirit::ascii::space_type> time;
//		boost::spirit::qi::rule<Iterator, TUtc(), boost::spirit::ascii::space_type> utc;
//		boost::spirit::qi::rule<Iterator, TDateTime(), boost::spirit::ascii::space_type> datetime, datetime_value, internaldate;
//		boost::spirit::qi::rule<Iterator, THeader(), boost::spirit::ascii::space_type> header;
//		boost::spirit::qi::rule<Iterator, TAddress(), boost::spirit::ascii::space_type> address;
//		boost::spirit::qi::rule<Iterator, TAddressList(), boost::spirit::ascii::space_type> addressList;
//		boost::spirit::qi::rule<Iterator, TEnvelope(), boost::spirit::ascii::space_type> envelope;
//		boost::spirit::qi::rule<Iterator, TBodyParam(), boost::spirit::ascii::space_type> body_param;
//		boost::spirit::qi::rule<Iterator, TBodyParamList(), boost::spirit::ascii::space_type> body_param_list;
//	//boost::spirit::qi::rule<Iterator, TSectionDescription(), boost::spirit::ascii::space_type> sectionDescriptor;
//		boost::spirit::qi::rule<Iterator, TSection(), boost::spirit::ascii::space_type> section;
//		boost::spirit::qi::rule<Iterator, TBody(), boost::spirit::ascii::space_type> body;
//		boost::spirit::qi::rule<Iterator, body_node(), boost::spirit::ascii::space_type> node;
//		boost::spirit::qi::rule<Iterator, TDocument(), boost::spirit::ascii::space_type> document;
//		boost::spirit::qi::rule<Iterator, TSource(), boost::spirit::ascii::space_type> source;
//		boost::spirit::qi::rule<Iterator, TContainer(), boost::spirit::ascii::space_type> container;
//		boost::spirit::qi::rule<Iterator, container_node(), boost::spirit::ascii::space_type> cnode;
//
//
//		nil = boost::spirit::qi::string("NIL");
//
//
//		flags = boost::spirit::lit('(')
//			>> *(
//				boost::spirit::qi::string("\\Answered") |
//				boost::spirit::qi::string("\\Flagged") |
//				boost::spirit::qi::string("\\Deleted") |
//				boost::spirit::qi::string("\\Draft") |
//				boost::spirit::qi::string("\\Seen") |
//				boost::spirit::qi::string("\\Unanswered") |
//				boost::spirit::qi::string("\\Unflagged") |
//				boost::spirit::qi::string("\\Undeleted") |
//				boost::spirit::qi::string("\\Undraft") |
//				boost::spirit::qi::string("\\Unseen")
//				)
//			>> boost::spirit::lit(')');
//
//		//flags = -boost::spirit::lit('(')
//		//	>> +(
//		//		boost::spirit::qi::string("\\Answered") |
//		//		boost::spirit::qi::string("\\Flagged") |
//		//		boost::spirit::qi::string("\\Deleted") |
//		//		boost::spirit::qi::string("\\Draft") |
//		//		boost::spirit::qi::string("\\Seen") |
//		//		boost::spirit::qi::string("\\Unanswered") |
//		//		boost::spirit::qi::string("\\Unflagged") |
//		//		boost::spirit::qi::string("\\Undeleted") |
//		//		boost::spirit::qi::string("\\Undraft") |
//		//		boost::spirit::qi::string("\\Unseen")
//		//		)
//		//	>> -boost::spirit::lit(')');
//
//		month = (
//			boost::spirit::qi::string("Jan") |
//			boost::spirit::qi::string("Feb") |
//			boost::spirit::qi::string("Mar") |
//			boost::spirit::qi::string("Apr") |
//			boost::spirit::qi::string("May") |
//			boost::spirit::qi::string("Jun") |
//			boost::spirit::qi::string("Jul") |
//			boost::spirit::qi::string("Aug") |
//			boost::spirit::qi::string("Sep") |
//			boost::spirit::qi::string("Oct") |
//			boost::spirit::qi::string("Nov") |
//			boost::spirit::qi::string("Dec")
//			);
//
//		week_day = (
//			boost::spirit::qi::string("Mon") |
//			boost::spirit::qi::string("Tue") |
//			boost::spirit::qi::string("Wed") |
//			boost::spirit::qi::string("Thu") |
//			boost::spirit::qi::string("Fri") |
//			boost::spirit::qi::string("Sat") |
//			boost::spirit::qi::string("Sun")
//			);
//
//		date = boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])]
//			>> -boost::spirit::lit('-')
//			>> month[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> -boost::spirit::lit('-')
//			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1])];
//
//
//		time = boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])]
//			>> boost::spirit::lit(':')
//			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])]
//			>> boost::spirit::lit(':')
//			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1])];
//
//		//utc = boost::spirit::lexeme[(boost::spirit::qi::char_("+-"))]
//		//	>> boost::spirit::lexeme[+(boost::spirit::qi::char_("0-9"))];
//
//		utc = boost::spirit::lexeme[(boost::spirit::qi::char_("+-")[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])]
//			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])];;
//
//
//		name = '\"'
//		>> boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
//			>> '\"';
//
//		name2 = boost::spirit::lit('{')
//			>> boost::spirit::qi::int_
//			>> boost::spirit::lit('}')
//			//>> boost::spirit::lit('\r')
//			//>> boost::spirit::lit('\n')
//			>> (boost::spirit::no_skip[+~boost::spirit::qi::char_("(\"")]);
//			//>> boost::spirit::qi::string("?=");
//
//	//name = '\"'
//	//	>> boost::spirit::lexeme[+(boost::spirit::qi::char_("-a-zA-Z0-9_.<>@"))]
//	//	>> '\"';
//
//		utf8 = '\"'
//		>> boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
//			>> '\"';
//
////	utf8 = '\"'
////		>> boost::spirit::lexeme[+(boost::spirit::qi::char_("a-zA-Z0-9_.<>?=+-@!#$%^*()/"))]
////		>> '\"';
//
//
//		datetime = -week_day
//			>> -boost::spirit::lit(',')
//			>> date[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> time[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> utc[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> -boost::spirit::qi::string("(UTC)");
//
//		datetime_value = -boost::spirit::lit('\"')
//			>> datetime
//			>> -boost::spirit::lit('\"');
//
//		internaldate = /*"INTERNALDATE"
//			>>*/ datetime_value;
//
//		rfc822 = /*"RFC822.SIZE"
//				 >>*/ boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])];
//
//		address =
//			"("
//			>> (nil | "\"\"" | utf8[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])
//			>> nil
//			>> name[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> name[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> ")"
//			;
//
//		addressList = *address[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)];
//
//		envelope = boost::spirit::lit('(')
//			>> boost::spirit::lit('\"')
//			>> datetime[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> boost::spirit::lit('\"')
//			//>> utf8[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> (nil | utf8[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1] | name2[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])
//			>> boost::spirit::lit('(')
//			>> address[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> boost::spirit::lit(')')
//			//>> +nil
//			>> (nil |
//				(boost::spirit::lit('(')
//					>> address
//					>> boost::spirit::lit(')')
//					)
//				)
//			>> (nil | 
//			  (boost::spirit::lit('(')
//			   >> address
//			   >> boost::spirit::lit(')')
//		      )
//			)
//			//>> boost::spirit::lit('(')
//			//>> addressList[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
//			//>> boost::spirit::lit(')')
//			>> (nil |
//				(boost::spirit::lit('(')
//					//>> address[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
//					>> addressList[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
//					>> boost::spirit::lit(')')
//					)
//				)
//			>> (nil |
//				(boost::spirit::lit('(')
//					//>> address[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
//					>> addressList[boost::phoenix::at_c<4>(boost::spirit::_val) = boost::spirit::qi::_1]
//					>> boost::spirit::lit(')')
//					)
//				)
//			>> nil
//		        >> (nil | name)
//		        >> (nil | name)
//			>> boost::spirit::lit(')');
//
//		//header = "FLAGS"
//		//	>> flags[boost::phoenix::at_c<0>(boost::spirit::_val) += boost::spirit::qi::_1]
//		//	>> "INTERNALDATE"
//		//	>> -boost::spirit::lit('\"')
//		//	>> datetime[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//		//	>> -boost::spirit::lit('\"')
//		//	>> "RFC822.SIZE"
//		//	>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1])]
//		//	>> "ENVELOPE"
//		//	>> envelope[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1];
//
//
//		content = '\"'
//			>> (
//				boost::spirit::qi::string("text") |
//				boost::spirit::qi::string("image") |
//				boost::spirit::qi::string("audio") |
//				boost::spirit::qi::string("video") |
//				boost::spirit::qi::string("application") |
//				boost::spirit::qi::string("multipart") |
//				boost::spirit::qi::string("message")
//				)
//			>> '\"';
//
//		content_type = name;
//
//		//content_type = '\"'
//		//	>> (
//		//		// for text
//		//		boost::spirit::qi::string("plain") |
//		//		boost::spirit::qi::string("html") |
//		//boost::spirit::qi::string("calendar") |
//		//		// for image
//		//		boost::spirit::qi::string("png") |
//		//		boost::spirit::qi::string("jpeg") |
//		//		boost::spirit::qi::string("gif") |
//		//		// for audio
//		//		boost::spirit::qi::string("basic") |
//		//		// for video
//		//		boost::spirit::qi::string("mpeg") |
//		//		// for application
//		//		boost::spirit::qi::string("octet-stream") |
//		//		boost::spirit::qi::string("PostScript") |
//		//		// for multipart
//		//		boost::spirit::qi::string("mixed") |
//		//		boost::spirit::qi::string("alternative") |
//		//		boost::spirit::qi::string("digest") |
//		//		boost::spirit::qi::string("parallel") |
//		//		// for message
//		//		boost::spirit::qi::string("rfc822") |
//		//		boost::spirit::qi::string("partial") |
//		//		boost::spirit::qi::string("External-body") |
//		//		boost::spirit::qi::string("vnd.openxmlformats-officedocument.wordprocessingml.document") |
//		//		boost::spirit::qi::string("x-c")
//		//		)
//		//	>> '\"';
//
//		subtype = '\"'
//			>> (
//				boost::spirit::qi::string("mixed") |
//				boost::spirit::qi::string("alternative") |
//				boost::spirit::qi::string("related")
//				)
//			>> '\"';
//
//		//code = '\"'
//		//	>> (
//		//		boost::spirit::qi::string("utf-8") |
//		//		boost::spirit::qi::string("ascii") |
//		//		boost::spirit::qi::string("8bit") |
//		//		boost::spirit::qi::string("7bit") |
//		//		boost::spirit::qi::string("koi8-r") |
//		//		boost::spirit::qi::string("base64") |
//		//		boost::spirit::qi::string("quoted-printable")
//		//		)
//		//	>> '\"';
//
//		code = name;
//
//	//body_param_type = '\"'
//	//	>>(
//	//	boost::spirit::qi::string("charset") |
//	//	boost::spirit::qi::string("name")
//	//	)
//	//	>> '\"';
//
//	body_param = name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//		>> name[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1];
//
//
//		//>> -name
//		//>> ((body_param_type[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//		//	>> code[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1] ) |
//		//	(body_param_type[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//		//	>> name[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1] ))
//		//>> -name
//		//>> -name;
//
//	body_param_list = *body_param[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)];
//
//	source = name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1] | nil | boost::spirit::qi::int_;
//
//	cnode = (container | source)[boost::spirit::_val = boost::spirit::qi::_1];
//
//	container = '('
//		>> *cnode[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)]
//		>> ')';
//
//
//
//	section/*sectionDescriptor*/ =
//			content[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> content_type[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			//>> boost::spirit::lit('(')
//		//>> body_param_list[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
//			//>> boost::spirit::lit(')')
//		>> (nil |
//			(boost::spirit::lit('(')
//				>> body_param_list[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
//				>> boost::spirit::lit(')')
//				)
//			)
//
//			>> (name | nil)
//			>> (name2 | name | nil)
//			>> code[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<4>(boost::spirit::_val) = boost::spirit::qi::_1])]
//			>> *container
//			>> -boost::spirit::qi::int_;
//
//		//section = 
//		//	//-boost::spirit::lit('(')
//		//	/*>>*/ sectionDescriptor[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//		//	//>> -boost::spirit::lit(')')
//		//	//>> *subtype[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//		//	;
//
//
//		node = (body | section)[boost::spirit::_val = boost::spirit::qi::_1];
//
//
//		body = '('
//			>> *node[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)]
//			>> ')'
//			>> -subtype[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1];
//
//		//body_c = "BODY"
//		//	>> body;
//
//
//		header = *("FLAGS" > flags[boost::phoenix::at_c<0>(boost::spirit::_val) += boost::spirit::qi::_1]
//			| "INTERNALDATE" > internaldate[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			| "RFC822.SIZE" > rfc822[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
//			| "ENVELOPE" > envelope[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
//			| "BODY" > body[boost::phoenix::at_c<4>(boost::spirit::_val) = boost::spirit::qi::_1]
//			)
//			;
//
//
//		document = '*'
//			>> boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> "FETCH"
//			>> '('
//			>> header[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			//>> body_c[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> ')';
//			//>> *boost::spirit::lit('\r')
//			//>> *boost::spirit::lit('\n');
//
//
//		//auto lambda2 = [](const THeader& header, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type) {std::cout << header.recieve_date.c_str() << '\n'; };
//
//		TDocument output;
//
//		bool r = false;
//
//		try
//		{
//			r = boost::spirit::qi::phrase_parse(
//				first,
//				last,
//				(
//					document[_callback]
//					),
//				boost::spirit::ascii::space,
//				output
//				);
//		}
//		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
//		{
//			std::string error(e.first, e.last);
//			e_what = error;
//
//		}
//		catch (const std::exception& e)
//		{
//			e_what = e.what();
//		}
//		catch (...)
//		{
//			e_what = "Unknown exception";
//		}
//
//		if (first != last) // fail if we did not get a full match
//			return false;
//
//		//sectionList list = output.GetSections();
//		return r;
//#else
//
//		return false;
//#endif
//	}
//
//	using ParserCallBack2 = std::function <void(const int& numMsg, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)>;
//
//	template <typename Iterator>
//	bool parse2(Iterator first, Iterator last, ParserCallBack2 _callback, std::string& e_what)
//	{
//#ifdef PARSER_ENABLE
//		bool r = false;
//
//		try
//		{
//			r = boost::spirit::qi::phrase_parse(
//				first,
//				last,
//				(
//					'*'
//					>> boost::spirit::qi::int_[_callback]
//					>> "FETCH"
//					),
//				boost::spirit::ascii::space
//				);
//		}
//		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
//		{
//			std::string error(e.first, e.last);
//			e_what = error;
//
//		}
//		catch (const std::exception& e)
//		{
//			e_what = e.what();
//		}
//		catch (...)
//		{
//			e_what = "Unknown exception";
//		}
//
//		if (first != last) // fail if we did not get a full match
//			return false;
//
//		//sectionList list = output.GetSections();
//		return r;
//#else
//
//		return false;
//#endif
//	}
//}
//
//

bool fetchparser_parse2(std::string source, std::function <void(const int& numMsg, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);

/******************************* eof *************************************/