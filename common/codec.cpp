/************************************************************************/
/* Name     : MCC\codec.cpp                                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 01 Apr 2016                                               */
/************************************************************************/

#include "stdafx.h"
#include "codec.h"

#include <iostream>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/algorithm/string.hpp>

#include "Log/SystemLog.h"
#include "ImapSubjectParser.h"

std::string decodeBase64(const std::string &val) 
{
	using namespace boost::archive::iterators;
	using It = transform_width<binary_from_base64<std::string::const_iterator>, 8, 6>;

	return boost::algorithm::trim_right_copy_if(
		std::string(It(std::begin(val)), It(std::end(val))), [](char c) {
		return c == '\0';
	});
}

std::string decodeBase64_rar(const std::string &val)
{
	using namespace boost::archive::iterators;
	using It = transform_width<binary_from_base64<std::string::const_iterator>, 8, 6>;

	return std::string(It(std::begin(val)), It(std::end(val)));
}

std::string encodeBase64(const std::string &val) {
	using namespace boost::archive::iterators;
	using It = base64_from_binary<transform_width<std::string::const_iterator, 6, 8>>;
	auto tmp = std::string(It(std::begin(val)), It(std::end(val)));
	return tmp.append((3 - val.size() % 3) % 3, '=');
}

std::string QuotedPrintableEncode(const std::string &input)
{
	std::string output;

	char byte;
	const char hex[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	for (std::size_t i = 0; i < input.length(); ++i)
	{
		byte = input[i];

		if ((byte == 0x20) || (byte >= 33) && (byte <= 126) && (byte != 61))
		{
			output += (byte);
		}
		else
		{
			output += ('=');
			output += (hex[((byte >> 4) & 0x0F)]);
			output += (hex[(byte & 0x0F)]);
		}
	}

	return output;
}

std::string QuotedPrintableDecode(const std::string &input)
{
	//                    0  1  2  3  4  5  6  7  8  9  :  ;  <  =  >  ?  @  A   B   C   D   E   F
	const int hexVal[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 10, 11, 12, 13, 14, 15 };

	std::string output;

	for (std::size_t i = 0; i < input.length(); ++i)
	{
		if (input.at(i)/*.toAscii()*/ == '=')
		{
			char b1 = input.at(++i) - '0';

			if (b1 < 0 || b1>=sizeof(hexVal))
			{
				++i;
				continue;
			}

			char a1 = hexVal[b1] << 4;

			char b2 = input.at(++i) - '0';

			if (b2 < 0 || b2>=sizeof(hexVal))
			{
				continue;
			}

			char a2 = hexVal[b2];

			int b = a1 | a2;
			output += char(b);

			//output+=char(byte(hexVal[input.at(++i)/*.toAscii()*/ - '0'] << 4) | byte(hexVal[input.at(++i)/*.toAscii()*/ - '0']));
		}
		else
		{
			output += (input.at(i)/*.toAscii()*/);
		}
	}

	return output;
}



std::wstring DecodeText(const::std::string & _charset, const std::string & _encoding, const std::string & _source)
{
	singleton_auto_pointer<CSystemLog> log;

	//if (_charset != "ascii")
	//{
	//	std::string tempSource = _source;

	//	try
	//	{
	//		if (_encoding == "base64")
	//		{
	//			boost::replace_all(tempSource, "\n", "");
	//			boost::replace_all(tempSource, "\r", "");
	//			boost::replace_all(tempSource, " ", "");
	//			tempSource = decodeBase64(tempSource);
	//		}
	//		else if (_encoding == "quoted-printable")
	//		{
	//			tempSource = QuotedPrintableDecode(tempSource);
	//		}

	//		if (_charset.empty())
	//		{
	//			return stow(tempSource);
	//		}
	//		else
	//		{
	//			boost::locale::generator gen;
	//			std::locale loc = gen.generate(std::string("ru_RU.") + _charset);
	//			std::wstring utf8_string = boost::locale::conv::to_utf<wchar_t>(tempSource, loc);
	//			return utf8_string;
	//		}
	//	}
	//	catch (boost::locale::conv::conversion_error& error)
	//	{
	//		log->LogStringModule(LEVEL_INFO, L"ENCODING", L"Exception == \"%s\", source == \"%s\"", stow(error.what()).c_str(), stow(tempSource).c_str());
	//	}
	//	catch (boost::locale::conv::invalid_charset_error& error)
	//	{
	//		log->LogStringModule(LEVEL_INFO, L"ENCODING", L"Exception == \"%s\", source == \"%s\"", stow(error.what()).c_str(), stow(tempSource).c_str());
	//	}
	//	catch (...)
	//	{
	//		log->LogStringModule(LEVEL_INFO, L"ENCODING", L"Exception == \"unknown\", source == \"%s\"", stow(tempSource).c_str());

	//	}
	//}

		std::string tempSource = std::move(_source);

		try
		{
			if (_encoding == "base64")
			{
				boost::replace_all(tempSource, "\n", "");
				boost::replace_all(tempSource, "\r", "");
				boost::replace_all(tempSource, " ", "");
				tempSource = decodeBase64(tempSource);
			}
			else if (_encoding == "quoted-printable")
			{
				tempSource = QuotedPrintableDecode(tempSource);
			}

			if (_charset != "ascii")
			{
				if (_charset.empty())
				{
					return stow(tempSource);
				}
				else
				{
					boost::locale::generator gen;
					std::locale loc = gen.generate(std::string("ru_RU.") + _charset);
					std::wstring utf8_string = boost::locale::conv::to_utf<wchar_t>(tempSource, loc);
					return utf8_string;
				}
			}
		}
		catch (boost::locale::conv::conversion_error& error)
		{
			log->LogStringModule(LEVEL_INFO, L"ENCODING", L"Exception == \"%s\", source == \"%s\"", stow(error.what()).c_str(), stow(tempSource).c_str());
		}
		catch (boost::locale::conv::invalid_charset_error& error)
		{
			log->LogStringModule(LEVEL_INFO, L"ENCODING", L"Exception == \"%s\", source == \"%s\"", stow(error.what()).c_str(), stow(tempSource).c_str());
		}
		catch (...)
		{
			log->LogStringModule(LEVEL_INFO, L"ENCODING", L"Exception == \"unknown\", source == \"%s\"", stow(tempSource).c_str());

		}
		return stow(tempSource);
}

std::wstring DecodeSubject(const std::string & _source)
{
	singleton_auto_pointer<CSystemLog> log;

	std::wstring tempSubject;
	std::string sError;

	auto lambda = [&tempSubject](const imapsubjectparser::TSubject& subject, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
	{
		std::string encoding = "base64";
		if (std::toupper(subject._coding[0]) == 'Q')
			encoding = "quoted-printable";

		tempSubject += DecodeText(subject._charset, encoding, subject._subject);
	};

	std::string tmp = _source;

	bool r = imapsubjectparser::parse(
		tmp.begin(),
		tmp.end(),
		lambda,
		sError);

	if (!r)
	{
		log->LogStringModule(LEVEL_INFO, L"SPIRIT", L"Exception: == \"%s\"", stow(sError).c_str());
		tempSubject = stow(_source);
	}

	return tempSubject;
}

std::wstring decodeBase64_Safe(const std::string & _source)
{
	std::string tempSource = _source;
	try
	{
			boost::replace_all(tempSource, "\n", "");
			boost::replace_all(tempSource, "\r", "");
			tempSource = decodeBase64_rar(tempSource);
	}
	catch (...)
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogStringModule(LEVEL_INFO, L"ENCODING", L"Exception when try to decode base64, source == \"%s\"", stow(tempSource).c_str());

	}

	return stow(tempSource);
}


/******************************* eof *************************************/