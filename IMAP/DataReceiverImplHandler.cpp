/************************************************************************/
/* Name     : MEMAIL\DataReceiverImplHandler.cpp                        */
/* Author   : Andrey Alekseev                                           */
/* Project  : MEMAIL                                                    */
/* Company  : Expert Solutions                                          */
/* Date     : 15 Aug 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include "DataReceiverImpl.h"
#include "api/ProcessHelper.h"

static __int64 CreateMCCID(int num_message, uint32_t _client_id)
{
	__int64 mccid = _client_id;
	mccid = (mccid << 32) | num_message;
	return mccid;
}


///************************************************************************/
///***************************   OUTCOMING   ******************************/
///************************************************************************/
//
//void CDataReceiverImpl::onReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
//{
//
//}

/************************************************************************/
/***************************   INCOMING    ******************************/
/************************************************************************/

void CDataReceiverImpl::receive_onAccepted(MessagePtr message)const
{

}

void CDataReceiverImpl::receive_onCompleted(const std::wstring& _email, MessagePtr msg)const
{
	SystemConfig settings;

	std::wstring sImapMessageType;
	bool bAlterTextTypeEnable = true, bHtmlContentTypeEnable = true;
	try
	{
		sImapMessageType = settings->get_config_value<std::wstring>(_T("PreferedMessageType"));
	}
	catch (...)
	{
		sImapMessageType = L"text.plain";
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"PreferedMessageType\" config key");
	}

	try
	{
		bAlterTextTypeEnable = settings->safe_get_config_bool(L"AlterTextTypeEnable", true);
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"AlterTextTypeEnable\" config key");
	}

	try
	{
		bHtmlContentTypeEnable = settings->safe_get_config_bool(L"HtmlContentTypeEnable", true);
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"HtmlContentTypeEnable\" config key");
	}



	auto mccid = CreateMCCID(msg->getIMAPMessageId(), GetClientId());
	std::wstring sMessageFolderMCCID = toStr<wchar_t>(mccid);

	CMessage newMail(L"MCC2TA_NEWEMAIL");
	newMail[L"Type"] = L"Email";
	newMail[L"MCCID"] = mccid;
	newMail[L"From"] = msg->getFrom();
	newMail[L"To"] = _email;//msg->getTo().c_str(); 
	newMail[L"ToList"] = msg->getTo();
	newMail[L"CC"] = msg->getCC();
	newMail[L"Subject"] = msg->getSubject();
	newMail[L"Senddate"] = msg->getSendDate();
	newMail[L"ReceiveDate"] = msg->getReceiveDate();
	newMail[L"Host"] = stow(boost::asio::ip::host_name());

	std::wstring sAttachmentFolder, sAttachmentString;
	try
	{
		sAttachmentFolder = settings->get_config_value<std::wstring>(_T("AttachmentFolder"));
		if (sAttachmentFolder[sAttachmentFolder.length() - 1] == ('\\'))
		{
			sAttachmentFolder += sMessageFolderMCCID;
		}
		else
		{
			sAttachmentFolder += L"\\" + sMessageFolderMCCID;
		}
		sAttachmentFolder += L"\\inbox";

	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"AttachmentFolder\" config key");
		sAttachmentFolder = L"c:\\Attachments\\inbox\\" + sMessageFolderMCCID;
	}

	try
	{
		sAttachmentString = settings->get_config_value<std::wstring>(_T("AttachmentString"));

		if (sAttachmentString[sAttachmentString.length() - 1] == ('/'))
		{
			sAttachmentString += sMessageFolderMCCID;
		}
		else
		{
			sAttachmentString += L"/" + sMessageFolderMCCID;
		}

		sAttachmentString += L"/inbox";
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"AttachmentString\" config key");
	}


	int iSectionCount = 0;
	bool bGotPlain = false;

	if (!boost::filesystem::exists(sAttachmentFolder) && !boost::filesystem::create_directories(sAttachmentFolder))
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_WARNING, L"Cannot create \"%s\" folder", sAttachmentFolder.c_str());
	}
	else
	{
		msg->getSectionsSource([&newMail,
			sAttachmentFolder = sAttachmentFolder,
			sAttachmentString = sAttachmentString,
			sMessageFolderMCCID = sMessageFolderMCCID,
			&bGotPlain,
			&iSectionCount,
			sImapMessageType = sImapMessageType,
			bAlterTextTypeEnable = bAlterTextTypeEnable,
			bHtmlContentTypeEnable = bHtmlContentTypeEnable]
			(const std::wstring& _name,
				const std::wstring& _content,
				const std::wstring& _type,
				const std::wstring& source)
		{
			if (!bHtmlContentTypeEnable && _type == L"html" && _content == L"text")
			{
				return;
			}

			std::wstring filename = /*_content + L"_" + _type + L"_" +*/ _name;// sAttachmentFolder + L"\\" + _name;

			std::wstring outputFileName = sAttachmentFolder + L"\\" + filename;
			std::wstring attachFileName = sAttachmentString + L"/" + filename;

			//std::wofstream ofs(sAttachmentFolder + L"\\" + filename, std::wofstream::ios_base::binary);
			//ofs.write(reinterpret_cast<const wchar_t*>(source.c_str()), source.size());
			////ofs << source;
			//ofs.close();

			std::fstream ofs(outputFileName, std::fstream::ios_base::out | std::fstream::ios_base::binary);
			ofs.write(wtos(source).c_str(), source.size());
			ofs.close();



			//std::wstring attachName = 

			//newMail[L"attach"]
			if (_content == L"text" && (_name == L"text.plain" || _name == L"text.html"))
			{
				std::wstring messageUrl = newMail.SafeReadParam(L"MessageUrl", core::checked_type::String, L"").AsWideStr();
				if (!bGotPlain)
				{
					newMail[L"MessageUrl"] = attachFileName;

					if (_name == sImapMessageType)
					{
						bGotPlain = true;
					}

					if (messageUrl.empty())
						return;

					attachFileName = messageUrl;
				}

				if (!bAlterTextTypeEnable)
					return;

				//if (messageUrl.empty())
				//	return;

				//attachFileName = messageUrl;
			}


			//if (_content == L"text")
			//{
			//	////if (_name == L"text.plain")
			//	////{
			//	////	bGotPlain = true;
			//	////	//newMail[L"Source"] = source;
			//	////	newMail[L"MessageUrl"] = attachFileName;

			//	////	return;
			//	////}
			//	////else if (_name == L"text.html" && !bGotPlain)
			//	////{
			//	////	//bGotPlain = true;
			//	////	//newMail[L"Source"] = source;
			//	////	newMail[L"MessageUrl"] = attachFileName;
			//	////}

			//	//if (_name == L"text.html")
			//	//{
			//	//	bGotPlain = true;
			//	//	//newMail[L"Source"] = source;
			//	//	newMail[L"MessageUrl"] = attachFileName;

			//	//	return;
			//	//}
			//	//else if (_name == L"text.plain" && !bGotPlain)
			//	//{
			//	//	//bGotPlain = true;
			//	//	//newMail[L"Source"] = source;
			//	//	newMail[L"MessageUrl"] = attachFileName;
			//	//	return;
			//	//}

			//	if (_name == sImapMessageType)
			//	{
			//		bGotPlain = true;
			//		newMail[L"MessageUrl"] = attachFileName;
			//		return;
			//	}
			//	else if (!bGotPlain && (_name == L"text.plain" | _name == L"text.html"))
			//	{
			//		newMail[L"MessageUrl"] = attachFileName;
			//		return;
			//	}

			//}
			//else
			//{
			int iAttachmentCount = iSectionCount++;

			newMail[std::wstring(L"Attach") + toStr<wchar_t>(iAttachmentCount) + L"Url"] = attachFileName;
			//newMail[std::wstring(L"Attach") + toStr<wchar_t>(iAttachmentCount)+L"Location"] = outputFileName;
			newMail[std::wstring(L"Attach") + toStr<wchar_t>(iAttachmentCount) + L"Type"] = 1;
			newMail[std::wstring(L"Attach") + toStr<wchar_t>(iAttachmentCount) + L"Name"] = filename;
			newMail[std::wstring(L"Attach") + toStr<wchar_t>(iAttachmentCount) + L"Size"] = source.size();
			//}
		});

	}

	newMail[L"AttachCount"] = iSectionCount;



	//std::wstring sSource, sCharset;
	//msg->getPlainTextAndCharset(sSource, sCharset);

	//newMail[L"Source"] = sSource;
	//newMail[L"Charset"] = sCharset;

	this->SendToAny(newMail);

	CMessage postmsg(L"MCC2RT_EMAILSTATE");

	postmsg[L"DateTime"] = core::support::now();
	postmsg[L"MCCID"] = mccid;
	postmsg[L"From"] = msg->getFrom();
	postmsg[L"To"] = msg->getTo();
	postmsg[L"Dir"] = L"I";

	postmsg[L"Host"] = stow(boost::asio::ip::host_name());
	postmsg[L"State"] = L"INBOX";

	this->SendToAll(postmsg);
}

void CDataReceiverImpl::receive_onFailed(const std::string& message)const
{
	int test = 0;
	// ToDo
}


/******************************* eof *************************************/