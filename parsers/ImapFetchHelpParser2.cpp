/************************************************************************/
/* Name     : MCC\ImapFetchHelpParser.cpp                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 1 Jun 2016                                                */
/************************************************************************/

#include "stdafx.h"
#include "ImapFetchHelpParser.h"

//const std::string FAILED_BOX_NAME = "\"FAIL\"";
//
//bool readstatusparser_parse(std::string source, std::function <void(const readstatusparser::TIMAPStatus& answer, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback)
//{
//
//	bool r = readstatusparser::parse(
//		source.begin(),
//		source.end(),
//		_callback);
//
//	return r;
//}
//
//
//
//bool listparser_parse(std::string source, std::function<void(const std::string&box_name, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString)
//{
//	bool r = listparser::parse(
//		source.begin(),
//		source.end(),
//		_callback,
//		errString
//		);
//
//	return r;
//}
//
//bool searchparser_parse(std::string source, std::function<void(const int&i, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string & errString)
//{
//	bool r = searchparser::parse(
//		source.begin(),
//		source.end(),
//		_callback,
//		errString
//		);
//	return r;
//}

//bool fetchallparser_parse(std::string source, std::function<void(const fetchallparser::TDocument&doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string & errString)
//{
//	bool r = fetchallparser::parse(
//		source.begin(),
//		source.end(),
//		_callback,
//		errString
//		);
//
//		return r;
//}

//bool fetchbodystructureparser_parse(std::string source, std::function<void(const fetchbodystructureparser::TDocument&doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string & errString)
//{
//	bool r = fetchbodystructureparser::parse(
//		source.begin(),
//		source.end(),
//		_callback,
//		errString
//		);
//
//		return r;
//}

///******************************* eof *************************************/