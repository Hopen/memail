/************************************************************************/
/* Name     : MEMAIL\IMAP\MailConnectorImpl.cpp                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : MEMAIL\IMAP                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 16 Aug 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/program_options/detail/convert.hpp>

#include "MailConnectorImpl.h"
#include "Log/SystemLog.h"

#include "ImapClient.h"

/*************************************************************/
/********************* CMailConnectorImpl ********************/
/*************************************************************/

CMailConnectorImpl::CMailConnectorImpl(boost::asio::io_service& io)
	:r_io(io)
{
}


CMailConnectorImpl::~CMailConnectorImpl()
{
}



std::atomic<unsigned int> threadsCount{ 0 };

bool CMailConnectorImpl::_getMail(CDataReceiverImpl *_routerInterface,
	TImapParams params) const
{
	singleton_auto_pointer<CSystemLog> log;
	log->LogStringModule(LEVEL_INFO, L"IMAP", L"[CONNECT] [%s:%s] - %s", params._uri.c_str(), params._port.c_str(), params._email.c_str());

	//std::future <unsigned int> run;

	try
	{
		//boost::asio::io_service io_service;
		//boost::asio::ip::tcp::resolver resolver(/*r_io*/io_service);
		//boost::asio::ip::tcp::resolver::query query(wtos(params._uri), wtos(params._port));
		//boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);


		//ImapClientPtr client;
		//if (!params._certificate_uri.empty())
		//{
		//	boost::asio::ssl::context ctx(boost::asio::ssl::context::tlsv12);
		//	log->LogStringModule(LEVEL_INFO, L"IMAP", L"Using SSL Certificate [%s]", params._certificate_uri.c_str());
		//	ctx.load_verify_file(wtos(params._certificate_uri));

		//	//statusHandlerImap accept = boost::bind(&CMailConnectorImpl::_accept, this, _1);
		//	//makeSSL_ImapClient(log->GetInstance());


		//	client = makeSSL_ImapClient(log->GetInstance(), params._login, params._pass, io_service, ctx, iterator,
		//		[pReciever = _routerInterface](MessagePtr ptr)
		//	    {
		//			pReciever->receive_onAccepted(ptr);
		//		},
		//		[pReciever = _routerInterface, _email = params._email](MessagePtr ptr)
		//		{
		//			pReciever->receive_onCompleted(_email, ptr);
		//		},
		//		[pReciever = _routerInterface](const std::string& what)
		//		{
		//			pReciever->receive_onFailed(what);
		//		},
		//			params._peek,
		//			params._afterReadMoveTo);
		//}
		//else
		//{
		//	client = makeNOSSL_ImapClient(log->GetInstance(), params._login, params._pass, io_service, iterator,
		//		[pReciever = _routerInterface](MessagePtr ptr)
		//		{
		//			pReciever->receive_onAccepted(ptr);
		//		},
		//			[pReciever = _routerInterface, _email = params._email](MessagePtr ptr)
		//		{
		//			pReciever->receive_onCompleted(_email, ptr);
		//		},
		//			[pReciever = _routerInterface](const std::string& what)
		//		{
		//			pReciever->receive_onFailed(what);
		//		},
		//			params._peek,
		//			params._afterReadMoveTo);
		//}
		//if (client)
		//	client->start();

		////run = std::async(std::launch::async, boost::bind(&boost::asio::io_service::run, &io_service));
		//unsigned int nThread = ++threadsCount;

		//log->LogStringModule(LEVEL_INFO, L"IMAP", L"ASIO thread #%i has run", nThread);

		//io_service.run(); //wait until asio got a job

		//log->LogStringModule(LEVEL_INFO, L"IMAP", L"ASIO thread #%i has stoped", nThread);
	}
	catch (boost::system::system_error& ec)
	{
		log->LogStringModule(LEVEL_WARNING, L"IMAP", L"[CONNECTION FAILED] [%s:%s] with error: %s", params._uri.c_str(), params._port.c_str(), stow(std::string(ec.what())).c_str());
	}
	catch (...)
	{
		log->LogStringModule(LEVEL_WARNING, L"IMAP", L"[CONNECTION FAILED] [%s:%s]");
	}
	return false;

	//return run;
}



//bool CMailConnectorImpl::_sendMail(CDataReceiverImpl *_routerInterface,
//	MessageCollector& msg_list,
//	const std::wstring & _sSmtpClientAddress,
//	const std::wstring & certificate_uri,
//	const std::wstring & _uri,
//	const std::wstring & _port,
//	const std::wstring & _login,
//	const std::wstring & _pass,
//	bool bSSLEnable) const


bool CMailConnectorImpl::_sendMail(CDataReceiverImpl *_routerInterface,
	MessageCollector msg_list,
	TSmtpParams params
	) const
{
	return false;
}


/******************************* eof *************************************/
