/************************************************************************/
/* Name     : MCC\ImapMessageContext.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 31 Mar 2016                                               */
/************************************************************************/

#include "stdafx.h"
#include "ImapMessageContext.h"

void CSection::doCallbackByParam(const std::wstring & _content, const std::wstring & _type, const std::wstring & _subtype, boost::function<void(const int&)> callback)const
{
	//if (m_content == _content && m_type == _type && m_subtype == _subtype)
	if ( (_content.empty() || (!_content.empty() && m_content == _content)) &&
		 (_type.empty   () || (!_type.empty   () && m_type    == _type   )) &&
		 (_subtype.empty() || (!_subtype.empty() && m_subtype == _subtype))
		)
	{
		callback(m_index);
	}
	for each (BodyPtr body in m_children)
	{
		body->doCallbackByParam(_content, _type, _subtype, callback);
	}

}

//void CSection::getSectionIndex(int parent_index, std::string s_parent_index, boost::function<void(const std::wstring&)> callback) const
//{
//	if (m_index != 0)
//	{
//		if (!getContent().empty())
//		{
//			std::wstring index;
//			if (parent_index == 0) //parent index
//			{
//				index = toStr<wchar_t>(m_index);
//			}
//			else
//			{
//				index = toStr<wchar_t>(parent_index) + L"." + toStr<wchar_t>(m_index);
//			}
//
//
//			callback(index);
//		}
//
//		parent_index = m_index;
//		if (!s_parent_index.empty())
//		{
//			s_parent_index += ".";
//		}
//		s_parent_index += toStr<char>(m_index);
//	}
//
//
//	for each (BodyPtr body in m_children)
//	{
//		body->getSectionIndex(parent_index, s_parent_index, callback);
//	}
//
//}

void CSection::getSectionIndex(int parent_index, std::wstring s_parent_index, boost::function<void(const std::wstring&)> callback) const
{
	if (m_index != 0)
	{
		parent_index = m_index;
		if (!s_parent_index.empty())
		{
			s_parent_index += L".";
		}
		s_parent_index += toStr<wchar_t>(m_index);

		if (!getContent().empty())
		{
			callback(s_parent_index);
		}

	}


	for each (BodyPtr body in m_children)
	{
		body->getSectionIndex(parent_index, s_parent_index, callback);
	}

}

template<class value_type>
value_type toNum(std::string _in)
{
	value_type num(0);
	std::istringstream inn(_in);

	inn >> num;
	return num;
}


BodyPtr CSection::getSectionByIndex(int parent_index, const std::string& index)
{
	std::string newIndex = index;
	if (m_index != 0 && index.size())
	{
		//if (index.size() == 1)
		//{
		//	if (m_index == toNum<int>(index))
		//		return shared_from_this();

		//	return nullptr;
		//}
		//else
		//{
		//	int pos = index.find(".");
		//	if (pos > 0)
		//	{
		//		std::string parent = index.substr(0, pos);
		//		std::string child = index.substr(pos + 1, index.length() - 1);

		//		if (m_index != toNum<decltype(m_index)>(parent))
		//			return nullptr;

		//		newIndex = child;
		//	}

		//}
		int pos = index.find(".");

		if (pos < 0)
		{
			if (m_index == toNum<int>(index))
				return shared_from_this();

			return nullptr;
		}
		else
		{
			//int pos = index.find(".");
			//if (pos > 0)
			{
				std::string parent = index.substr(0, pos);
				std::string child = index.substr(pos + 1, index.length() - 1);

				if (m_index != toNum<decltype(m_index)>(parent))
					return nullptr;

				newIndex = child;
			}

		}


	}


	for each (BodyPtr body in m_children)
	{
		BodyPtr result = body->getSectionByIndex(parent_index, newIndex);
		if (result.get())
			return result;
	}

	return nullptr;
}

void CSection::getSectionsSource(boost::function<void(const std::wstring& _name, const std::wstring& _content, const std::wstring& _type, const std::wstring& source)> callback) const
{
	if ((m_content == L"text") ||
		(m_content == L"image") ||
		(m_content == L"application") ||
		(m_content == L"video") ||
		(m_content == L"audio") ||
		(m_content == L"message")
		)
	{
		BodyPtr ptrBody = getFirstChild();
		if (ptrBody)
		{
			std::wstring source = ptrBody->getSource();
			if (!source.empty() && !ptrBody->getName().empty())
			{
				callback(ptrBody->getName(), m_content, m_type, source);
			}
		}

	}

	for each (BodyPtr body in m_children)
	{
		body->getSectionsSource(callback);
	}


}


void CIMAPMessage::setSectionSource(const std::string& _index, const std::string& _source)
{
	BodyPtr ptrSection = m_section->getSectionByIndex(m_section->getIndex(), _index);
	if (ptrSection)
	{
		CSection *section = dynamic_cast<CSection *>(ptrSection.get());
		if (section)
		{
			BodyPtr ptrBody = section->getFirstChild();
			if (ptrBody)
			{
				ptrBody->setSource(_source);
			}
		}
	}
}

//bool CIMAPMessage::getSectionSourceAndCharsetByIndex(const unsigned int& _index, std::wstring& _source, std::wstring& _charset)
//{
//	BodyPtr ptrSection = m_section->getSectionByIndex(_index);
//	if (ptrSection)
//	{
//		CSection *section = dynamic_cast<CSection *>(ptrSection.get());
//		if (section)
//		{
//			BodyPtr ptrBody = section->getFirstChild();
//			if (ptrBody)
//			{
//				_source = ptrBody->getSource();
//				CText * text = dynamic_cast<CText *>(ptrBody.get());
//				if (text)
//					_charset = text->getCharser();
//
//				return true;
//			}
//
//		}
//	}
//
//	return false;
//}


//int CIMAPMessage::getSectionIndexByContentType(const std::wstring& content, const std::wstring& type)const
//{
//	int index = 0;
//	m_section->getIndexByContentType(content, type, index);
//
//	return index;
//}

//void CBody::getSectionsSource(boost::function<void(const std::wstring&, const std::wstring&)> callback)const
//{
//	callback(m_name, m_source);
//}

//void CIMAPMessage::getSectionsToDownload(boost::function<void(const int&)> callback)
//{
//	//int plain_index = getSectionIndexByContentType(L"text", L"plain");
//	//if (plain_index == 0)
//	//	plain_index = getSectionIndexByContentType(L"text", L"html");
//
//	//if (callback && plain_index)
//	//	callback(plain_index);
//
//	m_section->doCallbackByParam(L"text", L"plain", L"", callback);
//
//	if (!getWaitingSessionCount())
//		m_section->doCallbackByParam(L"text", L"html", L"", callback);
//
//	m_section->doCallbackByParam(L"", L"", L"mixed", callback);
//}

void CIMAPMessage::getSectionsToDownload(boost::function<void(const std::wstring&)> callback)
{
	m_section->getSectionIndex(0, L"", callback);
}


void CIMAPMessage::getSectionsSource(boost::function<void(const std::wstring& _name, const std::wstring& _content, const std::wstring& _type, const std::wstring& source)> callback) const
{
	m_section->getSectionsSource(callback);
}

/******************************* eof *************************************/

