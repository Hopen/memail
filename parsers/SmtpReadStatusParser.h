/************************************************************************/
/* Name     : MCC\SmtpReadStatusParser.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 22 Mar 2016                                               */
/************************************************************************/

#pragma once

//#define PARSER_ENABLE

//#include <string>
//#include <functional>
//
//#include <boost/format.hpp>
//#  pragma warning(disable:4348)
//#include <boost/config/warning_disable.hpp>
//#include <boost/spirit/include/qi.hpp>
//#include "Patterns/string_functions.h"
//
//#ifdef PARSER_ENABLE
//#include <boost/spirit/include/phoenix_operator.hpp>
//#include <boost/spirit/include/phoenix_fusion.hpp>
//#include <boost/fusion/include/adapt_struct.hpp>
//#include <boost/variant/recursive_variant.hpp>
//#include <boost/spirit/include/phoenix_stl.hpp>
//#include <boost/spirit/include/qi_no_skip.hpp>
//#endif

namespace smtpreadstatusparser
{
	struct TCommand
	{
		int code;
		std::string data;
	};


	//struct TCommandList
	//{
	//	std::vector<TCommand> commandList;
	//};


};

BOOST_FUSION_ADAPT_STRUCT(
	smtpreadstatusparser::TCommand,
	(int, code)
	(std::string, data)
	)
//
//BOOST_FUSION_ADAPT_STRUCT(
//	smtpreadstatusparser::TCommandList,
//	(std::vector<smtpreadstatusparser::TCommand>, commandList)
//	)
//
namespace smtpreadstatusparser
{
	using ParserCallBack = std::function <void(const TCommand& answer, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)>;

	template <typename Iterator>
	bool parse(Iterator first, Iterator last, ParserCallBack _callback)
	{
#ifdef PARSER_ENABLE
		// rules
		boost::spirit::qi::rule<Iterator, std::string(), boost::spirit::ascii::space_type> name;
		boost::spirit::qi::rule<Iterator, TCommand(), boost::spirit::ascii::space_type> command;
		//boost::spirit::qi::rule<Iterator, TCommandList(), boost::spirit::ascii::space_type> commandList;

		name = boost::spirit::no_skip[+~boost::spirit::qi::char_('\r\n')]
			;


		command = boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])]
			>> -boost::spirit::lit('-')
			>> name[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1];


		//commandList = *command[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)];

		//TCommand output;

		bool r = boost::spirit::qi::phrase_parse(
			first,
			last,
			(
				//commandList[_callback]
				*command[_callback]
				),
			boost::spirit::ascii::space/*,
			output*/
			);

		if (first != last) // fail if we did not get a full match
			return false;

		return r;

#endif
		return false;
	}
};

/******************************* eof *************************************/
