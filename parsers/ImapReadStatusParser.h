/************************************************************************/
/* Name     : MCC\ImapReadStatusParser.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 14 Mar 2016                                               */
/************************************************************************/

#pragma once

namespace readstatusparser
{
	struct TIMAPStatus
	{
		int _number;
		std::string _status;
		std::string _extra;
		std::string _command_name;
		std::string _completed;
	};

}


bool readstatusparser_parse(std::string source, std::function <void(const readstatusparser::TIMAPStatus& answer, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback);

/******************************* eof *************************************/