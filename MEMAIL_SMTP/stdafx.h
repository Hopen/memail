// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>



// TODO: reference additional headers your program requires here
#define PARSER_ENABLE

#include <string>
#include <functional>

#include <boost/format.hpp>
#  pragma warning(disable:4348)
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include "Patterns/string_functions.h"

#ifdef PARSER_ENABLE
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/qi_no_skip.hpp>
#endif


//#include "ImapFetchBodyContentParser.h"
//#include "ImapFetchParser.h"
//#include "ImapSearchParser.h"
//#include "ImapSubjectParser.h"
//#include "ImapReadStatusParser.h"
//#include "ImapListParser.h"
#include "SmtpReadStatusParser.h"
//#include "ImapFetchBodyStructureParser.h"
//#include "ImapFetchAllParser.h"