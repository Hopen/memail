/************************************************************************/
/* Name     : MCC\ImapFetchAllParser.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 27 May 2016                                               */
/************************************************************************/

#pragma once

namespace fetchallparser
{
	struct TDate
	{
		int day;
		std::string month;
		int year;

		std::wstring makeDate()const
		{
			std::wstring s_date = (boost::wformat(L"%i %s %i") % day % month.c_str() % year).str();
			return s_date;
		}

	};

	struct TTime
	{
		int hours;
		int minutes;
		int seconds;

		std::wstring addZero(int time)const
		{
			std::wstring s_time = (time > 9) ? (boost::wformat(L"%i") % time).str() : (boost::wformat(L"0%i") % time).str();
			return s_time;
		}


		std::wstring makeTime()const
		{
			std::wstring time = (boost::wformat(L"%s:%s:%s") % addZero(hours).c_str() % addZero(minutes).c_str() % addZero(seconds).c_str()).str();

			return time;
		}
	};

	struct TUtc
	{
		char sing;
		int timezone;
	};

	struct TDateTime
	{


		TDate date;
		TTime time;
		TUtc utc;

		auto getUtc()const
		{
			if (utc.sing == '+')
				return static_cast<int>(static_cast<double>(utc.timezone) / 100);
			else
				return (-1) * static_cast<int>(static_cast<double>(utc.timezone) / 100);
		}

		auto makeDateTime(const int _utc = 0)const
		{
			std::map<std::string, int> months;
			months["Jan"] = 0;
			months["Feb"] = 1;
			months["Mar"] = 2;
			months["Apr"] = 3;
			months["May"] = 4;
			months["Jun"] = 5;
			months["Jul"] = 6;
			months["Aug"] = 7;
			months["Sep"] = 8;
			months["Oct"] = 9;
			months["Nov"] = 10;
			months["Dec"] = 11;

			std::tm tm = {};

			tm.tm_hour = time.hours;
			tm.tm_min = time.minutes;
			tm.tm_sec = time.seconds;
			tm.tm_year = date.year - 1900;
			tm.tm_mday = date.day;
			tm.tm_mon = months.at(date.month);

			tm.tm_hour += _utc;

			return core::time_type::clock::from_time_t(_mkgmtime(&tm));
		}
	};

	struct TAddress
	{
		std::string name;
		std::string box_name;
		std::string domain;

		std::wstring makeAddress()const
		{
			return stow(box_name) + L"@" + stow(domain);
		}
	};


	struct TAddressList
	{
		std::vector<TAddress> addressList;

		std::wstring makeAddress()const
		{
			std::wstring addresses;

			for each (auto address in addressList)
			{
				addresses += address.makeAddress() + L";";
			}

			return addresses;
		}
	};

	struct TEnvelope
	{
		TDateTime send_date;
		std::string subject;
		TAddress from;
		TAddressList to;
		TAddressList cc;

		std::string getSubject()const { return subject; }
	};

	struct TRFC822
	{
		int size;
	};

	struct THeader
	{
		int uid;
		std::string flag;
		TDateTime receive_date; //INTERNALDATE
		TRFC822 size; //RFC822.SIZE
		TEnvelope envelope; //ENVELOPE
	};

	struct TDocument
	{
		int num;
		THeader header;

	};


};


bool fetchallparser_parse(std::string source, std::function <void(const fetchallparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);
bool fetchallparser_parse2(std::string source, std::function <void(const fetchallparser::TDocument& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);


/******************************* eof *************************************/