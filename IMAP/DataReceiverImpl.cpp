/************************************************************************/
/* Name     : MEMAIL\DataReceiverImpl.cpp                               */
/* Author   : Andrey Alekseev                                           */
/* Project  : MEMAIL                                                    */
/* Company  : Expert Solutions                                          */
/* Date     : 15 Aug 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include <future>
#include <type_traits>

#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include "DataReceiverImpl.h"
#include "api/ProcessHelper.h"
#include "MailConnectorImpl.h"

const int EXPIREDTIME = 30;

CDataReceiverImpl::CDataReceiverImpl(boost::asio::io_service& io) :
	m_log(NULL),
	r_io(io)
{
	m_idapConnector = std::make_unique<CMailConnectorImpl>(r_io);
}


CDataReceiverImpl::~CDataReceiverImpl()
{
	int test = 0;
}

int CDataReceiverImpl::init_impl(CSystemLog* pLog)
{
	if (!pLog)
		return -1111;

	m_log = pLog;

	m_log->LogString(LEVEL_FINEST, L"CDataReceiverImpl initializing...");

	boost::asio::spawn(r_io, [&](boost::asio::yield_context yield)
	{
		boost::asio::steady_timer timer(r_io);

		timer.expires_from_now(std::chrono::milliseconds(100));
		timer.async_wait(yield); //wait for 10 seconds

		start();
	});

	return 0;
}

int CDataReceiverImpl::start()
{
	SystemConfig settings;

	int iLongPoolTimer = settings->safe_get_config_int(_T("ExpiredTime"), EXPIREDTIME);

	TImapParams params;

	std::list <TImapParams> params_list;

	params._uri = settings->safe_get_config_value<std::wstring>(_T("IMAPServerName"), L"127.0.0.1");
	params._port = settings->safe_get_config_value<std::wstring>(_T("IMAPPort"), L"993");
	params._sslEnable = settings->safe_get_config_bool(L"SSLEnable", false);

	std::wstring pem_location = extra_api::CProcessHelper::GetCurrentFolderName();

	auto imap_node = settings->get_config_node(std::wstring(L"IMAP"));

	try
	{
		for (const auto& child : imap_node)
		{
			if (child.first == L"<xmlcomment>")
				continue;

			std::wstring sertificate = params._sslEnable ? settings->get_config_attr<std::wstring>(child.second, L"sslcertificate") : L"";
			if (!sertificate.empty())
				sertificate = pem_location + sertificate;

			params._certificate_uri = sertificate;
			params._email = settings->get_config_attr<std::wstring>(child.second, L"name");
			params._login = settings->get_config_attr<std::wstring>(child.second, L"login");
			params._pass = settings->get_config_attr<std::wstring>(child.second, L"password");
			params._peek = settings->get_config_bool(child.second, L"peek");
			params._afterReadMoveTo = settings->get_config_attr<std::wstring>(child.second, L"afterReadMoveTo");

			params_list.push_back(params);
		}
	}
	catch (const core::configuration_error& error)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading config key: %s", stow(error.what()));
	}


	try
	{
		boost::asio::spawn(r_io, [this,
			iLongPoolTimer,
			p_io = &r_io,
			params_list = std::move(params_list),
			m_log = m_log, 
			connector = m_idapConnector.get(),
			pMail = m_idapConnector.get()](boost::asio::yield_context yield)
		{

			boost::asio::steady_timer timer(*p_io);
			boost::system::error_code errorCode;

			int initTimerValue = 3;

			while (!(*p_io).stopped())
			{
				timer.expires_from_now(std::chrono::seconds(initTimerValue));
				timer.async_wait(yield[errorCode]); //wait for timer
				
				m_log->LogStringModule(LEVEL_FINEST, L"steady timer", L"========================Tick===================");
				
				if (errorCode)
				{
					throw errorCode;
				}

				initTimerValue = iLongPoolTimer;

				std::vector <std::future <bool>> tasks;

				for (auto& params : params_list)
				{
					tasks.emplace_back(std::async(std::launch::async, [pMail, this, params = std::move(const_cast<TImapParams&>(params))]()
					{
						return pMail->GetMail(this, std::move(params));
					}));
				}

			}
		});

	}
	catch (std::exception& e) {
		m_log->LogString(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
	}

	return 0;
}


void CDataReceiverImpl::set_Subscribes()
{
	//subscribe(L"S2MCC_Reply", &CDataReceiverImpl::onReplyEventHandler);
}

void CDataReceiverImpl::stop()
{
	r_io.stop();
}

void CDataReceiverImpl::on_routerConnect()
{
	m_log->LogStringModule(LEVEL_INFO, L"Receiver Core", L"Router has connected");
}

void CDataReceiverImpl::on_routerDeliverError(CMessage& msg, const CLIENT_ADDRESS &, const CLIENT_ADDRESS &)
{
	m_log->LogStringModule(LEVEL_FINE, L"Router client", msg.Dump().c_str());
}

void CDataReceiverImpl::on_appStatusChange(DWORD dwClient, core::ClientStatus nStatus)
{
	if (nStatus == (core::ClientStatus::CS_DISCONNECTED))
	{
		try
		{

		}
		catch (...)
		{

		}
	}

}



/******************************* eof *************************************/