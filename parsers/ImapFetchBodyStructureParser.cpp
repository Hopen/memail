/************************************************************************/
/* Name     : MCC\ImapFetchBodyStructureParser.cpp                      */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 1 Jun 2016                                                */
/************************************************************************/
#include "stdafx.h"
#include "ImapFetchBodyStructureParser.h"

//#define PARSER_ENABLE

//#include <string>
//#include <map>
//#include <functional>
//
//#include <boost/format.hpp>
//#  pragma warning(disable:4348)
//#include <boost/config/warning_disable.hpp>
//#include <boost/spirit/include/qi.hpp>
//#include "Patterns/string_functions.h"
//#include "Patterns/time_functions.h"
//
//#ifdef PARSER_ENABLE
//#include <boost/spirit/include/phoenix_operator.hpp>
//#include <boost/spirit/include/phoenix_fusion.hpp>
//#include <boost/fusion/include/adapt_struct.hpp>
//#include <boost/variant/recursive_variant.hpp>
//#include <boost/spirit/include/phoenix_stl.hpp>
//#include <boost/spirit/include/qi_no_skip.hpp>
//#endif

#include "ImapMessageContext.h"

namespace fetchbodystructureparser
{
	struct TIMAPStatus
	{
		int _number;
		std::string _status;
		std::string _extra;
	};

}


namespace fetchbodystructureparser
{
	class _lineanConverter
	{
	public:
		_lineanConverter(SectionPtr _body, int sectionNum)
			:m_body(_body), m_iSectionNum(sectionNum)
		{
		}

		void operator()(TBody const& body) const;
	private:
		SectionPtr m_body;
		mutable int m_iSectionNum{ 0 };
	};


	class _nodeLineanConverter : public boost::static_visitor<>
	{
	public:
		_nodeLineanConverter(SectionPtr _section, int sectionNum)
			:m_pSection(_section), m_iSectionNum(sectionNum)
		{
		}

		void operator()(TBody const& body) const
		{
			if (m_pSection)
			{
				//CSection * parentSection = dynamic_cast<CSection*>(m_pSection.get());

				SectionPtr newSection = std::make_shared<CSection>(stow(body.subType.subType));
				m_pSection->push_back(newSection);

				//m_pSection->setIndex(0);
				newSection->setIndex(m_iSectionNum);

				//newSection->setIndex(m_pSection->getChildrenSize());

				_lineanConverter printer(newSection, 0);
				printer(body);
			}
		}

		void operator()(TSection const& section) const
		{
			if (m_pSection)
			{
				//CSection * section2 = dynamic_cast<CSection*>(m_pSection.get());

				m_pSection->setContent(section.getContentType());
				m_pSection->setType(section.getSubType());

				if (m_pSection->getIndex() == 0)//just one section
					m_pSection->setIndex(1);

				//m_pSection->setIndex(m_iSectionNum);


				if (section.contentType == "text")
				{
					std::wstring textName = section.body_param.getParamByName_Decode("name");
					if (textName.empty())
						textName = section.attach_param.params.getParamByName_Decode("filename");

					if (textName.empty())
						textName = section.getContentType() + L"." + section.getSubType();

					m_pSection->push_back(std::make_shared<CText>(section.getSize(), textName, section.body_param.getParamByName("charset"), section.getEncoding()));
				}
				else if (section.contentType == "image")
				{
					std::wstring contentName;
					contentName = section.body_param.getParamByName_Decode("name");
					if (contentName.empty())
						contentName = section.attach_param.params.getParamByName_Decode("filename");
					m_pSection->push_back(std::make_shared<CImage>(section.getSize(), contentName, section.getEncoding()));
				}
				else if ((section.contentType == "application") ||
					(section.contentType == "video") ||
					(section.contentType == "audio"))
				{
					std::wstring contentName;
					contentName = section.body_param.getParamByName_Decode("name");
					if (contentName.empty())
						contentName = section.attach_param.params.getParamByName_Decode("filename");

					m_pSection->push_back(std::make_shared<CApplication>(section.getSize(), contentName, section.getEncoding()));
				}
				else if (section.contentType == "message")
				{
					std::wstring name = section.body_param.getParamByName_Decode("name");
					if (name.empty())
						name = section.attach_param.params.getParamByName_Decode("filename");

					if (!name.empty())
						name += L".eml";
					else
						name = L"tmp.eml";

					m_pSection->push_back(std::make_shared<CMessageMfc>(section.getSize(), name, section.getEncoding()));
				}
				else
				{
					// ToDo
				}

				//m_pList->push_back(section);
			}
		}

		static void reset() { s_iCounter = 0; }
	private:

		mutable int m_iSectionNum{ 0 };

		static int s_iCounter;
		//sectionList *m_pList;
		//CSection * m_pSection;
		SectionPtr m_pSection;
	};

	int _nodeLineanConverter::s_iCounter = 0;


	SectionPtr TBody::GetSections()const
	{
		SectionPtr section = std::make_shared<CSection>();
		//section->setIndex(1);

		_lineanConverter converter(section, 0);
		converter(*this);

		_nodeLineanConverter::reset();

		return section;
	}

	void _lineanConverter::operator()(TBody const & body) const
	{
		BOOST_FOREACH(body_node const& node, body.sections)
		{
			boost::apply_visitor(_nodeLineanConverter(m_body, ++m_iSectionNum), node);
		}

	}
}

#ifdef PARSER_ENABLE

BOOST_FUSION_ADAPT_STRUCT(
	fetchbodystructureparser::TBodyParam2,
	(std::string, name)
	(std::string, value)
	)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchbodystructureparser::TEncoding,
		(std::string, name)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchbodystructureparser::TBodyParamList2,
		(std::vector<fetchbodystructureparser::TBodyParam2>, params)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchbodystructureparser::TAttacnmentParams,
		(std::string, type)
		(fetchbodystructureparser::TBodyParamList2, params)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchbodystructureparser::TSection,
		(std::string, contentType)
		(std::string, subType)
		(fetchbodystructureparser::TBodyParamList2, body_param)
		(fetchbodystructureparser::TEncoding, encoding)
		(int, size)
		(fetchbodystructureparser::TAttacnmentParams, attach_param)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchbodystructureparser::TBodySubtype,
		(std::string, subType)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchbodystructureparser::TBody,
		(std::vector <fetchbodystructureparser::body_node>, sections)
		(fetchbodystructureparser::TBodySubtype, subType)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchbodystructureparser::TBodyStructure,
		(int, uid)
		(fetchbodystructureparser::TBody, body)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchbodystructureparser::TDocument,
		(int, num)
		(fetchbodystructureparser::TBodyStructure, structure)
		)



	BOOST_FUSION_ADAPT_STRUCT(
		fetchbodystructureparser::TContainer,
		(std::vector <fetchbodystructureparser::container_node>, sections)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchbodystructureparser::TSource,
		(std::string, ss)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchbodystructureparser::TIMAPStatus,
		(int, _number)
		(std::string, _status)
		(std::string, _extra)
		)


#endif

namespace fetchbodystructureparser
{
	using ParserCallBack = std::function <void(const TDocument& body, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)>;

	template <typename Iterator>
	bool parse(Iterator first, Iterator last, ParserCallBack _callback, std::string& e_what)
	{
#ifdef PARSER_ENABLE

		// rules
		boost::spirit::qi::rule<Iterator, std::string(), boost::spirit::ascii::space_type> nil, name, utf8, content, type, content_type, code, subtype, name2, sample;
		boost::spirit::qi::rule<Iterator, TBodyParam2(), boost::spirit::ascii::space_type> body_param, body_nil_param;
		boost::spirit::qi::rule<Iterator, TBodyParamList2(), boost::spirit::ascii::space_type> body_param_list;
		boost::spirit::qi::rule<Iterator, TAttacnmentParams(), boost::spirit::ascii::space_type> attach_param;
		boost::spirit::qi::rule<Iterator, TSection(), boost::spirit::ascii::space_type> section;
		boost::spirit::qi::rule<Iterator, TEncoding(), boost::spirit::ascii::space_type> encoding;
		boost::spirit::qi::rule<Iterator, TBodySubtype(), boost::spirit::ascii::space_type> body_subtype;
		boost::spirit::qi::rule<Iterator, TBody(), boost::spirit::ascii::space_type> body;
		boost::spirit::qi::rule<Iterator, body_node(), boost::spirit::ascii::space_type> node;
		boost::spirit::qi::rule<Iterator, TBodyStructure(), boost::spirit::ascii::space_type> structure;
		boost::spirit::qi::rule<Iterator, TSource(), boost::spirit::ascii::space_type> source;
		boost::spirit::qi::rule<Iterator, TContainer(), boost::spirit::ascii::space_type> container;
		boost::spirit::qi::rule<Iterator, container_node(), boost::spirit::ascii::space_type> cnode;
		boost::spirit::qi::rule<Iterator, TDocument(), boost::spirit::ascii::space_type> document;

		nil = boost::spirit::qi::string("NIL");


		name = '\"'
			>> -boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
			>> '\"';

		name2 = boost::spirit::lit('{')
			>> boost::spirit::qi::int_
			>> boost::spirit::lit('}')
			//>> boost::spirit::lit('\r')
			//>> boost::spirit::lit('\n')
			>> (boost::spirit::no_skip[+~boost::spirit::qi::char_("(\"")]);
		//>> boost::spirit::qi::string("?=");

		utf8 = '\"'
			>> boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
			>> '\"';

		content = '\"'
			>> (
				boost::spirit::ascii::no_case[boost::spirit::qi::string("text")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("image")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("audio")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("video")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("application")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("multipart")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("message")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("unknown")] /*|
				boost::spirit::qi::string("TEXT") |
				boost::spirit::qi::string("IMAGE") |
				boost::spirit::qi::string("AUDIO") |
				boost::spirit::qi::string("VIDEO") |
				boost::spirit::qi::string("APPLICATION") |
				boost::spirit::qi::string("MULTIPART") |
				boost::spirit::qi::string("MESSAGE") |
				boost::spirit::qi::string("Text") |
				boost::spirit::qi::string("Image") |
				boost::spirit::qi::string("Audio") |
				boost::spirit::qi::string("Video") |
				boost::spirit::qi::string("Application") |
				boost::spirit::qi::string("Multipart") |
				boost::spirit::qi::string("Message") |
				boost::spirit::qi::string("UNKNOWN") |
				boost::spirit::qi::string("Unknown") */

				)
			>> '\"';

		content_type = name;

		subtype = '\"'
			>> (
				boost::spirit::ascii::no_case[boost::spirit::qi::string("mixed")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("alternative")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("related")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("relative")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("report")] |
				boost::spirit::ascii::no_case[boost::spirit::qi::string("signed")] //| 
				//boost::spirit::qi::string("Mixed") |
				//boost::spirit::qi::string("Alternative") |
				//boost::spirit::qi::string("Related") |
				//boost::spirit::qi::string("Report") |
				//boost::spirit::qi::string("MIXED") |
				//boost::spirit::qi::string("ALTERNATIVE") |
				//boost::spirit::qi::string("RELATED") |
				//boost::spirit::qi::string("REPORT")|
				//boost::spirit::qi::string("Signed") |
				//boost::spirit::qi::string("SIGNED")

				)
			>> '\"';

		code = name;

		body_param = name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> name[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1];

		body_nil_param = (nil | name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])
			>> (nil | name[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]);

		body_param_list = *body_param[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)];

		attach_param = (nil | (boost::spirit::lit('(') 
								>>(name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
									>> (nil | (boost::spirit::lit('(')
											>> body_param_list[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
											>> boost::spirit::lit(')')
											)
										)
									)
								>> boost::spirit::lit(')')
								)
			)
			//>> nil
			>> -(nil |
				('('
					>> name
					>> ')')
				)
			>> -nil
			;

		source = name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1] | name2 | nil | boost::spirit::qi::int_;

		cnode = (container | source)[boost::spirit::_val = boost::spirit::qi::_1];

		container = '('
			>> *cnode[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)]
			>> ')';



		encoding = (name | nil)
			>> (name2 | name | nil)
			>> name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			;

		section = /*boost::spirit::lit('(')
			>>*/ content[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> content_type[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> (nil |
				(boost::spirit::lit('(')
					>> body_param_list[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
					>> boost::spirit::lit(')')
					)
				)
			>> encoding[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> boost::spirit::qi::int_[boost::phoenix::at_c<4>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> *container
			>> -boost::spirit::qi::int_
			>> nil
			//>> (nil | (boost::spirit::lit('(')
			//		>>attach_param[boost::phoenix::at_c<5>(boost::spirit::_val) = boost::spirit::qi::_1]
			//		>> boost::spirit::lit(')')
			//		)
			//	)
			//>> *nil
			>> attach_param[boost::phoenix::at_c<5>(boost::spirit::_val) = boost::spirit::qi::_1]
			;


		body_subtype = subtype[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> '('
			>> name 
			>> (nil | name)
			>> *name
			>> ')'
			>> - (nil | (boost::spirit::lit('(')
						>> body_nil_param
						>> boost::spirit::lit(')'))
				)
			>> -(nil | 
				('('
				 >> name
				 >>')')
				)
			;

		node = (body | section)[boost::spirit::_val = boost::spirit::qi::_1];


		body = ( boost::spirit::lit('(')
					>> *node[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)]
					>> boost::spirit::lit(')') 
				)
			>> -body_subtype[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			;

		structure = *("UID" > boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			| "BODYSTRUCTURE" > 
			( -body[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> -( '(' 
				>> body_subtype
				>> ')'))
			
			)
			;


		document = '*'
			>> boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> "FETCH"
			>> '('
			>> structure[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> ')'
			>> -('*' >> boost::spirit::qi::int_ >> "EXISTS")
			>> -('*' >> boost::spirit::qi::int_ >> "RECENT");

		TDocument output;
		//TBody output;

		bool r = false;

		try
		{
			r = boost::spirit::qi::phrase_parse(
				first,
				last,
				(
					document[_callback]
					),
				boost::spirit::ascii::space,
				output
				);
		}
		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
		{
			std::string error(e.first, e.last);
			e_what = error;

		}
		catch (const std::exception& e)
		{
			e_what = e.what();
		}
		catch (...)
		{
			e_what = "Unknown exception";
		}

		if (first != last) // fail if we did not get a full match
			return false;

		return r;
#else

		return false;
#endif
	}

	template <typename Iterator>
	bool parse2(Iterator first, Iterator last, ParserCallBack _callback, std::string& e_what)
	{
#ifdef PARSER_ENABLE

		// rules
		boost::spirit::qi::rule<Iterator, std::string(), boost::spirit::ascii::space_type> nil, name, utf8, content, type, content_type, code, subtype, name2, sample;
		//boost::spirit::qi::rule<Iterator, TBodyParam2(), boost::spirit::ascii::space_type> body_param;
		//boost::spirit::qi::rule<Iterator, TBodyParamList2(), boost::spirit::ascii::space_type> body_param_list;
		//boost::spirit::qi::rule<Iterator, TAttacnmentParams(), boost::spirit::ascii::space_type> attach_param;
		boost::spirit::qi::rule<Iterator, TSection(), boost::spirit::ascii::space_type> section;
		//boost::spirit::qi::rule<Iterator, TEncoding(), boost::spirit::ascii::space_type> encoding;
		//boost::spirit::qi::rule<Iterator, TBodySubtype(), boost::spirit::ascii::space_type> body_subtype;
		boost::spirit::qi::rule<Iterator, TBody(), boost::spirit::ascii::space_type> body;
		boost::spirit::qi::rule<Iterator, body_node(), boost::spirit::ascii::space_type> node;
		boost::spirit::qi::rule<Iterator, TBodyStructure(), boost::spirit::ascii::space_type> structure;
		//boost::spirit::qi::rule<Iterator, TSource(), boost::spirit::ascii::space_type> source;
		//boost::spirit::qi::rule<Iterator, TContainer(), boost::spirit::ascii::space_type> container;
		//boost::spirit::qi::rule<Iterator, container_node(), boost::spirit::ascii::space_type> cnode;
		boost::spirit::qi::rule<Iterator, TDocument(), boost::spirit::ascii::space_type> document;
		//boost::spirit::qi::rule<Iterator, std::string(), boost::spirit::ascii::space_type> type2;
		//boost::spirit::qi::rule<Iterator, TIMAPStatus(), boost::spirit::ascii::space_type> answer;

		//type2 = +(boost::spirit::qi::string("OK") |
		//	boost::spirit::qi::string("BAD") |
		//	boost::spirit::qi::string("NO"));

		//answer = boost::spirit::lit('a')
		//	>> (boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])
		//	>> type2[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
		//	>> +(boost::spirit::qi::char_/*[boost::phoenix::at_c<2>(boost::spirit::_val) += boost::spirit::qi::_1]*/)
		//	;


		//nil = boost::spirit::qi::string("NIL");


		name = '\"'
			>> boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
			>> '\"';

		//name2 = boost::spirit::lit('{')
		//	>> boost::spirit::qi::int_
		//	>> boost::spirit::lit('}')
		//	//>> boost::spirit::lit('\r')
		//	//>> boost::spirit::lit('\n')
		//	>> (boost::spirit::no_skip[+~boost::spirit::qi::char_("(\"")]);
		////>> boost::spirit::qi::string("?=");

		//utf8 = '\"'
		//	>> boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
		//	>> '\"';

		content = '\"'
			>> (
				boost::spirit::qi::string("text") |
				boost::spirit::qi::string("Text") |
				boost::spirit::qi::string("image") |
				boost::spirit::qi::string("audio") |
				boost::spirit::qi::string("video") |
				boost::spirit::qi::string("application") |
				boost::spirit::qi::string("multipart") |
				boost::spirit::qi::string("message")
				)
			>> '\"';

		content_type = name;

		//subtype = '\"'
		//	>> (
		//		boost::spirit::qi::string("mixed") |
		//		boost::spirit::qi::string("alternative") |
		//		boost::spirit::qi::string("related") |
		//		boost::spirit::qi::string("report")
		//		)
		//	>> '\"';

		//code = name;

		//body_param = name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
		//	>> name[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1];

		//body_param_list = *body_param[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)];

		//attach_param = name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
		//	>> (nil | (boost::spirit::lit('(')
		//		>> body_param_list[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
		//		>> boost::spirit::lit(')')
		//		)
		//		);


		//source = name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1] | nil | boost::spirit::qi::int_;

		//cnode = (container | source)[boost::spirit::_val = boost::spirit::qi::_1];

		//container = '('
		//	>> *cnode[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)]
		//	>> ')';



		//encoding = (name | nil)
		//	>> (name2 | name | nil)
		//	>> name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
		//	;

		section = content[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> content_type[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			//>> (nil |
			//	(boost::spirit::lit('(')
			//		>> body_param_list[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
			//		>> boost::spirit::lit(')')
			//		)
			//	)
			//>> encoding[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
			//>> boost::spirit::qi::int_[boost::phoenix::at_c<4>(boost::spirit::_val) = boost::spirit::qi::_1]
			//>> *container
			//>> -boost::spirit::qi::int_
			//>> nil
			//>> (nil | (boost::spirit::lit('(')
			//	>> attach_param[boost::phoenix::at_c<5>(boost::spirit::_val) = boost::spirit::qi::_1]
			//	>> boost::spirit::lit(')')
			//	)
			//	)
			//>> *nil
			;


		//body_subtype = subtype[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
		//	>> '('
		//	>> name
		//	>> (nil | name)
		//	>> *name
		//	>> ')'
		//	>> -nil
		//	>> -(nil |
		//		('('
		//			>> name
		//			>> ')')
		//		)
		//	;

		//node = (body | section)[boost::spirit::_val = boost::spirit::qi::_1];
		node = body[boost::spirit::_val = boost::spirit::qi::_1] | section[boost::spirit::_val = boost::spirit::qi::_1];


		body = '('
			>> *node[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)]
			>> ')'
			//>> -body_subtype[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			;

		//structure = "BODYSTRUCTURE"
		//	>> body[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
		//	;
		//structure = *("UID" > boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
		//	| "BODYSTRUCTURE" >
		//	(-body[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
		//		>> -('('
		//			>> body_subtype
		//			>> ')'))

		//	)
		//	;

		structure = *("UID" > boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			| "BODYSTRUCTURE" > body[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			)
			;



		document = '*'
			>> boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> "FETCH"
			>> '('
			>> structure[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> ')'
			/*>> answer*/;

		TDocument output;
		//TBody output;

		bool r = false;

		try
		{
			r = boost::spirit::qi::phrase_parse(
				first,
				last,
				(
					document[_callback]
					),
				boost::spirit::ascii::space,
				output
				);
		}
		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
		{
			std::string error(e.first, e.last);
			e_what = error;

		}
		catch (const std::exception& e)
		{
			e_what = e.what();
		}
		catch (...)
		{
			e_what = "Unknown exception";
		}

		if (first != last) // fail if we did not get a full match
			return false;

		return r;
#else

		return false;
#endif
	}
}

bool fetchbodystructureparser_parse(std::string source, std::function<void(const fetchbodystructureparser::TDocument&doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string & errString)
{
	bool r = fetchbodystructureparser::parse(
		source.begin(),
		source.end(),
		_callback,
		errString
		);

		return r;
}


bool fetchbodystructureparser_parse2(std::string source, std::function<void(const fetchbodystructureparser::TDocument&doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string & errString)
{
	bool r = fetchbodystructureparser::parse2(
		source.begin(),
		source.end(),
		_callback,
		errString
		);

	return r;
}

/******************************* eof *************************************/