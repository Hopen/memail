/************************************************************************/
/* Name     : MCC\ImapFetchBodyContentParser.h                          */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 13 Mar 2016                                               */
/************************************************************************/
#pragma once

namespace fetchparsercontent
{
	struct TFetchData
	{
		int num;
		int uid;
		std::string contentIndex;
		int size;
		//std::string source;
	};
}

bool fetchparsercontent_parse(std::string source, std::function <void(const fetchparsercontent::TFetchData& doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);

/******************************* eof *************************************/
