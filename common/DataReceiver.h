/************************************************************************/
/* Name     : MCC\DataReceiver.h                                        */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCC                                                       */
/* Company  : Expert Solutions                                          */
/* Date     : 08 Feb 2016                                               */
/************************************************************************/
#pragma once
#include "Router\router_compatibility.h"

class CSystemLog;

using MessageCollector = std::list<CMessage>;

template <class Impl>
class CDataReceiver
{
public:
	CDataReceiver()
	{
		SubscribeOnEvents();

		CRouterManager::ConnectHandler chandler = boost::bind(&Impl::on_routerConnect, static_cast<Impl*>(this));
		m_router.RegisterConnectHandler(chandler);
		CRouterManager::DeliverErrorHandler ehandler = boost::bind(&Impl::on_routerDeliverError, static_cast<Impl*>(this), _1, _2, _3);
		m_router.RegisterDeliverErrorHandler(ehandler);

		CRouterManager::StatusChangeHandler status_handler = boost::bind(&Impl::on_appStatusChange, static_cast<Impl*>(this), _1, _2);
		m_router.RegisterStatusHandler(status_handler);
	}

	int Init(CSystemLog* pLog)
	{
		return static_cast<Impl*>(this)->init_impl(pLog);
	}

	void Stop()
	{
		return static_cast<Impl*>(this)->stop();
	}

	void SubscribeOnEvents()
	{
		static_cast<Impl*>(this)->set_Subscribes();
	}

	uint32_t GetClientId()const
	{
		//return static_cast<const Impl*>(this)->get_ClientId();
		return m_router.BoundAddress();
	}

	template <class TMessage>
	void SendToAny(TMessage&& message)const
	{
		//static_cast<const Impl*>(this)->sendToAny(std::forward<TMessage>(message));
		m_router.SendToAny(std::forward<TMessage>(message));
	}

	template <class TMessage>
	void SendToAll(TMessage&& message)const
	{
		//static_cast<const Impl*>(this)->sendToAll(std::forward<TMessage>(message));
		m_router.SendToAll(std::forward<TMessage>(message));
	}

	template <class TMessage, class TAddress>
	void SendTo(TMessage&& message, TAddress&& address)const
	{
		//static_cast<const Impl*>(this)->sendTo(std::forward<TMessage>(message), std::forward<TAddress>(address));
		m_router.SendTo(std::forward<TMessage>(message), std::forward<TAddress>(address));
	}

protected:
	template<typename Func>
	void subscribe(const std::wstring& _messageName, Func&& func)
	{
		core::SubscribeMessage(m_router, _messageName, static_cast<Impl*>(this), std::forward<Func>(func));
	}

	void onReplyEventHandler2(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
	{

	}

private:
	mutable CRouterManager m_router;
};

//class CDataReceiverImpl;


/******************************* eof *************************************/

