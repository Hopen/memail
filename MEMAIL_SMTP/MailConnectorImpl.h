/************************************************************************/
/* Name     : MEMAIL\SMTP\MailConnectorImpl.h                           */
/* Author   : Andrey Alekseev                                           */
/* Project  : MEMAIL\SMTP                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 16 Aug 2016                                               */
/************************************************************************/

#pragma once

#include <string>
#include <future>

#include "Router\router_compatibility.h"
#include "DataReceiverImpl.h"

#include "MailConnector.h"
#include "DataReceiverImpl.h"

class smtp_client;
class CMailConnectorImpl : public CMailConnector < CMailConnectorImpl >
{
	friend class CMailConnector < CMailConnectorImpl >;
public:
	CMailConnectorImpl(boost::asio::io_service& io);
	~CMailConnectorImpl();

private:
	bool _getMail(CDataReceiverImpl *_routerInterface,
		TImapParams params)const;

	bool _sendMail(CDataReceiverImpl *_routerInterface, MessageCollector msg_list, /*const std::wstring& _email, const std::wstring& certificate_uri, const std::wstring& _uri, const std::wstring& _port,
		const std::wstring& _login, const std::wstring& _pass, bool bSSLEnable*/
		TSmtpParams params)const;

private:
	boost::asio::io_service& r_io;

	using SmtpClientPtr = std::shared_ptr<smtp_client>;

	CDataReceiverImpl *m_pReceiver;
};


/******************************* eof *************************************/
