/************************************************************************/
/* Name     : MEMAIL\SmtpClient.h                                       */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 16 Aug 2016                                               */
/************************************************************************/

#pragma once

#include "DataReceiverImpl.h"

void makeSSL_SmtpClient(boost::asio::io_service& io_service, TSmtpParams params, CSystemLog* pLog, MessageCollector msg_list);
void makeNoSSL_SmtpClient(boost::asio::io_service& io_service, TSmtpParams params, CSystemLog* pLog, MessageCollector msg_list);


/******************************* eof *************************************/
