/************************************************************************/
/* Name     : MEMAIL\DataReceiverImplHandler.cpp                        */
/* Author   : Andrey Alekseev                                           */
/* Project  : MEMAIL                                                    */
/* Company  : Expert Solutions                                          */
/* Date     : 15 Aug 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/program_options/detail/convert.hpp>

#include "DataReceiverImpl.h"
#include "api/ProcessHelper.h"

#include "codec.h"

static __int64 CreateMCCID(int num_message, uint32_t _client_id)
{
	__int64 mccid = _client_id;
	mccid = (mccid << 32) | num_message;
	return mccid;
}


///************************************************************************/
///***************************   OUTCOMING   ******************************/
///************************************************************************/

void CDataReceiverImpl::onReplyEventHandler(CMessage _msg, const CLIENT_ADDRESS& _from, const CLIENT_ADDRESS& _to)
{
	SystemConfig settings;
	std::vector<std::wstring> listOfMails;

	try
	{
		std::wstring sDoNoReplyAddress = settings->get_config_value<std::wstring>(_T("ReplayAddresses"));
		std::vector <std::wstring> toList;
		boost::split(toList, sDoNoReplyAddress, std::bind2nd(std::equal_to<wchar_t>(), ';'));
		for each (auto address in toList)
		{
			if (!address.empty())
			{
				listOfMails.push_back(address);
			}
		}
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"DoNotReplayAddress\" config key");
	}


	std::wstring sTo = _msg.SafeReadParam(L"Operator", core::checked_type::String, L"").AsWideStr();
	if (!listOfMails.empty() && !sTo.empty())
	{
		if (std::find(listOfMails.begin(), listOfMails.end(), sTo) == listOfMails.end())
			return;
	}

	if (1)
	{
		std::wstring sOperatorName = _msg.SafeReadParam(L"OperatorName", core::checked_type::String, L"").AsWideStr();
		if (!sOperatorName.empty())
		{
			_msg[L"OperatorName"] = L"=?utf-8?B?" + stow(encodeBase64(boost::to_utf8(sOperatorName))) + L"?=";
		}
	}


	m_log->LogStringModule(LEVEL_INFO, L"S2MCC_Reply", L"Incoming message: %s", _msg.Dump().c_str());

	if (1)
	{
		std::wstring sSubject = _msg.SafeReadParam(L"MessageTitle", core::checked_type::String, L"").AsWideStr();
		if (!sSubject.empty())
		{
			_msg[L"MessageTitle"] = L"=?utf-8?B?" +  stow(encodeBase64(boost::to_utf8(sSubject))) + L"?=";
		}
	}

	__int64 iMCCID = 0;
	try
	{
		iMCCID = _msg.SafeReadParam(L"MCCID", core::checked_type::Int64, 0).AsInt64();
		if (iMCCID == 0)
		{
			m_log->LogStringModule(LEVEL_WARNING, L"S2MCC_Reply", L"Incoming message has no MCCID and has skipped");
			return;
		}
	}
	catch (core::message_parameter_invalid_type_exception& error)
	{
		m_log->LogStringModule(LEVEL_WARNING, L"S2MCC_Reply", L"Exception when try to read \"MCCID\":", stow(error.what()));
		return;
	}

	std::wstring sMessageFolderMCCID = toStr<wchar_t>(iMCCID);


	std::wstring sAttachmentFolder;
	try
	{
		sAttachmentFolder = settings->get_config_value<std::wstring>(_T("AttachmentFolder"));
	}
	catch (...)
	{
		m_log->LogString(LEVEL_WARNING, L"Exception reading \"AttachmentFolder\" config key");
		sAttachmentFolder = L"c:\\Attachments\\outbox\\";
	}


	struct TAttachmentsParam
	{
		std::wstring Url;
		std::wstring SourceName;
		bool dDecode{ false };

		TAttachmentsParam(const std::wstring& _url, const std::wstring& _sourceName, bool _decode = false)
			:Url(_url), SourceName(_sourceName), dDecode(_decode)
		{}
	};

	std::vector <TAttachmentsParam> attachments;

	auto attachCount = _msg.SafeReadParam(L"AttachCount", core::checked_type::Int, 0).AsInt();
	if (attachCount == 0)
	{
		m_log->LogString(LEVEL_FINEST, L"No attachments");
	}
	else
	{

		for (int i = 0; i < attachCount; ++i)
		{
			auto attach = _msg.SafeReadParam(L"Attach" + toStr<wchar_t>(i) + L"Url", core::checked_type::String, L"");
			if (!attach.IsEmpty() && attach.AsWideStr().length())
			{
				attachments.emplace_back(attach.AsWideStr(), std::wstring(L"Attach") + toStr<wchar_t>(i) + L"Source");
			}

			std::wstring attachName = L"Attach" + toStr<wchar_t>(i) + L"Name";

			auto attachNameSource = _msg.SafeReadParam(attachName, core::checked_type::String, L"").AsWideStr();
			_msg[attachName] = L"=?UTF-8?B?" + stow(encodeBase64(boost::to_utf8(attachNameSource))) + L"?=";

			//auto attachNameSourceSafe = _msg.SafeReadParam(attachName, core::checked_type::String, L"").AsWideStr();
			//int test = 0;
		}
	}

	if (1)
	{
		auto mainMassage = _msg.SafeReadParam(L"Message", core::checked_type::String, L"").AsWideStr();

		if (!mainMassage.empty())
		{
			std::string sSource = boost::to_utf8(mainMassage);
			_msg[L"Message"] = /*encodeBase64*/stow(sSource);
		}
		else
		{
			auto mainMassage = _msg.SafeReadParam(L"MessageUrl", core::checked_type::String, L"").AsWideStr();

			m_log->LogString(LEVEL_FINEST, L"MessageUrl: %s", mainMassage.c_str());

			if (!mainMassage.empty())
			{
				attachments.emplace_back(mainMassage, L"Message", true);
			}
		}

	}


	for each (auto attachmentParam in attachments)
	{
		std::wstring attachPath = attachmentParam.Url;
		std::wstring attachSourceName = attachmentParam.SourceName;

		m_log->LogString(LEVEL_FINEST, L"attachPath: %s", attachPath.c_str());
		m_log->LogString(LEVEL_FINEST, L"attachSourceName: %s", attachSourceName.c_str());
		m_log->LogString(LEVEL_FINEST, L"sMessageFolderMCCID: %s", sMessageFolderMCCID.c_str());

		std::wstring localPath;
		int pos = attachPath.find(sMessageFolderMCCID);
		if (pos > 0)
		{
			localPath = attachPath.substr(pos, attachPath.length());
		}

		m_log->LogString(LEVEL_FINEST, L"localPath: %s", localPath.c_str());

		if (localPath.empty())
		{
			break;
		}

		if (sAttachmentFolder[sAttachmentFolder.length() - 1] == ('\\'))
		{
			localPath = sAttachmentFolder + localPath;
		}
		else
		{
			localPath = sAttachmentFolder + L"\\" + localPath;
		}

		m_log->LogString(LEVEL_FINEST, L"localPath: %s", localPath.c_str());

		std::string sSource;

		std::ifstream _in(localPath/*attachPath*/.c_str(), std::ios::binary);
		// read whole file
		DWORD   dwDataSize = 0;
		if (_in.seekg(0, std::ios::end))
		{
			dwDataSize = static_cast<DWORD>(_in.tellg());
		}
		std::string buff(dwDataSize, 0);
		if (dwDataSize && _in.seekg(0, std::ios::beg))
		{
			std::copy(std::istreambuf_iterator< char>(_in),
				std::istreambuf_iterator< char >(),
				buff.begin());

			sSource = buff;
		}
		else
		{
			m_log->LogString(LEVEL_WARNING, L"Attachment file: \"%s\" is empty", /*attachPath*/localPath.c_str());
		}
		_in.close();

		m_log->LogString(LEVEL_FINEST, L"Source: %s", stow(sSource).c_str());

		if (!sSource.empty())
		{
			if (attachmentParam.dDecode)
			{
				sSource = boost::to_utf8(stow(sSource));
				_msg[attachSourceName] = stow(sSource);
			}
			else
			{
				_msg[attachSourceName] = stow(encodeBase64(sSource));
			}

		}
		else
		{
			m_log->LogString(LEVEL_FINEST, L"Source empty");
			_msg[attachSourceName] = L"";
		}

	}

	{
		std::lock_guard<std::mutex> lock(m_mutex);
		m_incomingMsgList.push_back(_msg);
	}

}

/************************************************************************/
/************************************************************************/
/************************************************************************/



void CDataReceiverImpl::send_onAccepted(const CMessage& msg)const
{
	CMessage postmsg(L"MCC2RT_EMAILSTATE");

	postmsg[L"DateTime"] = core::support::now();
	postmsg[L"MCCID"] = msg.SafeReadParam(L"MCCID", core::checked_type::Int64, -1);
	postmsg[L"From"] = msg.SafeReadParam(L"Operator", core::checked_type::String, L"");
	postmsg[L"To"] = msg.SafeReadParam(L"Customer", core::checked_type::String, L"");
	postmsg[L"Dir"] = L"O";

	postmsg[L"Host"] = stow(boost::asio::ip::host_name());
	postmsg[L"State"] = L"SENDING";

	SendToAll(postmsg);

	postmsg.SetName(L"MCC2S_Reply_Ack");

	this->SendTo(postmsg, core::client_address(msg.SafeReadParam(L"ScriptID", core::checked_type::Int64, -1).AsInt64()));
}

void CDataReceiverImpl::send_onCompleted(const CMessage& msg)const
{
	CMessage postmsg(L"MCC2RT_EMAILSTATE");

	postmsg[L"DateTime"] = core::support::now();
	postmsg[L"MCCID"] = msg.SafeReadParam(L"MCCID", core::checked_type::Int64, -1);
	postmsg[L"From"] = msg.SafeReadParam(L"Operator", core::checked_type::String, L"");
	postmsg[L"To"] = msg.SafeReadParam(L"Customer", core::checked_type::String, L"");
	postmsg[L"Dir"] = L"O";

	postmsg[L"Host"] = stow(boost::asio::ip::host_name());
	postmsg[L"State"] = L"SENT";

	this->SendToAll(postmsg);

	postmsg.SetName(L"MCC2S_Reply_Ack");

	this->SendTo(postmsg, core::client_address(msg.SafeReadParam(L"ScriptID", core::checked_type::Int64, -1).AsInt64()));
}

void CDataReceiverImpl::send_onFailed(const CMessage& msg)const
{
	CMessage postmsg(L"MCC2RT_EMAILSTATE");

	postmsg[L"DateTime"] = core::support::now();
	postmsg[L"MCCID"] = msg.SafeReadParam(L"MCCID", core::checked_type::Int64, -1);
	postmsg[L"From"] = msg.SafeReadParam(L"Operator", core::checked_type::String, L"");
	postmsg[L"To"] = msg.SafeReadParam(L"Customer", core::checked_type::String, L"");
	postmsg[L"Error"] = msg.SafeReadParam(L"Error", core::checked_type::Int, 0);
	postmsg[L"ErrorDescription"] = msg.SafeReadParam(L"ErrorDescription", core::checked_type::String, L"");
	postmsg[L"Dir"] = L"O";

	postmsg[L"Host"] = stow(boost::asio::ip::host_name());
	postmsg[L"State"] = L"FAILED";

	SendToAll(postmsg);

	postmsg.SetName(L"MCC2S_Reply_Ack");

	this->SendTo(postmsg, core::client_address(msg.SafeReadParam(L"ScriptID", core::checked_type::Int64, -1).AsInt64()));

}

/******************************* eof *************************************/