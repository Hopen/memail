/************************************************************************/
/* Name     : MEMAIL\ImapClient.h                                       */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 15 Aug 2016                                               */
/************************************************************************/

#pragma once

#include "DataReceiverImpl.h"

void makeSSL_ImapClient(TImapParams params, CSystemLog* pLog);


/******************************* eof *************************************/
