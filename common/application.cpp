/************************************************************************/
/* Name     : MCC\application.cpp                                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : Common MCC                                                */
/* Company  : Expert Solutions                                          */
/* Date     : 26 Jul 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include "application.h"

#define BOOST_APPLICATION_FEATURE_NS_SELECT_BOOST

#include <iostream>
#include <fstream>
#include <future>
#include <boost/program_options.hpp>
#include <boost/application.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>

// provide setup example for windows service
#if defined(BOOST_WINDOWS_API)
#   include "setup/windows/setup/service_setup.hpp"
#endif

#include "Log/SystemLog.h"
#include "Patterns/string_functions.h"

#include "DataReceiverImpl.h"
#include "ThreadHelper.h"

#ifdef  WIN32
#include "WinAPI/mdump.h"
#endif

namespace po = boost::program_options;


class myapp
{

public:

	myapp(boost::application::context& context)
		: context_(context)
	{
	}

	void worker()
	{
		boost::asio::io_service io;

		// insert singleton_auto_pointer HERE
		singleton_auto_pointer<CSystemLog> log;

#ifdef  WIN32
		singleton_auto_pointer<MiniDumper> g_MiniDump;
#endif //  WIN32


		m_receiver = std::make_unique<CDataReceiverImpl>(io);

		//boost::this_thread::sleep(boost::posix_time::millisec(100));

		if (!m_receiver->Init(log->GetInstance()))
		{
			//singleton_auto_pointer<CSystemLog> log;
			log->LogString(LEVEL_INFO, L"Starting run");
			//unit test
			//S2MCC_Reply_test(m_receiver.get());

			CThreadWrapper t(std::thread(boost::bind(&boost::asio::io_service::run, &io)));

			boost::application::csbl::shared_ptr<boost::application::status> st =
				context_.find<boost::application::status>();

			int count = 0;
			while (st->state() != boost::application::status::stopped)
			{
				boost::this_thread::sleep(boost::posix_time::seconds(1));

				//if (st->state() == application::status::paused)
				//	my_log_file_ << count++ << ", paused..." << std::endl;
				//else
				//	my_log_file_ << count++ << ", running..." << std::endl;
			}

			m_receiver->Stop();
		}


	}

	// param
	int operator()()
	{
		worker();

		return 0;
	}

	// windows/posix

	bool stop()
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Stoping the application...");

		if (m_receiver)
		{
			m_receiver->Stop();
		}

		return true; // return true to stop, false to ignore
	}

	// windows specific (ignored on posix)

	bool pause()
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Pause the application...");
		return true; // return true to pause, false to ignore
	}

	bool resume()
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Resume the application...");

		return true; // return true to resume, false to ignore
	}

private:

	//std::ofstream my_log_file_;

	boost::application::context& context_;

	std::unique_ptr<CDataReceiverImpl> m_receiver;

};

//#include "MailConnector.h"


#include "boost/archive/iterators/base64_from_binary.hpp"
#include "boost/archive/iterators/binary_from_base64.hpp"
#include "boost/archive/iterators/transform_width.hpp"
#include <boost/archive/iterators/insert_linebreaks.hpp>
#include <boost/archive/iterators/ostream_iterator.hpp>
#include <sstream>
#include <string>

#include "boost/asio/ssl.hpp"

//using namespace std;
//using namespace boost::asio;
//using namespace boost::asio::ip;

int CreateMain(int argc, char * c_argv[])
{
	//boost::asio::io_service io_service;

	//io_service.post([&]()
	//{
	//	//ssl::context context(ssl::context::sslv23);
	//	//context.set_default_verify_paths();

	//	//ssl::stream<tcp::socket> socket(_ioService, context);
	//	//tcp::resolver resolver(_ioService);
	//	//tcp::resolver::query query("api.telegram.org", "443");

	//	//connect(socket.lowest_layer(), resolver.resolve(query));

	//	//socket.set_verify_mode(ssl::verify_none);
	//	////socket.set_verify_callback(ssl::rfc2818_verification(url.host));

	//	//socket.set_verify_callback([&](bool preverified,
	//	//	boost::asio::ssl::verify_context& ctx)
	//	//{
	//	//	// The verify callback can be used to check whether the certificate that is
	//	//	// being presented is valid for the peer. For example, RFC 2818 describes
	//	//	// the steps involved in doing this for HTTPS. Consult the OpenSSL
	//	//	// documentation for more details. Note that the callback is called once
	//	//	// for each certificate in the certificate chain, starting from the root
	//	//	// certificate authority.

	//	//	// In this example we will simply print the certificate's subject name.
	//	//	char subject_name[256];
	//	//	X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
	//	//	X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);

	//	//	//std::cout << subject_name << std::endl;

	//	//	//LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");

	//	//	return preverified;
	//	//});

	//	//socket.handshake(ssl::stream<tcp::socket>::client);

	//	////string requestText = HttpParser::getInstance().generateRequest(url, args, false);
	//	//std::string requestText = "GET /bot245204529%3aAAF0qTSFtW9Euj9KJ8jBdaM8Pgy6GGi2OgQ/getMe HTTP/1.1\r\nHost: api.telegram.org\r\nConnection: close\r\n\r\n";
	//	//write(socket, buffer(requestText.c_str(), requestText.length()));

	//	//string response;
	//	//char buff[1024];
	//	//boost::system::error_code error;
	//	//while (!error) {
	//	//	size_t bytes = read(socket, buffer(buff), error);
	//	//	response += string(buff, bytes);
	//	//}

	//	singleton_auto_pointer<CSystemLog> log;
	//	log->LogString(LEVEL_INFO, L"Test socket");
	//	
	//	boost::asio::ip::tcp::resolver resolver(io_service);
	//	log->LogString(LEVEL_INFO, L"12");
	//	boost::asio::ip::tcp::resolver::query query("api.telegram.org", "443");
	//	log->LogString(LEVEL_INFO, L"13");
	//	boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
	//	log->LogString(LEVEL_INFO, L"14");
	//	boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();
	//	log->LogString(LEVEL_INFO, L"15");

	//	boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
	//	//ctx.load_verify_file(params._sertificate);
	//	ctx.set_default_verify_paths();
	//	log->LogString(LEVEL_INFO, L"16");


	//	boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket(io_service, ctx);

	//	//connect(_socket.lowest_layer(), iterator);
	//	log->LogString(LEVEL_INFO, L"17");

	//	boost::system::error_code errorCode;

	//	//boost::asio::async_connect(_socket.lowest_layer(), iterator, yield[errorCode]);
	//	boost::asio::connect(_socket.lowest_layer(), iterator);

	//	if (errorCode)
	//	{
	//		log->LogString(LEVEL_INFO, L"errorCode: %i", errorCode.value());
	//		throw errorCode;
	//	}

	//	_socket.set_verify_mode(boost::asio::ssl::verify_none);
	//	_socket.set_verify_callback([&](bool preverified,
	//		boost::asio::ssl::verify_context& ctx)
	//	{
	//		// The verify callback can be used to check whether the certificate that is
	//		// being presented is valid for the peer. For example, RFC 2818 describes
	//		// the steps involved in doing this for HTTPS. Consult the OpenSSL
	//		// documentation for more details. Note that the callback is called once
	//		// for each certificate in the certificate chain, starting from the root
	//		// certificate authority.

	//		// In this example we will simply print the certificate's subject name.
	//		char subject_name[256];
	//		X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
	//		X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);

	//		std::cout << subject_name << std::endl;

	//		log->LogString(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");

	//		return preverified;
	//	});
	//	log->LogString(LEVEL_INFO, L"18");


	//	//_socket.async_handshake(boost::asio::ssl::stream_base::client, yield[errorCode]);
	//	_socket.handshake(boost::asio::ssl::stream_base::client);

	//	log->LogString(LEVEL_INFO, L"19");

	//	if (errorCode)
	//	{
	//		log->LogString(LEVEL_INFO, L"errorCode: %i", errorCode.value());
	//		throw errorCode;
	//	}
	//	
	//	//LogStringModule(LEVEL_FINEST, L"Write: %", stow(params._request).c_str());

	//	//_socket.async_write_some(boost::asio::buffer(params._request), yield[errorCode]);

	//	std::string requestText = "GET /bot245204529%3aAAF0qTSFtW9Euj9KJ8jBdaM8Pgy6GGi2OgQ/getMe HTTP/1.1\r\nHost: api.telegram.org\r\nConnection: close\r\n\r\n";
	//	boost::asio::write(_socket, boost::asio::buffer(requestText.c_str(), requestText.length()));

	//	if (errorCode)
	//	{
	//		throw errorCode;
	//	}

	//	std::string in_data;
	//	//do
	//	//{
	//	char reply_[1024] = {};
	//	//_socket.async_read_some(boost::asio::buffer(reply_), yield);
	//	size_t bytes = boost::asio::read(_socket, boost::asio::buffer(reply_), errorCode);
	//	//if (errorCode)
	//	//{
	//	//	throw errorCode;
	//	//}

	//	in_data += reply_;

	//	//} while (!read_complete_predicate(in_data));


	//	log->LogString(LEVEL_FINEST, L"Read: %s", stow(in_data).c_str());


	//});

	//io_service.run();







	//////////////////////////////////////////////////

	bool bRunAsConsole = false;

#if defined(BOOST_WINDOWS_API)
#if !defined(__MINGW32__)
	// get our executable path name
	boost::filesystem::path executable_path_name = boost::application::path().location();
	boost::system::error_code ec;

	po::variables_map vm;
	po::options_description install("service options");
	install.add_options()
		("help", "produce a help message")
		("console", "run as console")
		(",i", "install service")
		(",u", "unistall service")
		("name", po::value<std::string>()->default_value(executable_path_name.stem().string()), "service name")
		("display", po::value<std::string>()->default_value(""), "service display name (optional, installation only)")
		("description", po::value<std::string>()->default_value(""), "service description (optional, installation only)")
		;


	po::store(po::parse_command_line(argc, c_argv, install), vm);

	if (vm.count("help"))
	{
		std::cout << "[I] Setup changed the current configuration." << std::endl;
		std::cout << install << std::endl;
		return 0;
	}

	if (vm.count("console"))
	{
		bRunAsConsole = true;
	}

	if (vm.count("-i"))
	{
		std::cout << "[I] Setup changed the current configuration." << std::endl;

		try
		{
			boost::application::example::install_windows_service(
				boost::application::setup_arg(stow(vm["name"].as<std::string>())),
				boost::application::setup_arg(stow(vm["display"].as<std::string>())),
				boost::application::setup_arg(stow(vm["description"].as<std::string>())),
				boost::application::setup_arg(executable_path_name),
				boost::application::setup_arg(L""),
				boost::application::setup_arg(L""),
				boost::application::setup_arg(L"auto"),
				boost::application::setup_arg(L""),
				boost::application::setup_arg(L"")).install(ec);

			std::cout << ec.message() << std::endl;
		}
		catch (boost::system::system_error& ec)
		{
			std::cout << ec.what() << std::endl;
		}
		return 0;
	}

	if (vm.count("-u"))
	{
		std::cout << "[I] Setup changed the current configuration." << std::endl;

		boost::application::example::uninstall_windows_service(
			boost::application::setup_arg(vm["name"].as<std::string>()),
			boost::application::setup_arg(executable_path_name)).uninstall(ec);

		std::cout << ec.message() << std::endl;
		return 0;
	}
#endif
#endif


	boost::application::context app_context;
	myapp app(app_context);

	// my server aspects

	//insert path aspect
	boost::application::path path;
	path.location();
	app_context.insert<boost::application::path>(
		boost::application::csbl::make_shared<boost::application::path>(path));


	// add termination handler
	boost::application::handler<>::callback termination_callback
		= boost::bind(&myapp::stop, &app);

	app_context.insert<boost::application::termination_handler>(
		boost::application::csbl::make_shared<boost::application::termination_handler_default_behaviour>(termination_callback));

#if defined(BOOST_WINDOWS_API)

	// windows only : add pause handler
	boost::application::handler<>::callback pause_callback
		= boost::bind(&myapp::pause, &app);

	app_context.insert<boost::application::pause_handler>(
		boost::application::csbl::make_shared<boost::application::pause_handler_default_behaviour>(pause_callback));

	// windows only : add resume handler

	boost::application::handler<>::callback resume_callback
		= boost::bind(&myapp::resume, &app);

	app_context.insert<boost::application::resume_handler>(
		boost::application::csbl::make_shared<boost::application::resume_handler_default_behaviour>(resume_callback));

#endif

	int result = 0;
	if (bRunAsConsole)
		result = boost::application::launch< boost::application::common>(app, app_context, ec);
	else
		result = boost::application::launch< boost::application::server>(app, app_context, ec);


	if (ec)
	{
		singleton_auto_pointer<CSystemLog> log;
		log->LogString(LEVEL_INFO, L"Process terminated with error: <%i> - %s", ec.value(), ec.message().c_str());
	}

	return result;
}


/******************************* eof *************************************/