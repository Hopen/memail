/************************************************************************/
/* Name     : MEMAIL\SMTP\DataReceiverImpl.cpp                          */
/* Author   : Andrey Alekseev                                           */
/* Project  : MEMAIL\SMTP                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 16 Aug 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include <future>
#include <type_traits>

#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include "DataReceiverImpl.h"
#include "api/ProcessHelper.h"
#include "MailConnectorImpl.h"

const int EXPIREDTIME = 30;

CDataReceiverImpl::CDataReceiverImpl(boost::asio::io_service& io) :
	m_log(NULL),
	r_io(io)
{
	m_idapConnector = std::make_unique<CMailConnectorImpl>(r_io);
}


CDataReceiverImpl::~CDataReceiverImpl()
{
	int test = 0;
}

int CDataReceiverImpl::init_impl(CSystemLog* pLog)
{
	if (!pLog)
		return -1111;

	m_log = pLog;

	m_log->LogString(LEVEL_FINEST, L"CDataReceiverImpl initializing...");

	boost::asio::spawn(r_io, [&](boost::asio::yield_context yield)
	{
		boost::asio::steady_timer timer(r_io);

		timer.expires_from_now(std::chrono::milliseconds(100));
		timer.async_wait(yield); //wait for 10 seconds

		start();
	});

	return 0;
}

void S2MCC_Reply_test(CDataReceiverImpl* pReciever)
{
	CMessage msg(L"S2MCC_Reply");
	msg[L"MCCID"] = 84984517885968;
	msg[L"Customer"] = L"hopen@inbox.ru";
	msg[L"Operator"] = L"seemslikebox@mail.ru";
	msg[L"OperatorName"] = L"������ ��������";
	msg[L"CC"] = L"aalekseev@expertsolutions.ru";
	//msg[L"CC"] = "";
	msg[L"BCC"] = L"";
	msg[L"Message"] = L"������ ���������� ��� ���: ��������� ����� �� ��������� ����������.";
	//msg[L"BCC"] = L"aalekseev@forte-it.ru";
	//msg[L"Operator"] = L"testimap@beeline.ru";
	msg[L"MessageTitle"] = L"���������";
	//msg[L"MessageUrl"] = L"http://tv-ocm002/Attachments/1234/outbox/text.plain";
	//msg[L"MessageUrl"] = L"D:\\Attachments\\84984517885968\\inbox\\text.plain";
	//msg[L"AttachCount"] = 0;
	//msg[L"Attach0Name"] = L"���������_�������_�_�������������_�_������,_���������������_���������.png";
	//msg[L"Attach0Url"] = L"D:\\Attachments\\84984517885968\\inbox\\���������_�������_�_�������������_�_������,_���������������_���������.png";
	//msg[L"Attach0Name"] = L"text.html";
	//msg[L"Attach0Url"] = L"D:\\Attachments\\84984517885968\\inbox\\text.html";
	//msg[L"Attach0Name"] = L"1894837554.jpg";
	//msg[L"Attach0Url"] = L"D:\\Attachments\\84984517885968\\inbox\\1894837554.jpg";
	//msg[L"Attach0Name"] = L"������� ��������� ��������.txt";
	//msg[L"Attach0Url"] = L"D:\\Attachments\\84984517885968\\inbox\\������� ��������� ��������.txt";

	CLIENT_ADDRESS _from;
	_from.SetAsQWORD((__int64)0x00063C40 << 32);
	CLIENT_ADDRESS _to;
	bool blDeliverError = false;

	pReciever->TestReplyEventHandler(msg, _from, _to);

	msg[L"Customer"] = L"hopen@inbox.ru";
	msg[L"MessageTitle"] = L"Title";
	msg[L"Message"] = L"���������";
	msg[L"Charset"] = L"ascii";

	pReciever->TestReplyEventHandler(msg, _from, _to);

}

int CDataReceiverImpl::start()
{
	//S2MCC_Reply_test(this);

	SystemConfig settings;

	int iLongPoolTimer = settings->safe_get_config_int(_T("ExpiredTime"), EXPIREDTIME);
	
	
	//std::wstring sSmtpServer, sSmtpPort, sLogin, sPassword, sCertificate, sSmtpClientAddress;
	TSmtpParams params;

	params._sSmtpServer = settings->safe_get_config_value<std::wstring>(_T("SMTPServerName"), L"127.0.0.1");
	params._sSmtpPort = settings->safe_get_config_value<std::wstring>(_T("SMTPPort"), L"25");
	params._sLogin = settings->safe_get_config_value<std::wstring>(_T("SMTPLogin"), L"");
	params._sPassword = settings->safe_get_config_value<std::wstring>(_T("SMTPPassword"), L"");

	params._sCertificate = settings->safe_get_config_value<std::wstring>(_T("SMTPSSLSertificate"), L"");
	std::wstring pem_location = extra_api::CProcessHelper::GetCurrentFolderName();//fpath.parent_path().c_str();
	params._sCertificate = pem_location + params._sCertificate;

	params._sslEnable = settings->safe_get_config_bool(L"SSLEnable", false);

	params._sSmtpClientAddress = settings->safe_get_config_value<std::wstring>(_T("SmtpClientName"), L"");

	params._accepted = [this](const CMessage& msg)
	{
		send_onAccepted(msg);
	};

	params._completed = [this](const CMessage& msg)
	{
		send_onCompleted(msg);
	};

	params._failed = [this](const CMessage& msg)
	{
		send_onFailed(msg);
	};


	try
	{
		boost::asio::spawn(r_io, [this,
			iLongPoolTimer,
			p_io = &r_io,
			m_log = m_log,
			params = std::move(params),
			connector = m_idapConnector.get(),
			pMail = m_idapConnector.get()](boost::asio::yield_context yield)
		{

			boost::asio::steady_timer timer(*p_io);
			boost::system::error_code errorCode;

			int initTimerValue = 3;

			while (!(*p_io).stopped())
			{
				timer.expires_from_now(std::chrono::seconds(initTimerValue));
				timer.async_wait(yield[errorCode]); //wait for timer

				m_log->LogStringModule(LEVEL_FINEST, L"steady timer", L"========================Tick===================");

				if (errorCode)
				{
					throw errorCode;
				}

				initTimerValue = iLongPoolTimer;


				MessageCollector _incomingMsgList;
				{
					std::lock_guard <std::mutex> lock(m_mutex);
					if (m_incomingMsgList.empty())
						continue;

					_incomingMsgList.swap(m_incomingMsgList);
				}

				m_idapConnector->SendMail(this, std::move(_incomingMsgList), std::move(params));


			}
		});

	}
	catch (std::exception& e) {
		m_log->LogString(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
	}


	//TImapParams params;

	//std::list <TImapParams> params_list;

	//params._uri = settings->safe_get_config_value<std::wstring>(_T("IMAPServerName"), L"127.0.0.1");
	//params._port = settings->safe_get_config_value<std::wstring>(_T("IMAPPort"), L"993");
	//params._sslEnable = settings->safe_get_config_bool(L"SSLEnable", false);

	//std::wstring pem_location = extra_api::CProcessHelper::GetCurrentFolderName();

	//auto imap_node = settings->get_config_node(std::wstring(L"IMAP"));

	//try
	//{
	//	for (const auto& child : imap_node)
	//	{
	//		if (child.first == L"<xmlcomment>")
	//			continue;

	//		std::wstring sertificate = params._sslEnable ? settings->get_config_attr<std::wstring>(child.second, L"sslcertificate") : L"";
	//		if (!sertificate.empty())
	//			sertificate = pem_location + sertificate;

	//		params._certificate_uri = sertificate;
	//		params._email = settings->get_config_attr<std::wstring>(child.second, L"name");
	//		params._login = settings->get_config_attr<std::wstring>(child.second, L"login");
	//		params._pass = settings->get_config_attr<std::wstring>(child.second, L"password");
	//		params._peek = settings->get_config_bool(child.second, L"peek");
	//		params._afterReadMoveTo = settings->get_config_attr<std::wstring>(child.second, L"afterReadMoveTo");

	//		params_list.push_back(params);
	//	}
	//}
	//catch (const core::configuration_error& error)
	//{
	//	m_log->LogString(LEVEL_WARNING, L"Exception reading config key: %s", stow(error.what()));
	//}


	//try
	//{
	//	boost::asio::spawn(r_io, [this,
	//		iLongPoolTimer,
	//		p_io = &r_io,
	//		params_list = std::move(params_list),
	//		m_log = m_log,
	//		connector = m_idapConnector.get(),
	//		pMail = m_idapConnector.get()](boost::asio::yield_context yield)
	//	{

	//		boost::asio::steady_timer timer(*p_io);
	//		boost::system::error_code errorCode;

	//		int initTimerValue = 3;

	//		while (!(*p_io).stopped())
	//		{
	//			timer.expires_from_now(std::chrono::seconds(initTimerValue));
	//			timer.async_wait(yield[errorCode]); //wait for timer

	//			if (errorCode)
	//			{
	//				throw errorCode;
	//			}

	//			initTimerValue = iLongPoolTimer;

	//			std::vector <std::future <bool>> tasks;

	//			for (auto& params : params_list)
	//			{
	//				tasks.emplace_back(std::async(std::launch::async, [pMail, this, params = std::move(const_cast<TImapParams&>(params))]()
	//				{
	//					return pMail->GetMail(this, std::move(params));
	//				}));
	//			}

	//		}
	//	});

	//}
	//catch (std::exception& e) {
	//	m_log->LogString(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
	//}

	return 0;
}


void CDataReceiverImpl::set_Subscribes()
{
	subscribe(L"S2MCC_Reply", &CDataReceiverImpl::onReplyEventHandler);
}

void CDataReceiverImpl::stop()
{
	r_io.stop();
}

void CDataReceiverImpl::on_routerConnect()
{
	m_log->LogStringModule(LEVEL_INFO, L"Receiver Core", L"Router has connected");
}

void CDataReceiverImpl::on_routerDeliverError(CMessage& msg, const CLIENT_ADDRESS &, const CLIENT_ADDRESS &)
{
	m_log->LogStringModule(LEVEL_FINE, L"Router client", msg.Dump().c_str());
}

void CDataReceiverImpl::on_appStatusChange(DWORD dwClient, core::ClientStatus nStatus)
{
	if (nStatus == (core::ClientStatus::CS_DISCONNECTED))
	{
		try
		{

		}
		catch (...)
		{

		}
	}

}



/******************************* eof *************************************/