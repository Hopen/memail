/************************************************************************/
/* Name     : MEMAIL\SMTP\SmtpClient.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 16 Aug 2016                                               */
/************************************************************************/
#include "stdafx.h"
//#include <boost/program_options/detail/convert.hpp>
#include "boost/asio/ssl.hpp"
#include <boost/asio/spawn.hpp>
#include "SmtpClient.h"
#include "codec.h"


const std::string END_OF_MESSAGE_TEXT = ".\r\n";


//inline auto read_complete_predicate (int& size_response, const std::string& data)
//{
//	if (size_response && size_response == data.size())
//	{
//		size_response = 0;//init again
//		return true;
//	}
//
//	if (data.empty())
//		return true;
//
//	size_response = data.size();
//
//	return false;
//}
//
//
//template <class TSocket>
//inline auto ReadSome (TSocket &_socket, boost::asio::yield_context &yield)
//{
//	std::string in_data;
//	boost::system::error_code errorCode;
//	//do
//	//{
//	char reply_[1024] = {};
//	_socket.async_read_some(boost::asio::buffer(reply_), yield[errorCode]);
//	if (errorCode)
//	{
//		throw errorCode;
//	}
//
//	in_data += reply_;
//
//	//} while (!read_complete_predicate(in_data));
//
//	return in_data;
//}
//
//template <class TSocket>
//inline auto WriteSome(TSocket &_socket, boost::asio::yield_context &yield, std::string request)
//{
//	boost::system::error_code errorCode;
//
//	_socket.async_write_some(boost::asio::buffer(std::move(request)), yield[errorCode]);
//	if (errorCode)
//	{
//		throw errorCode;
//	}
//};

template <class TSocket, class TYield>
inline auto Work(TSocket && _socket, TYield&& yield, TSmtpParams params, CSystemLog* pLog, MessageCollector msg_list)
{
	//////

	auto LogStringModule = [pLog = pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
	{
		if (!pLog)
			return;
		pLog->LogStringModule(level, L"smtp client", formatMessage, std::forward<decltype(args)>(args)...);
	};


	int size_response = 0;
	auto read_complete_predicate = [&](const std::string& data)
	{
		if (size_response && size_response == data.size())
		{
			size_response = 0;//init again
			return true;
		}

		if (data.empty())
			return true;

		size_response = data.size();

		return false;
	};

	boost::system::error_code errorCode;

	auto ReadSome = [&_socket, &errorCode, &read_complete_predicate, yield = std::forward<TYield>(yield)]()
	{
		std::string in_data;
		//do
		//{
			char reply_[1024] = {};
			_socket.async_read_some(boost::asio::buffer(reply_), yield[errorCode]);
			if (errorCode)
			{
				throw errorCode;
			}

			in_data += reply_;

		//} while (!read_complete_predicate(in_data));

		return in_data;
	};

	auto WriteSome = [&_socket, &errorCode, yield = std::forward<TYield>(yield)](std::string request)
	{
		_socket.async_write_some(boost::asio::buffer(std::move(request)), yield[errorCode]);
		if (errorCode)
		{
			throw errorCode;
		}
	};

	bool bPIPELINING = false,
		bBITMIME = false,
		bAUTHLOGIN = false;
	int codeStatus = 0;

	auto ParseResponse = [&codeStatus, &bPIPELINING, &bBITMIME, &bAUTHLOGIN, &LogStringModule](std::string response)
	{
		bool sOK = true;

		auto lambda = [&](const smtpreadstatusparser::TCommand& command_status, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
		{
			switch (command_status.code)
			{
			case -8:case 211:case 214:case 220:case 221:case 250:case 251:case 252:case 354:
			{
				if (command_status.data == "PIPELINING")
					bPIPELINING = true;
				else if (command_status.data == "BITMIME")
					bBITMIME = true;
				else if ((command_status.data.find("AUTH") != std::string::npos) && (command_status.data.find("LOGIN") != std::string::npos))
					bAUTHLOGIN = true;

				break;
			}
			case 421:case 450:case 451:case 452:case 500:case 501:case 502:case 503:
			case 504:case 550:case 551:case 552:case 553:case 554:
			{
				sOK = false;
				codeStatus = command_status.code;
				break;
			}
			default:
			{
				//unknown
				break;
			}
			};

		};

		sOK = smtpreadstatusparser::parse(
			response.begin(),
			response.end(),
			lambda);

		return sOK;
	};

	////

	bool bMakeQuit = false;

	////clear socket buffer
	std::string in_data = ReadSome();

	// hello SMTP Server!
	WriteSome(std::move((boost::format("EHLO %s\r\n") % wtos(params._sSmtpClientAddress).c_str()).str()));
	bMakeQuit = !ParseResponse(std::move(ReadSome()));

	if (!bMakeQuit && bAUTHLOGIN)
	{
		//logining to SMTP 
		WriteSome(std::move(std::string("auth login\r\n")));
		in_data = ReadSome();
		WriteSome(std::move((boost::format("%s\r\n") % encodeBase64(wtos(params._sLogin)).c_str()).str()));
		in_data = ReadSome();
		WriteSome(std::move((boost::format("%s\r\n") % encodeBase64(wtos(params._sPassword)).c_str()).str()));
		bMakeQuit = !ParseResponse(std::move(ReadSome()));
	}


	if (!bMakeQuit)
	{
		for each (auto& newMsg in msg_list)
		{
			bool isOK = true;

			if (params._accepted)
				params._accepted(newMsg);

			std::wstring sTo = newMsg.SafeReadParam(L"Customer", core::checked_type::String, L"").AsWideStr(),
				sFrom = newMsg.SafeReadParam(L"Operator", core::checked_type::String, L"").AsWideStr(),
				sSubject = newMsg.SafeReadParam(L"MessageTitle", core::checked_type::String, L"").AsWideStr(),
				sMessage = newMsg.SafeReadParam(L"Message", core::checked_type::String, L"").AsWideStr(),
				sCC = newMsg.SafeReadParam(L"CC", core::checked_type::String, L"").AsWideStr(),
				sBCC = newMsg.SafeReadParam(L"BCC", core::checked_type::String, L"").AsWideStr(),
				sOperatorName = newMsg.SafeReadParam(L"OperatorName", core::checked_type::String, L"").AsWideStr();

			std::vector <std::wstring> toList, ccList, bccList;

			std::set<std::wstring> allList;

			boost::split(toList, sTo, std::bind2nd(std::equal_to<wchar_t>(), ';'));
			boost::split(ccList, sCC, std::bind2nd(std::equal_to<wchar_t>(), ';'));
			boost::split(bccList, sBCC, std::bind2nd(std::equal_to<wchar_t>(), ';'));

			allList.insert(toList.begin(), toList.end());
			allList.insert(ccList.begin(), ccList.end());
			allList.insert(bccList.begin(), bccList.end());

			int attachmentsCount = newMsg.SafeReadParam(L"AttachCount", core::checked_type::Int, 0).AsInt();

			WriteSome(std::move((boost::format("mail from:<%s>\r\n") % wtos(sFrom).c_str()).str()));
			isOK = ParseResponse(std::move(ReadSome()));

			if (isOK)
			{
				for each (auto address in allList)
				{
					if (!address.empty())
					{
						WriteSome(std::move((boost::format("rcpt to:<%s>\r\n") % wtos(address).c_str()).str()));
						isOK = ParseResponse(std::move(ReadSome()));

						if (!isOK)
							break;
					}
				}
			}

			if (isOK)
			{
				WriteSome(std::move(std::string("data\r\n")));
				ReadSome();

				std::string outString;
				if (1)
				{
					std::wstring sTemp;
					if (!sSubject.empty())
					{
						sTemp = (boost::wformat(L"subject:%s\r\n") % sSubject.c_str()).str();
					}

					if (!sOperatorName.empty())
					{
						sTemp += (boost::wformat(L"from:\"%s\"<%s>\r\nTo:%s\r\n")
							% sOperatorName.c_str()
							% sFrom.c_str()
							% sTo.c_str()).str();

					}
					else
					{
						sTemp += (boost::wformat(L"from:<%s>\r\nTo:%s\r\n")
							% sFrom.c_str()
							% sTo.c_str()).str();
					}


					if (!sCC.empty())
					{
						sTemp += (boost::wformat(L"CC:%s\r\n")
							% sCC.c_str()).str();
					}

					if (!sBCC.empty())
					{
						sTemp += (boost::wformat(L"BCC:%s\r\n")
							% sBCC.c_str()).str();
					}

					sTemp += L"MIME-Version: 1.0\r\n";
					if (!attachmentsCount)
					{
						//if (!sImapMessageType.compare(L"text.html"))
						sTemp += L"Content-type: text/html; charset=utf-8\r\n";
						//else
						//	sTemp += L"Content-type: text/plain; charset=utf-8\r\n";

						//sTemp += L"Content-Transfer-Encoding: base64\r\n";
						sTemp += L"\r\n";
						sTemp += sMessage;
					}
					else
					{
						sTemp += L"Content-type: multipart/mixed; boundary=\"frontier\"\r\n";
						sTemp += L"\r\n";

						if (!sMessage.empty())
						{
							sTemp += L"--frontier\r\n";

							//if (!sImapMessageType.compare(L"text.html"))
							sTemp += L"Content-type: text/html; charset=utf-8\r\n";
							//else
							//	sTemp += L"Content-type: text/plain; charset=utf-8\r\n";

							//sTemp += L"Content-Transfer-Encoding: base64\r\n";
							sTemp += L"\r\n";
							sTemp += sMessage;
							sTemp += L"\r\n\r\n";
						}
					}

					outString += wtos(sTemp);
				}


				if (attachmentsCount)
				{
					for (int i = 0; i < attachmentsCount; ++i)
					{
						std::string sSource, sFileName;
						std::wstring attachUrlName = L"Attach" + toStr<wchar_t>(i) + L"Name",
							attachSourceName = L"Attach" + toStr<wchar_t>(i) + L"Source";

						auto attach = newMsg.SafeReadParam(attachSourceName, core::checked_type::String, L"");
						if (!attach.IsEmpty() && attach.AsStr().length())
						{
							sSource = attach.AsStr();
						}
						else
						{
							continue;
						}

						auto fileName = newMsg.SafeReadParam(attachUrlName, core::checked_type::String, L"");
						if (!fileName.IsEmpty() && fileName.AsStr().length())
						{
							sFileName = fileName.AsStr();
						}

						if (sSource.empty() || sFileName.empty())
							continue;

						std::string sTemp = "--frontier\r\n";
						sTemp += (boost::format("Content-Disposition: attachment; filename=\"%s\"\r\n")
							% sFileName.c_str()).str();
						sTemp += "Content-Transfer-Encoding: base64\r\n";
						sTemp += "\r\n";
						//push_command(sTemp, nullptr, boost::bind(&smtp_client_impl::on_failed, this, newMsg, _1, _2));

						sTemp += sSource;
						sTemp += "\r\n";

						outString += sTemp;
					}

					outString += "--frontier\r\n";
				}

				outString += "\r\n" + END_OF_MESSAGE_TEXT;

				WriteSome(std::move(outString));
				isOK = ParseResponse(std::move(ReadSome()));
			}

			if (isOK)
			{
				if (params._completed)
					params._completed(newMsg);
			}
			else
			{
				if (params._failed)
					params._failed(newMsg);
			}
		}
	}

	WriteSome(std::move(std::string("quit\r\n")));

	LogStringModule(LEVEL_FINEST, L"%s", ReadSome().c_str());
}



void makeSSL_SmtpClient(boost::asio::io_service &io_service, TSmtpParams params, CSystemLog* pLog, MessageCollector msg_list)
{
	boost::asio::spawn(io_service, [&io_service, params = std::move(params), pLog, msg_list = std::move(msg_list)]
		(boost::asio::yield_context yield)
	{
		auto LogStringModule = [pLog = pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
		{
			if (!pLog)
				return;
			pLog->LogStringModule(level, L"smtp client", formatMessage, std::forward<decltype(args)>(args)...);
		};


		try
		{
			boost::asio::ip::tcp::resolver resolver(io_service);
			boost::asio::ip::tcp::resolver::query query(wtos(params._sSmtpServer), wtos(params._sSmtpPort));
			boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
			boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();

			boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
			ctx.set_default_verify_paths();

			LogStringModule(LEVEL_INFO, L"Using SSL Certificate [%s]", params._sCertificate.c_str());
			ctx.load_verify_file(wtos(params._sCertificate));

			boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket(io_service, ctx);
			boost::system::error_code errorCode;

			boost::asio::async_connect(_socket.lowest_layer(), iterator, yield[errorCode]);

			if (errorCode)
			{
				LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
				throw errorCode;
			}

			_socket.set_verify_mode(boost::asio::ssl::verify_peer);

			_socket.set_verify_callback([&](bool preverified,
				boost::asio::ssl::verify_context& ctx)
			{
				// The verify callback can be used to check whether the certificate that is
				// being presented is valid for the peer. For example, RFC 2818 describes
				// the steps involved in doing this for HTTPS. Consult the OpenSSL
				// documentation for more details. Note that the callback is called once
				// for each certificate in the certificate chain, starting from the root
				// certificate authority.

				// In this example we will simply print the certificate's subject name.
				char subject_name[256];
				X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
				X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);

				//std::cout << subject_name << std::endl;

				LogStringModule(LEVEL_INFO, L"Sertificat verification answer: \"%s\"\nVerified = \"%s\"", stow(subject_name).c_str(), preverified ? L"true" : L"false");

				return preverified;
			});
			_socket.async_handshake(boost::asio::ssl::stream_base::client, yield[errorCode]);

			if (errorCode)
			{
				LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
				throw errorCode;
			}
			
			Work(_socket, yield, std::move(params), pLog, std::move(msg_list));
		}
		catch (std::exception& e) {
			LogStringModule(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
		}
	}

	);//end corutine
}

void makeNoSSL_SmtpClient(boost::asio::io_service & io_service, TSmtpParams params, CSystemLog * pLog, MessageCollector msg_list)
{
	boost::asio::spawn(io_service, [&io_service, params = std::move(params), pLog, msg_list = std::move(msg_list)]
		(boost::asio::yield_context yield)
	{
		auto LogStringModule = [pLog = pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
		{
			if (!pLog)
				return;
			pLog->LogStringModule(level, L"smtp client", formatMessage, std::forward<decltype(args)>(args)...);
		};


		try
		{
			boost::asio::ip::tcp::resolver resolver(io_service);
			boost::asio::ip::tcp::resolver::query query(wtos(params._sSmtpServer), wtos(params._sSmtpPort));
			boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
			boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();

			boost::asio::ip::tcp::socket _socket(io_service);
			boost::system::error_code errorCode;

			boost::asio::async_connect(_socket.lowest_layer(), iterator, yield[errorCode]);

			if (errorCode)
			{
				LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
				throw errorCode;
			}

			Work(_socket, yield, params, pLog, msg_list);
		}
		catch (std::exception& e) {
			LogStringModule(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
		}
	}

	);//end corutine

	////boost::asio::spawn(io_service, [&](boost::asio::yield_context yield)
	////{
	////	int test = 0;
	////});

	//boost::asio::spawn(io_service, [&io_service, params = std::move(params), pLog, msg_list = std::move(msg_list)]
	//	(boost::asio::yield_context yield)
	//{
	//	auto LogStringModule = [pLog = pLog](LogLevel level, const std::wstring& formatMessage, auto&& ...args)
	//	{
	//		if (!pLog)
	//			return;
	//		pLog->LogStringModule(level, L"smtp client", formatMessage, std::forward<decltype(args)>(args)...);
	//	};

	//	int size_response = 0;
	//	//auto read_complete_predicate = [&](const std::string& data)
	//	//{
	//	//	if (size_response && size_response == data.size())
	//	//	{
	//	//		size_response = 0;//init again
	//	//		return true;
	//	//	}

	//	//	if (data.empty())
	//	//		return true;

	//	//	size_response = data.size();

	//	//	return false;
	//	//};

	//	bool bPIPELINING = false,
	//		bBITMIME = false,
	//		bAUTHLOGIN = false;

	//	int codeStatus = 0;


	//	try
	//	{
	//		boost::asio::ip::tcp::resolver resolver(io_service);
	//		boost::asio::ip::tcp::resolver::query query(wtos(params._sSmtpServer), wtos(params._sSmtpPort));
	//		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);
	//		boost::asio::ip::tcp::endpoint endpoint = iterator->endpoint();

	//		boost::asio::ip::tcp::socket _socket(io_service);
	//		boost::system::error_code errorCode;

	//		auto ParseResponse = [&codeStatus, &bPIPELINING, &bBITMIME, &bAUTHLOGIN, &LogStringModule](std::string response)
	//		{
	//			bool sOK = true;

	//			auto lambda = [&](const smtpreadstatusparser::TCommand& command_status, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)
	//			{
	//				switch (command_status.code)
	//				{
	//				case -8:case 211:case 214:case 220:case 221:case 250:case 251:case 252:case 354:
	//				{
	//					if (command_status.data == "PIPELINING")
	//						bPIPELINING = true;
	//					else if (command_status.data == "BITMIME")
	//						bBITMIME = true;
	//					else if ((command_status.data.find("AUTH") != std::string::npos) && (command_status.data.find("LOGIN") != std::string::npos))
	//						bAUTHLOGIN = true;

	//					break;
	//				}
	//				case 421:case 450:case 451:case 452:case 500:case 501:case 502:case 503:
	//				case 504:case 550:case 551:case 552:case 553:case 554:
	//				{
	//					sOK = false;
	//					codeStatus = command_status.code;
	//					break;
	//				}
	//				default:
	//				{
	//					//unknown
	//					break;
	//				}
	//				};

	//			};

	//			sOK = smtpreadstatusparser::parse(
	//				response.begin(),
	//				response.end(),
	//				lambda);

	//			return sOK;
	//		};

	//		////

	//		boost::asio::async_connect(_socket.lowest_layer(), iterator, yield[errorCode]);

	//		if (errorCode)
	//		{
	//			LogStringModule(LEVEL_INFO, L"errorCode: %i", errorCode.value());
	//			throw errorCode;
	//		}

	//		std::string in_data;

	//		bool bMakeQuit = false;

	//		////clear socket buffer
	//		//std::string in_data = ReadSome(_socket, yield);
	//		ClearSocketBuffer(_socket, yield);

	//		// hello SMTP Server!
	//		WriteSome(_socket, yield, std::move((boost::format("EHLO %s\r\n") % wtos(params._sSmtpClientAddress).c_str()).str()));
	//		bMakeQuit = !ParseResponse(std::move(ReadSome(_socket, yield)));

	//		if (!bMakeQuit && bAUTHLOGIN)
	//		{
	//			//logining to SMTP 
	//			WriteSome(_socket, yield, std::move(std::string("auth login\r\n")));
	//			in_data = ReadSome(_socket, yield);
	//			WriteSome(_socket, yield, std::move((boost::format("%s\r\n") % encodeBase64(wtos(params._sLogin)).c_str()).str()));
	//			in_data = ReadSome(_socket, yield);
	//			WriteSome(_socket, yield, std::move((boost::format("%s\r\n") % encodeBase64(wtos(params._sPassword)).c_str()).str()));
	//			bMakeQuit = !ParseResponse(std::move(ReadSome(_socket, yield)));
	//		}


	//		if (!bMakeQuit)
	//		{
	//			for each (auto& newMsg in msg_list)
	//			{
	//				bool isOK = true;

	//				if (params._accepted)
	//					params._accepted(newMsg);

	//				std::wstring sTo = newMsg.SafeReadParam(L"Customer", core::checked_type::String, L"").AsWideStr(),
	//					sFrom = newMsg.SafeReadParam(L"Operator", core::checked_type::String, L"").AsWideStr(),
	//					sSubject = newMsg.SafeReadParam(L"MessageTitle", core::checked_type::String, L"").AsWideStr(),
	//					sMessage = newMsg.SafeReadParam(L"Message", core::checked_type::String, L"").AsWideStr(),
	//					sCC = newMsg.SafeReadParam(L"CC", core::checked_type::String, L"").AsWideStr(),
	//					sBCC = newMsg.SafeReadParam(L"BCC", core::checked_type::String, L"").AsWideStr(),
	//					sOperatorName = newMsg.SafeReadParam(L"OperatorName", core::checked_type::String, L"").AsWideStr();

	//				std::vector <std::wstring> toList, ccList, bccList;

	//				std::set<std::wstring> allList;

	//				boost::split(toList, sTo, std::bind2nd(std::equal_to<wchar_t>(), ';'));
	//				boost::split(ccList, sCC, std::bind2nd(std::equal_to<wchar_t>(), ';'));
	//				boost::split(bccList, sBCC, std::bind2nd(std::equal_to<wchar_t>(), ';'));

	//				allList.insert(toList.begin(), toList.end());
	//				allList.insert(ccList.begin(), ccList.end());
	//				allList.insert(bccList.begin(), bccList.end());

	//				int attachmentsCount = newMsg.SafeReadParam(L"AttachCount", core::checked_type::Int, 0).AsInt();

	//				WriteSome(_socket, yield, std::move((boost::format("mail from:<%s>\r\n") % wtos(sFrom).c_str()).str()));
	//				isOK = ParseResponse(std::move(ReadSome(_socket, yield)));

	//				if (isOK)
	//				{
	//					for each (auto address in allList)
	//					{
	//						if (!address.empty())
	//						{
	//							WriteSome(_socket, yield, std::move((boost::format("rcpt to:<%s>\r\n") % wtos(address).c_str()).str()));
	//							isOK = ParseResponse(std::move(ReadSome(_socket, yield)));

	//							if (!isOK)
	//								break;
	//						}
	//					}
	//				}

	//				if (isOK)
	//				{
	//					WriteSome(_socket, yield, std::move(std::string("data\r\n")));
	//					ReadSome(_socket, yield);

	//					std::string outString;
	//					if (1)
	//					{
	//						std::wstring sTemp;
	//						if (!sSubject.empty())
	//						{
	//							sTemp = (boost::wformat(L"subject:%s\r\n") % sSubject.c_str()).str();
	//						}

	//						sTemp += (boost::wformat(L"from:\"%s\"<%s>\r\nTo:%s\r\n")
	//							% sOperatorName.c_str()
	//							% sFrom.c_str()
	//							% sTo.c_str()).str();

	//						if (!sCC.empty())
	//						{
	//							sTemp += (boost::wformat(L"CC:%s\r\n")
	//								% sCC.c_str()).str();
	//						}

	//						if (!sBCC.empty())
	//						{
	//							sTemp += (boost::wformat(L"BCC:%s\r\n")
	//								% sBCC.c_str()).str();
	//						}

	//						sTemp += L"MIME-Version: 1.0\r\n";
	//						if (!attachmentsCount)
	//						{
	//							//if (!sImapMessageType.compare(L"text.html"))
	//							sTemp += L"Content-type: text/html; charset=utf-8\r\n";
	//							//else
	//							//	sTemp += L"Content-type: text/plain; charset=utf-8\r\n";

	//							//sTemp += L"Content-Transfer-Encoding: base64\r\n";
	//							sTemp += L"\r\n";
	//							sTemp += sMessage;
	//						}
	//						else
	//						{
	//							sTemp += L"Content-type: multipart/mixed; boundary=\"frontier\"\r\n";
	//							sTemp += L"\r\n";

	//							if (!sMessage.empty())
	//							{
	//								sTemp += L"--frontier\r\n";

	//								//if (!sImapMessageType.compare(L"text.html"))
	//								sTemp += L"Content-type: text/html; charset=utf-8\r\n";
	//								//else
	//								//	sTemp += L"Content-type: text/plain; charset=utf-8\r\n";

	//								//sTemp += L"Content-Transfer-Encoding: base64\r\n";
	//								sTemp += L"\r\n";
	//								sTemp += sMessage;
	//								sTemp += L"\r\n\r\n";
	//							}
	//						}

	//						outString += wtos(sTemp);
	//					}


	//					if (attachmentsCount)
	//					{
	//						for (int i = 0; i < attachmentsCount; ++i)
	//						{
	//							std::string sSource, sFileName;
	//							std::wstring attachUrlName = L"Attach" + toStr<wchar_t>(i) + L"Name",
	//								attachSourceName = L"Attach" + toStr<wchar_t>(i) + L"Source";

	//							auto attach = newMsg.SafeReadParam(attachSourceName, core::checked_type::String, L"");
	//							if (!attach.IsEmpty() && attach.AsStr().length())
	//							{
	//								sSource = attach.AsStr();
	//							}
	//							else
	//							{
	//								continue;
	//							}

	//							auto fileName = newMsg.SafeReadParam(attachUrlName, core::checked_type::String, L"");
	//							if (!fileName.IsEmpty() && fileName.AsStr().length())
	//							{
	//								sFileName = fileName.AsStr();
	//							}

	//							if (sSource.empty() || sFileName.empty())
	//								continue;

	//							std::string sTemp = "--frontier\r\n";
	//							sTemp += (boost::format("Content-Disposition: attachment; filename=\"%s\"\r\n")
	//								% sFileName.c_str()).str();
	//							sTemp += "Content-Transfer-Encoding: base64\r\n";
	//							sTemp += "\r\n";
	//							//push_command(sTemp, nullptr, boost::bind(&smtp_client_impl::on_failed, this, newMsg, _1, _2));

	//							sTemp += sSource;
	//							sTemp += "\r\n";

	//							outString += sTemp;
	//						}

	//						outString += "--frontier\r\n";
	//					}

	//					outString += "\r\n" + END_OF_MESSAGE_TEXT;

	//					WriteSome(_socket, yield, std::move(outString));
	//					isOK = ParseResponse(std::move(ReadSome(_socket, yield)));
	//				}

	//				if (isOK)
	//				{
	//					if (params._completed)
	//						params._completed(newMsg);
	//				}
	//				else
	//				{
	//					if (params._failed)
	//						params._failed(newMsg);
	//				}
	//			}
	//		}

	//		WriteSome(_socket, yield, std::move(std::string("quit\r\n")));

	//		LogStringModule(LEVEL_FINEST, L"%s", ReadSome(_socket, yield).c_str());
	//	}
	//	catch (std::exception& e) {
	//		LogStringModule(LEVEL_WARNING, L"Exception: %s", stow(e.what()).c_str());
	//	}





	//}

	//);//end corutine
}
/******************************* eof *************************************/