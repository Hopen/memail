/************************************************************************/
/* Name     : MCC\ImapListParser.cpp.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 1 Jun 2016                                                */
/************************************************************************/
#include "stdafx.h"
#include "ImapFetchAllParser.h"

//#include <string>
//#include <functional>
//
//#include <boost/format.hpp>
//#  pragma warning(disable:4348)
//#include <boost/config/warning_disable.hpp>
//#include <boost/spirit/include/qi.hpp>
//#include "Patterns/string_functions.h"
//
//#ifdef PARSER_ENABLE
//#include <boost/spirit/include/phoenix_operator.hpp>
//#include <boost/spirit/include/phoenix_fusion.hpp>
//#include <boost/fusion/include/adapt_struct.hpp>
//#include <boost/variant/recursive_variant.hpp>
//#include <boost/spirit/include/phoenix_stl.hpp>
//#include <boost/spirit/include/qi_no_skip.hpp>
//#endif

namespace listparser
{
	struct TIMAPStatus
	{
		int _number;
		std::string _status;
		std::string _extra;
	};

}

#ifdef PARSER_ENABLE

BOOST_FUSION_ADAPT_STRUCT(
	listparser::TIMAPStatus,
	(int, _number)
	(std::string, _status)
	(std::string, _extra)
	)

#endif

namespace listparser
{
	using ParserCallBack = std::function <void(const std::string& name, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)>;

	template <typename Iterator>
	bool parse(Iterator first, Iterator last, ParserCallBack _callback, std::string& e_what)
	{
#ifdef PARSER_ENABLE

		//rules
		boost::spirit::qi::rule<Iterator, std::string(), boost::spirit::ascii::space_type> flags, name, list, lastname;

		flags = boost::spirit::lit('(')
			>> *(
				boost::spirit::qi::string("\\Inbox") |
				boost::spirit::qi::string("\\Spam") |
				boost::spirit::qi::string("\\Sent") |
				boost::spirit::qi::string("\\Drafts") |
				boost::spirit::qi::string("\\Trash") |
				boost::spirit::qi::string("\\Noinferiors") |
				boost::spirit::qi::string("\\Noselect") |
				boost::spirit::qi::string("\\Marked") |
				boost::spirit::qi::string("\\Unmarked") |
				boost::spirit::qi::string("\\HasNoChildren")
				)
			>> boost::spirit::lit(')');

		name = '\"'
			>> boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
			>> '\"';

		lastname = boost::spirit::no_skip[+~boost::spirit::qi::char_('\n')];


		list = "* LIST"
			>> flags
			>> name
			>> lastname[_callback];

		bool r = false;

		try
		{
			r = boost::spirit::qi::phrase_parse(
				first,
				last,
				(
					*list
					),
				boost::spirit::ascii::space
				);

		}
		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
		{
			std::string error(e.first, e.last);
			e_what = error;
		}
		catch (const std::exception& e)
		{
			e_what = e.what();
		}
		catch (...)
		{
			e_what = "Unknown exception";
		}

		if (first != last) // fail if we did not get a full match
			return false;

		return r;
#else

		return false;
#endif
	}

	template <typename Iterator>
	bool parse2(Iterator first, Iterator last, ParserCallBack _callback, std::string& e_what)
	{
#ifdef PARSER_ENABLE

		//rules
		boost::spirit::qi::rule<Iterator, std::string(), boost::spirit::ascii::space_type> flags, name, list, lastname;
		boost::spirit::qi::rule<std::string::iterator, std::string(), boost::spirit::ascii::space_type> type;
		boost::spirit::qi::rule<std::string::iterator, TIMAPStatus(), boost::spirit::ascii::space_type> answer;

		type = +(boost::spirit::qi::string("OK") |
			boost::spirit::qi::string("BAD") |
			boost::spirit::qi::string("NO"));

		answer = boost::spirit::lit('a')
			>> (boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])
			>> type[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> +(boost::spirit::qi::char_/*[boost::phoenix::at_c<2>(boost::spirit::_val) += boost::spirit::qi::_1]*/)
			;


		flags = boost::spirit::lit('(')
			>> *(
				boost::spirit::qi::string("\\Inbox") |
				boost::spirit::qi::string("\\Spam") |
				boost::spirit::qi::string("\\Sent") |
				boost::spirit::qi::string("\\Drafts") |
				boost::spirit::qi::string("\\Trash") |
				boost::spirit::qi::string("\\Noinferiors") |
				boost::spirit::qi::string("\\Noselect") |
				boost::spirit::qi::string("\\Marked") |
				boost::spirit::qi::string("\\Unmarked") |
				boost::spirit::qi::string("\\HasNoChildren")
				)
			>> boost::spirit::lit(')');

		name = '\"'
			>> boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
			>> '\"';

		lastname = boost::spirit::no_skip[+~boost::spirit::qi::char_('\n')];


		list = (("* LIST"
			>> flags
			>> name
			>> lastname[_callback]) | answer);

		bool r = false;

		try
		{
			r = boost::spirit::qi::phrase_parse(
				first,
				last,
				(
					*list
					),
				boost::spirit::ascii::space
				);

		}
		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
		{
			std::string error(e.first, e.last);
			e_what = error;
		}
		catch (const std::exception& e)
		{
			e_what = e.what();
		}
		catch (...)
		{
			e_what = "Unknown exception";
		}

		if (first != last) // fail if we did not get a full match
			return false;

		return r;
#else

		return false;
#endif
	}

}
bool listparser_parse(std::string source, std::function<void(const std::string&box_name, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString)
{
	bool r = listparser::parse(
		source.begin(),
		source.end(),
		_callback,
		errString
		);

	return r;
}

bool listparser_parse2(std::string source, std::function<void(const std::string&box_name, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString)
{
	bool r = listparser::parse2(
		source.begin(),
		source.end(),
		_callback,
		errString
		);

	return r;
}

/******************************* eof *************************************/