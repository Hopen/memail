/************************************************************************/
/* Name     : MCC\ImapSubjectParser.h                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 01 Apr 2016                                               */
/************************************************************************/

#pragma once


//#include <string>
//#include <functional>
//
//#include <boost/format.hpp>
//#  pragma warning(disable:4348)
//#include <boost/config/warning_disable.hpp>
//#include <boost/spirit/include/qi.hpp>
//#include "Patterns/string_functions.h"
//
//#ifdef PARSER_ENABLE
//#include <boost/spirit/include/phoenix_operator.hpp>
//#include <boost/spirit/include/phoenix_fusion.hpp>
//#include <boost/fusion/include/adapt_struct.hpp>
//#include <boost/variant/recursive_variant.hpp>
//#include <boost/spirit/include/phoenix_stl.hpp>
//#include <boost/spirit/include/qi_no_skip.hpp>
//#endif
namespace imapsubjectparser
{
	struct TSubject
	{
		std::string _charset;
		std::string _coding;
		std::string _subject;
	};
}

BOOST_FUSION_ADAPT_STRUCT(
	imapsubjectparser::TSubject,
	(std::string, _charset)
	(std::string, _coding)
	(std::string, _subject)
	)

namespace imapsubjectparser
{
	using ParserCallBack = std::function <void(const TSubject& subj, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)>;

	template <typename Iterator>
	bool parse(Iterator first, Iterator last, ParserCallBack _callback, std::string& e_what)
	{
#ifdef PARSER_ENABLE

		//rules
		boost::spirit::qi::rule<std::string::iterator, std::string(), boost::spirit::ascii::space_type> name, coding;
		boost::spirit::qi::rule<std::string::iterator, TSubject(), boost::spirit::ascii::space_type> subject;

		name = '?'
			>> boost::spirit::no_skip[+~boost::spirit::qi::char_('?')]
			>> '?';


		coding = (boost::spirit::ascii::no_case[boost::spirit::qi::string("b")] |
			//boost::spirit::qi::string("b") |
			//boost::spirit::qi::string("Q") |
			boost::spirit::ascii::no_case[boost::spirit::qi::string("q")]);

		subject = boost::spirit::lit('=')
			>> name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			//>> boost::spirit::lit('B')
			>> coding[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> name[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> boost::spirit::lit('=')
			>> -boost::spirit::qi::int_
			;

		bool r = false;

		//TSubject output;

		try
		{
			r = boost::spirit::qi::phrase_parse(
				first,
				last,
				(
					*subject[_callback]
					),
				boost::spirit::ascii::space/*,
				output*/
				);

		}
		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
		{
			std::string error(e.first, e.last);
			e_what = error;

		}
		catch (const std::exception& e)
		{
			e_what = e.what();
		}
		catch (...)
		{
			e_what = "Unknown exception";
		}


		if (first != last) // fail if we did not get a full match
			return false;

		return r;
#else

		return false;
#endif
	}
}

/******************************* eof *************************************/