/************************************************************************/
/* Name     : MCC\ImapFetchAllParser.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 1 Jun 2016                                                */
/************************************************************************/
#include "stdafx.h"
#include "ImapFetchAllParser.h"

namespace fetchallparser
{
	struct TIMAPStatus
	{
		int _number;
		std::string _status;
		std::string _extra;
	};

}

#ifdef PARSER_ENABLE

BOOST_FUSION_ADAPT_STRUCT(
	fetchallparser::TDate,
	(int, day)
	(std::string, month)
	(int, year)
	)



	BOOST_FUSION_ADAPT_STRUCT(
		fetchallparser::TTime,
		(int, hours)
		(int, minutes)
		(int, seconds)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchallparser::TUtc,
		(char, sing)
		(int, timezone)
		)


	BOOST_FUSION_ADAPT_STRUCT(
		fetchallparser::TDateTime,
		(fetchallparser::TDate, date)
		(fetchallparser::TTime, time)
		//(std::string, utc)
		(fetchallparser::TUtc, utc)
		)


	BOOST_FUSION_ADAPT_STRUCT(
		fetchallparser::TAddress,
		(std::string, name)
		(std::string, box_name)
		(std::string, domain)
		)


	BOOST_FUSION_ADAPT_STRUCT(
		fetchallparser::TAddressList,
		(std::vector<fetchallparser::TAddress>, addressList)
		)


	BOOST_FUSION_ADAPT_STRUCT(
		fetchallparser::TEnvelope,
		(fetchallparser::TDateTime, send_date)
		(std::string, subject)
		(fetchallparser::TAddress, from)
		(fetchallparser::TAddressList, to)
		(fetchallparser::TAddressList, cc)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchallparser::TRFC822,
		(int, size)
		)



	BOOST_FUSION_ADAPT_STRUCT(
		fetchallparser::THeader,
		(int, uid)
		(std::string, flag)
		(fetchallparser::TDateTime, receive_date)
		(fetchallparser::TRFC822, size)
		(fetchallparser::TEnvelope, envelope)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		fetchallparser::TDocument,
		(int, num)
		(fetchallparser::THeader, header)
		)
	
	BOOST_FUSION_ADAPT_STRUCT(
		fetchallparser::TIMAPStatus,
		(int, _number)
		(std::string, _status)
		(std::string, _extra)
		)

#endif

namespace fetchallparser {

	using ParserCallBack = std::function <void(const TDocument& answer, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)>;

	template <typename Iterator>
	bool parse(Iterator first, Iterator last, ParserCallBack _callback, std::string& e_what)
	{
#ifdef PARSER_ENABLE

		// rules
		boost::spirit::qi::rule<Iterator, std::string(), boost::spirit::ascii::space_type> flags, month, week_day, /*utc,*/ nil, name, /*utf8,*/ code/*, name2, name3*/;
		boost::spirit::qi::rule<Iterator, TRFC822(), boost::spirit::ascii::space_type> rfc822;
		boost::spirit::qi::rule<Iterator, TDate(), boost::spirit::ascii::space_type> date;
		boost::spirit::qi::rule<Iterator, TTime(), boost::spirit::ascii::space_type> time;
		boost::spirit::qi::rule<Iterator, TUtc(), boost::spirit::ascii::space_type> utc;
		boost::spirit::qi::rule<Iterator, TDateTime(), boost::spirit::ascii::space_type> datetime, datetime_value, internaldate;
		boost::spirit::qi::rule<Iterator, THeader(), boost::spirit::ascii::space_type> header;
		boost::spirit::qi::rule<Iterator, TAddress(), boost::spirit::ascii::space_type> address;
		boost::spirit::qi::rule<Iterator, TAddressList(), boost::spirit::ascii::space_type> addressList;
		boost::spirit::qi::rule<Iterator, TEnvelope(), boost::spirit::ascii::space_type> envelope;
		boost::spirit::qi::rule<Iterator, TDocument(), boost::spirit::ascii::space_type> document;


		nil = boost::spirit::qi::string("NIL");


		flags = boost::spirit::lit('(')
			>> *(
				boost::spirit::qi::string("\\Answered") |
				boost::spirit::qi::string("\\Flagged") |
				boost::spirit::qi::string("\\Deleted") |
				boost::spirit::qi::string("\\Draft") |
				boost::spirit::qi::string("\\Seen") |
				boost::spirit::qi::string("\\Unanswered") |
				boost::spirit::qi::string("\\Unflagged") |
				boost::spirit::qi::string("\\Undeleted") |
				boost::spirit::qi::string("\\Undraft") |
				boost::spirit::qi::string("\\Unseen") |
				boost::spirit::qi::string("Junk") |
				boost::spirit::qi::string("NonJunk") |
				boost::spirit::qi::string("Spam")
				)
			>> boost::spirit::lit(')');

		month = (
			boost::spirit::qi::string("Jan") |
			boost::spirit::qi::string("Feb") |
			boost::spirit::qi::string("Mar") |
			boost::spirit::qi::string("Apr") |
			boost::spirit::qi::string("May") |
			boost::spirit::qi::string("Jun") |
			boost::spirit::qi::string("Jul") |
			boost::spirit::qi::string("Aug") |
			boost::spirit::qi::string("Sep") |
			boost::spirit::qi::string("Oct") |
			boost::spirit::qi::string("Nov") |
			boost::spirit::qi::string("Dec")
			);

		week_day = (
			boost::spirit::qi::string("Mon") |
			boost::spirit::qi::string("Tue") |
			boost::spirit::qi::string("Wed") |
			boost::spirit::qi::string("Thu") |
			boost::spirit::qi::string("Fri") |
			boost::spirit::qi::string("Sat") |
			boost::spirit::qi::string("Sun")
			);

		date = boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])]
			>> -boost::spirit::lit('-')
			>> month[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> -boost::spirit::lit('-')
			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1])];


		time = boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])]
			>> boost::spirit::lit(':')
			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])]
			>> boost::spirit::lit(':')
			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1])];

		utc = boost::spirit::lexeme[(boost::spirit::qi::char_("+-")[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])]
			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])];;


		name = '\"'
			>> boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
			>> '\"';

		//name2 = boost::spirit::lit('{')
		//	>> boost::spirit::qi::int_
		//	>> boost::spirit::lit('}')
		//	>> (boost::spirit::no_skip[+~boost::spirit::qi::char_("(\"")]);

		//name3 = boost::spirit::lit('{')
		//	>> boost::spirit::qi::int_
		//	>> boost::spirit::lit('}')
		//	>> (boost::spirit::no_skip[+~boost::spirit::qi::char_('(')]);

		//utf8 = '\"'
		//	>> boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
		//	>> '\"';

		datetime = -week_day
			>> -boost::spirit::lit(',')
			>> date[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> time[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> -utc[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> -boost::spirit::qi::string("GMT")
			>> -('('
				>> boost::spirit::no_skip[+~boost::spirit::qi::char_(')')]
				>> ')');

		datetime_value = -boost::spirit::lit('\"')
			>> datetime
			>> -boost::spirit::lit('\"');

		internaldate = datetime_value;

		rfc822 = /*"RFC822.SIZE"
				 >>*/ boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])];

		address =
			"("
			>> (nil | "\"\"" | name[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])
			>> nil
			>> (nil | name[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])
			>> (nil | name[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1])
			>> ")"
			;

		addressList = *address[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)];

		envelope = boost::spirit::lit('(')
			>> boost::spirit::lit('\"')
			>> datetime[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> boost::spirit::lit('\"')
			>> (nil | /*name3[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1] | name2[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1] | */name[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])
			>> boost::spirit::lit('(')
			>> address[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> boost::spirit::lit(')')
			//>> +nil
			>> (nil |
				(boost::spirit::lit('(')
					>> address
					>> boost::spirit::lit(')')
					)
				)
			>> (nil |
				(boost::spirit::lit('(')
					>> address
					>> boost::spirit::lit(')')
					)
				)
			>> (nil |
				(boost::spirit::lit('(')
					>> addressList[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
					>> boost::spirit::lit(')')
					)
				)
			>> (nil |
				(boost::spirit::lit('(')
					//>> address[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
					>> addressList[boost::phoenix::at_c<4>(boost::spirit::_val) = boost::spirit::qi::_1]
					>> boost::spirit::lit(')')
					)
				)
			>> nil
			>> (nil | "\"\"" | name)
			>> (nil | "\"\"" | name)
			>> boost::spirit::lit(')');


		header = *("UID" > boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			| "FLAGS" > flags[boost::phoenix::at_c<1>(boost::spirit::_val) += boost::spirit::qi::_1]
			| "INTERNALDATE" > internaldate[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
			| "RFC822.SIZE" > rfc822[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
			| "ENVELOPE" > envelope[boost::phoenix::at_c<4>(boost::spirit::_val) = boost::spirit::qi::_1]
			)
			;


		document = '*'
			>> boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> "FETCH"
			>> '('
			>> header[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> ')'
			>> -('*' >> boost::spirit::qi::int_ >> "EXISTS")
			>> -('*' >> boost::spirit::qi::int_ >> "RECENT");


		TDocument output;

		bool r = false;

		try
		{
			r = boost::spirit::qi::phrase_parse(
				first,
				last,
				(
					document[_callback]
					),
				boost::spirit::ascii::space,
				output
				);
		}
		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
		{
			std::string error(e.first, e.last);
			e_what = error;

		}
		catch (const std::exception& e)
		{
			e_what = e.what();
		}
		catch (...)
		{
			e_what = "Unknown exception";
		}

		if (first != last) // fail if we did not get a full match
			return false;

		return r;
#else

		return false;
#endif
	}

	template <typename Iterator>
	bool parse2(Iterator first, Iterator last, ParserCallBack _callback, std::string& e_what)
	{
//#ifdef PARSER_ENABLE
//
//		// rules
//		boost::spirit::qi::rule<Iterator, std::string(), boost::spirit::ascii::space_type> flags, month, week_day, /*utc,*/ nil, name, utf8, code, name2;
//		boost::spirit::qi::rule<Iterator, TRFC822(), boost::spirit::ascii::space_type> rfc822;
//		boost::spirit::qi::rule<Iterator, TDate(), boost::spirit::ascii::space_type> date;
//		boost::spirit::qi::rule<Iterator, TTime(), boost::spirit::ascii::space_type> time;
//		boost::spirit::qi::rule<Iterator, TUtc(), boost::spirit::ascii::space_type> utc;
//		boost::spirit::qi::rule<Iterator, TDateTime(), boost::spirit::ascii::space_type> datetime, datetime_value, internaldate;
//		boost::spirit::qi::rule<Iterator, THeader(), boost::spirit::ascii::space_type> header;
//		boost::spirit::qi::rule<Iterator, TAddress(), boost::spirit::ascii::space_type> address;
//		boost::spirit::qi::rule<Iterator, TAddressList(), boost::spirit::ascii::space_type> addressList;
//		boost::spirit::qi::rule<Iterator, TEnvelope(), boost::spirit::ascii::space_type> envelope;
//		boost::spirit::qi::rule<Iterator, TDocument(), boost::spirit::ascii::space_type> document;
//		boost::spirit::qi::rule<std::string::iterator, std::string(), boost::spirit::ascii::space_type> type;
//		boost::spirit::qi::rule<std::string::iterator, TIMAPStatus(), boost::spirit::ascii::space_type> answer;
//
//		type = +(boost::spirit::qi::string("OK") |
//			boost::spirit::qi::string("BAD") |
//			boost::spirit::qi::string("NO"));
//
//		answer = boost::spirit::lit('a')
//			>> (boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])
//			>> type[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> +(boost::spirit::qi::char_/*[boost::phoenix::at_c<2>(boost::spirit::_val) += boost::spirit::qi::_1]*/)
//			;
//
//
//		nil = boost::spirit::qi::string("NIL");
//
//
//		flags = boost::spirit::lit('(')
//			>> *(
//				boost::spirit::qi::string("\\Answered") |
//				boost::spirit::qi::string("\\Flagged") |
//				boost::spirit::qi::string("\\Deleted") |
//				boost::spirit::qi::string("\\Draft") |
//				boost::spirit::qi::string("\\Seen") |
//				boost::spirit::qi::string("\\Unanswered") |
//				boost::spirit::qi::string("\\Unflagged") |
//				boost::spirit::qi::string("\\Undeleted") |
//				boost::spirit::qi::string("\\Undraft") |
//				boost::spirit::qi::string("\\Unseen")
//				)
//			>> boost::spirit::lit(')');
//
//		month = (
//			boost::spirit::qi::string("Jan") |
//			boost::spirit::qi::string("Feb") |
//			boost::spirit::qi::string("Mar") |
//			boost::spirit::qi::string("Apr") |
//			boost::spirit::qi::string("May") |
//			boost::spirit::qi::string("Jun") |
//			boost::spirit::qi::string("Jul") |
//			boost::spirit::qi::string("Aug") |
//			boost::spirit::qi::string("Sep") |
//			boost::spirit::qi::string("Oct") |
//			boost::spirit::qi::string("Nov") |
//			boost::spirit::qi::string("Dec")
//			);
//
//		week_day = (
//			boost::spirit::qi::string("Mon") |
//			boost::spirit::qi::string("Tue") |
//			boost::spirit::qi::string("Wed") |
//			boost::spirit::qi::string("Thu") |
//			boost::spirit::qi::string("Fri") |
//			boost::spirit::qi::string("Sat") |
//			boost::spirit::qi::string("Sun")
//			);
//
//		date = boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])]
//			>> -boost::spirit::lit('-')
//			>> month[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> -boost::spirit::lit('-')
//			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1])];
//
//
//		time = boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])]
//			>> boost::spirit::lit(':')
//			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])]
//			>> boost::spirit::lit(':')
//			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1])];
//
//		utc = boost::spirit::lexeme[(boost::spirit::qi::char_("+-")[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])]
//			>> boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])];;
//
//
//		name = '\"'
//			>> boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
//			>> '\"';
//
//		name2 = boost::spirit::lit('{')
//			>> boost::spirit::qi::int_
//			>> boost::spirit::lit('}')
//			>> (boost::spirit::no_skip[+~boost::spirit::qi::char_("(\"")]);
//
//		utf8 = '\"'
//			>> boost::spirit::no_skip[+~boost::spirit::qi::char_('\"')]
//			>> '\"';
//
//		datetime = -week_day
//			>> -boost::spirit::lit(',')
//			>> date[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> time[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> -utc[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> -boost::spirit::qi::string("(UTC)")
//			>> -boost::spirit::qi::string("(MSK)")
//			>> -boost::spirit::qi::string("GMT");
//
//		datetime_value = -boost::spirit::lit('\"')
//			>> datetime
//			>> -boost::spirit::lit('\"');
//
//		internaldate = /*"INTERNALDATE"
//					   >>*/ datetime_value;
//
//		rfc822 = /*"RFC822.SIZE"
//				 >>*/ boost::spirit::lexeme[+(boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])];
//
//		address =
//			"("
//			>> (nil | "\"\"" | utf8[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])
//			>> nil
//			>> (nil | name[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])
//			>> (nil | name[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1])
//			>> ")"
//			;
//
//		addressList = *address[boost::phoenix::push_back(boost::phoenix::at_c<0>(boost::spirit::_val), boost::spirit::qi::_1)];
//
//		envelope = boost::spirit::lit('(')
//			>> boost::spirit::lit('\"')
//			>> datetime[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> boost::spirit::lit('\"')
//			//>> utf8[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> (nil | utf8[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1] | name2[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1])
//			>> boost::spirit::lit('(')
//			>> address[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> boost::spirit::lit(')')
//			//>> +nil
//			>> (nil |
//				(boost::spirit::lit('(')
//					>> address
//					>> boost::spirit::lit(')')
//					)
//				)
//			>> (nil |
//				(boost::spirit::lit('(')
//					>> address
//					>> boost::spirit::lit(')')
//					)
//				)
//			//>> boost::spirit::lit('(')
//			//>> addressList[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
//			//>> boost::spirit::lit(')')
//			>> (nil |
//				(boost::spirit::lit('(')
//					//>> address[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
//					>> addressList[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
//					>> boost::spirit::lit(')')
//					)
//				)
//			>> (nil |
//				(boost::spirit::lit('(')
//					//>> address[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
//					>> addressList[boost::phoenix::at_c<4>(boost::spirit::_val) = boost::spirit::qi::_1]
//					>> boost::spirit::lit(')')
//					)
//				)
//			>> nil
//			>> (nil | name)
//			>> (nil | name)
//			>> boost::spirit::lit(')');
//
//
//		header = *("UID" > boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//			| "FLAGS" > flags[boost::phoenix::at_c<1>(boost::spirit::_val) += boost::spirit::qi::_1]
//			| "INTERNALDATE" > internaldate[boost::phoenix::at_c<2>(boost::spirit::_val) = boost::spirit::qi::_1]
//			| "RFC822.SIZE" > rfc822[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
//			| "ENVELOPE" > envelope[boost::phoenix::at_c<4>(boost::spirit::_val) = boost::spirit::qi::_1]
//			)
//			;
//
//		document = '*'
//			>> boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> "FETCH"
//			>> '('
//			>> header[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
//			>> ')'
//			>> answer;
//
//		TDocument output;
//
//		bool r = false;
//
//		try
//		{
//			r = boost::spirit::qi::phrase_parse(
//				first,
//				last,
//				(
//					document[_callback]
//					),
//				boost::spirit::ascii::space,
//				output
//				);
//		}
//		catch (const boost::spirit::qi::expectation_failure<Iterator>& e)
//		{
//			std::string error(e.first, e.last);
//			e_what = error;
//
//		}
//		catch (const std::exception& e)
//		{
//			e_what = e.what();
//		}
//		catch (...)
//		{
//			e_what = "Unknown exception";
//		}
//
//		if (first != last) // fail if we did not get a full match
//			return false;
//
//		return r;
//#else
//
//		return false;
//#endif

		return false;
	}
}

bool fetchallparser_parse(std::string source, std::function<void(const fetchallparser::TDocument&doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string & errString)
{
	bool r = fetchallparser::parse(
		source.begin(),
		source.end(),
		_callback,
		errString
		);

		return r;
}

bool fetchallparser_parse2(std::string source, std::function<void(const fetchallparser::TDocument&doc, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string & errString)
{
	bool r = fetchallparser::parse2(
		source.begin(),
		source.end(),
		_callback,
		errString
		);

	return r;
}


/******************************* eof *************************************/