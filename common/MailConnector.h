/************************************************************************/
/* Name     : MCC\MailConnector.h                                       */
/* Author   : Andrey Alekseev                                           */
/* Project  : MCC                                                       */
/* Company  : Expert Solutions                                          */
/* Date     : 08 Feb 2016                                               */
/************************************************************************/

#pragma once

#include <string>
#include <future>

#include "Router\router_compatibility.h"
#include "DataReceiverImpl.h"
//#include "ImapMessageContext.h"

class CDataReceiverImpl;

template <class Impl>
class CMailConnector
{
public:

	template <class TParams>
	auto GetMail(CDataReceiverImpl *_routerInterface, 
		/*TImapParams params*/
		TParams && params
	)const
	{
		return static_cast<const Impl*>(this)->_getMail(_routerInterface, std::forward<TParams>(params));
	}

	template <class TParams, class TCollector>
	auto SendMail(CDataReceiverImpl *_routerInterface, TCollector&& msg_list, /*const std::wstring& _email, const std::wstring& certificate_uri, const std::wstring& _uri, const std::wstring& _port,
		const std::wstring& _login, const std::wstring& _pass, bool bSSLEnable*/
		TParams && params)const
	{
		return static_cast<const Impl*>(this)->_sendMail(_routerInterface, std::forward<TCollector>(msg_list), std::forward<TParams>(params)/*msg_list, _email, certificate_uri, _uri, _port, _login, _pass, bSSLEnable*/);
	}
};

//class smtp_client;
//class CMailConnectorImpl : public CMailConnector < CMailConnectorImpl >
//{
//	friend class CMailConnector < CMailConnectorImpl >;
//public:
//	CMailConnectorImpl(boost::asio::io_service& io);
//	~CMailConnectorImpl();
//
//private:
//	bool _getMail(CDataReceiverImpl *_routerInterface, 
//		TImapParams params)const;
//
//	bool _sendMail(CDataReceiverImpl *_routerInterface, MessageCollector& msg_list, const std::wstring& _email, const std::wstring& certificate_uri, const std::wstring& _uri, const std::wstring& _port,
//		const std::wstring& _login, const std::wstring& _pass, bool bSSLEnable)const;
//
////private:
////	//handler
////	void _imap_accept(CDataReceiverImpl *, MessagePtr message)const;
////	void _imap_completed(CDataReceiverImpl *, const std::wstring& _email, MessagePtr message)const;
////	void _imap_failed(CDataReceiverImpl *, MessagePtr message)const;
////
////	void _smtp_accept(CDataReceiverImpl *, const CMessage& msg)const;
////	void _smtp_completed(CDataReceiverImpl *, const CMessage& msg)const;
////	void _smtp_failed(CDataReceiverImpl *, const CMessage& msg)const;
//
//private:
//	boost::asio::io_service& r_io;
//
//	//using ImapClientPtr = std::shared_ptr<imap_client>;
//	using SmtpClientPtr = std::shared_ptr<smtp_client>;
//	//using ClientCollector = std::list <ImapClientPtr>;
//
//	CDataReceiverImpl *m_pReceiver;
//};


/******************************* eof *************************************/
