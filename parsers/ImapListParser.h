/************************************************************************/
/* Name     : MCC\ImapListParser.h                                      */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 11 Apr 2016                                               */
/************************************************************************/

#pragma once

bool listparser_parse(std::string source, std::function <void(const std::string& box_name, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);
bool listparser_parse2(std::string source, std::function <void(const std::string& box_name, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback, std::string& errString);

/******************************* eof *************************************/