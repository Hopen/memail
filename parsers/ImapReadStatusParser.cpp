/************************************************************************/
/* Name     : MCC\ImapReadStatusParser.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Expert Solutions                                          */
/* Date     : 1 Jun 2016                                                */
/************************************************************************/
#include "stdafx.h"
#include "ImapReadStatusParser.h"


//#define PARSER_ENABLE

//#include <string>
//#include <functional>
//
//#include <boost/format.hpp>
//#  pragma warning(disable:4348)
//#include <boost/config/warning_disable.hpp>
//#include <boost/spirit/include/qi.hpp>
//#include "Patterns/string_functions.h"
//
//#ifdef PARSER_ENABLE
//#include <boost/spirit/include/phoenix_operator.hpp>
//#include <boost/spirit/include/phoenix_fusion.hpp>
//#include <boost/fusion/include/adapt_struct.hpp>
//#include <boost/variant/recursive_variant.hpp>
//#include <boost/spirit/include/phoenix_stl.hpp>
//#include <boost/spirit/include/qi_no_skip.hpp>
//#endif


BOOST_FUSION_ADAPT_STRUCT(
	readstatusparser::TIMAPStatus,
	(int, _number)
	(std::string, _status)
	(std::string, _extra)
	(std::string, _command_name)
	(std::string, _completed)
	)


namespace readstatusparser
{
	using ParserCallBack = std::function <void(const TIMAPStatus& answer, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)>;

	template <typename Iterator>
	bool parse(Iterator first, Iterator last, ParserCallBack _callback)
	{
#ifdef PARSER_ENABLE

		//rules
		boost::spirit::qi::rule<std::string::iterator, std::string(), boost::spirit::ascii::space_type> type, completed, command_name, params;
		boost::spirit::qi::rule<std::string::iterator, TIMAPStatus(), boost::spirit::ascii::space_type> answer;

		params = '['
			>> -boost::spirit::no_skip[+~boost::spirit::qi::char_(']')]
			>> ']';

		type = +(boost::spirit::qi::string("OK")  | 
			     boost::spirit::qi::string("BAD") | 
			     boost::spirit::qi::string("NO")   );


		command_name = +(boost::spirit::ascii::no_case[boost::spirit::qi::string("LOGIN")] |
			boost::spirit::ascii::no_case[boost::spirit::qi::string("Authentication")] |
			boost::spirit::ascii::no_case[boost::spirit::qi::string("LIST")] |
			boost::spirit::ascii::no_case[boost::spirit::qi::string("CREATE")] |
			boost::spirit::ascii::no_case[boost::spirit::qi::string("SELECT")] |
			boost::spirit::ascii::no_case[boost::spirit::qi::string("SEARCH")] |
			boost::spirit::ascii::no_case[boost::spirit::qi::string("FETCH")] |
			boost::spirit::ascii::no_case[boost::spirit::qi::string("EXPUNGE")] |
			boost::spirit::ascii::no_case[boost::spirit::qi::string("STORE")] |
			boost::spirit::ascii::no_case[boost::spirit::qi::string("COPY")] |
			boost::spirit::ascii::no_case[boost::spirit::qi::string("LOGOUT")]);

		completed = +(boost::spirit::qi::string("completed") | 
			boost::spirit::qi::string("successful") |
			boost::spirit::qi::string("done")
			);

		
		answer = boost::spirit::lit('a')
			>> (boost::spirit::qi::int_[boost::phoenix::at_c<0>(boost::spirit::_val) = boost::spirit::qi::_1])
			>> type[boost::phoenix::at_c<1>(boost::spirit::_val) = boost::spirit::qi::_1]
			//>> +(boost::spirit::qi::char_/*[boost::phoenix::at_c<2>(boost::spirit::_val) += boost::spirit::qi::_1]*/)
			//>> -boost::spirit::qi::string("[READ-WRITE]")
			>> -params
			>> command_name[boost::phoenix::at_c<3>(boost::spirit::_val) = boost::spirit::qi::_1]
			>> completed[boost::phoenix::at_c<4>(boost::spirit::_val) = boost::spirit::qi::_1]
			;


		TIMAPStatus output;

		bool r = boost::spirit::qi::phrase_parse(
			first,
			last,
			(
				answer[_callback]
				),
			boost::spirit::ascii::space,
			output
			);

		if (first != last) // fail if we did not get a full match
			return false;

		//sectionList list = output.GetSections();
		return r;
#else

		return false;
#endif
	}

}

bool readstatusparser_parse(std::string source, std::function <void(const readstatusparser::TIMAPStatus& answer, boost::spirit::qi::unused_type, boost::spirit::qi::unused_type)> _callback)
{

	bool r = readstatusparser::parse(
		source.begin(),
		source.end(),
		_callback);

	return r;
}

/******************************* eof *************************************/