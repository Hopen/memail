/************************************************************************/
/* Name     : MEMAIL\SMTP\MailConnectorImpl.cpp                         */
/* Author   : Andrey Alekseev                                           */
/* Project  : MEMAIL\SMTP                                               */
/* Company  : Expert Solutions                                          */
/* Date     : 16 Aug 2016                                               */
/************************************************************************/
#include "stdafx.h"
#include <boost/program_options/detail/convert.hpp>

#include "MailConnectorImpl.h"
#include "Log/SystemLog.h"

#include "SmtpClient.h"


/*************************************************************/
/********************* CMailConnectorImpl ********************/
/*************************************************************/

CMailConnectorImpl::CMailConnectorImpl(boost::asio::io_service& io)
	:r_io(io)
{
}


CMailConnectorImpl::~CMailConnectorImpl()
{
}



std::atomic<unsigned int> threadsCount{ 0 };

bool CMailConnectorImpl::_getMail(CDataReceiverImpl *_routerInterface,
	TImapParams params) const
{
	return false;
}



bool CMailConnectorImpl::_sendMail(CDataReceiverImpl *_routerInterface,
	MessageCollector msg_list,
	/*const std::wstring & _sSmtpClientAddress,
	const std::wstring & certificate_uri,
	const std::wstring & _uri,
	const std::wstring & _port,
	const std::wstring & _login,
	const std::wstring & _pass,
	bool bSSLEnable*/
	TSmtpParams params
	) const
{
	singleton_auto_pointer<CSystemLog> log;
	log->LogStringModule(LEVEL_INFO, L"SMTP", L"[CONNECT] [%s:%s]", params._sSmtpServer.c_str(), params._sSmtpPort.c_str());

	try
	{
		if (params._sslEnable)
		{
			makeSSL_SmtpClient(_routerInterface->get_io_service(), std::move(params), log->GetInstance(), std::move(msg_list));
		}
		else
		{
			makeNoSSL_SmtpClient(_routerInterface->get_io_service(), std::move(params), log->GetInstance(), std::move(msg_list));
		}
		
		//boost::asio::io_service io_service;
		//boost::asio::ip::tcp::resolver resolver(/*r_io*/io_service);
		//boost::asio::ip::tcp::resolver::query query(wtos(_uri), wtos(_port));
		//boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(query);

		//boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);

		//SmtpClientPtr client;
		//if (bSSLEnable)
		//{
		//	boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
		//	log->LogStringModule(LEVEL_INFO, L"SMTP", L"Using SSL Certificate [%s]", certificate_uri.c_str());
		//	ctx.load_verify_file(wtos(certificate_uri));

		//	client = makeSSL_SmtpClient(io_service, ctx, iterator, msg_list, log->GetInstance(), _login, _pass, _sSmtpClientAddress,
		//		[pReciever = _routerInterface](const CMessage& msg)
		//		{
		//			pReciever->send_onAccepted(msg);
		//		},
		//			[pReciever = _routerInterface](const CMessage& msg)
		//		{
		//			pReciever->send_onCompleted(msg);
		//		},
		//			[pReciever = _routerInterface](const CMessage& msg)
		//		{
		//			pReciever->send_onFailed(msg);
		//		});
		//}
		//else
		//{
		//	client = makeNOSSL_SmtpClient(io_service, iterator, msg_list, log->GetInstance(), _login, _pass, _sSmtpClientAddress,
		//		[pReciever = _routerInterface](const CMessage& msg)
		//		{
		//			pReciever->send_onAccepted(msg);
		//		},
		//			[pReciever = _routerInterface](const CMessage& msg)
		//		{
		//			pReciever->send_onCompleted(msg);
		//		},
		//			[pReciever = _routerInterface](const CMessage& msg)
		//		{
		//			pReciever->send_onFailed(msg);
		//		});
		//}
		//if (client)
		//	client->start();

		//int nThread = ++threadsCount;

		//log->LogStringModule(LEVEL_INFO, L"SMTP", L"ASIO thread #%i has run", nThread);

		//io_service.run(); //wait until asio got a job

		//log->LogStringModule(LEVEL_INFO, L"SMTP", L"ASIO thread #%i has stoped", nThread);

		//msg_list.clear();

		//int test = 0;
	}
	catch (boost::system::system_error& ec)
	{
		log->LogStringModule(LEVEL_WARNING, L"SMTP", L"[CONNECTION FAILED] [%s:%s] with error: %s", params._sSmtpServer.c_str(), params._sSmtpPort.c_str(), stow(std::string(ec.what())).c_str());
	}
	catch (...)
	{
		log->LogStringModule(LEVEL_WARNING, L"SMTP", L"[CONNECTION FAILED]: unknown reason");
	}
	return false;
}


/******************************* eof *************************************/
